﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class MovingSystemTests {

    private const int TEST_FLOOR_WIDTH = 10;
    private const int TEST_FLOOR_HEIGHT = 10;
    private PathfindInfo pathfindInfo;
    private Vector3Int playerPosition;
    private List<Vector3Int> floorTiles;
    private List<Vector3Int> obstacleTiles;
    private List<Vector3Int> enemiesPositions;

    [OneTimeSetUp]
    public void BeforeAllTest() {
        playerPosition = new Vector3Int();
        floorTiles = new List<Vector3Int>();
        obstacleTiles = new List<Vector3Int>();
        enemiesPositions = new List<Vector3Int>();
        for(int i = 0; i < TEST_FLOOR_WIDTH; i++) {
            for (int j = 0; j < TEST_FLOOR_WIDTH; j++) {
                floorTiles.Add(new Vector3Int(i, j, 0));
            }
        }
    }

    [SetUp]
    public void BeforeEveryTest() {
        pathfindInfo = new PathfindInfo();
        obstacleTiles.Clear();
        enemiesPositions.Clear();
    }

    public class GetMovePositionsMethod : MovingSystemTests {

        [Test]
        public void _0AP() {
            playerPosition = Vector3Int.zero;
            List<Vector3Int> expectedMoveTiles = 
                new List<Vector3Int> { new Vector3Int() };
            List<KeyValuePair<int, Vector3Int>> expectedTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int())};

            MovingSystemPlain.GetMovePositions(playerPosition, playerPosition, 0, ref pathfindInfo,
                floorTiles, obstacleTiles, enemiesPositions);
            Assert.AreEqual(expectedMoveTiles, pathfindInfo.moveTiles);
            Assert.AreEqual(expectedTilesWithDistances, pathfindInfo.tilesWithDistances);
        }

        [Test]
        public void _InCorner_3AP_NoEnemies_NoObstacles() {
            playerPosition = Vector3Int.zero;
            obstacleTiles.Add(new Vector3Int(4, 2, 0)); //препятствия не в области хода
            obstacleTiles.Add(new Vector3Int(2, 5, 0));
            obstacleTiles.Add(new Vector3Int(2, 6, 0));
            List<Vector3Int> expectedMoveTiles = new List<Vector3Int> { new Vector3Int(),
                new Vector3Int(1,0,0), new Vector3Int(0,1,0),
                new Vector3Int(2,0,0), new Vector3Int(1,1,0), new Vector3Int(0,2,0),
                new Vector3Int(3,0,0), new Vector3Int(2,1,0), new Vector3Int(1,2,0),new Vector3Int(0,3,0)};
            List<KeyValuePair<int, Vector3Int>> expectedTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int()),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(1,0,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(0,1,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(2,0,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(1,1,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(0,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,0,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(1,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(0,3,0))};

            MovingSystemPlain.GetMovePositions(playerPosition, playerPosition, 3, ref pathfindInfo,
                floorTiles, obstacleTiles, enemiesPositions);
            Assert.AreEqual(expectedMoveTiles, pathfindInfo.moveTiles);
            Assert.AreEqual(expectedTilesWithDistances, pathfindInfo.tilesWithDistances);
        }

        [Test]
        public void _InCorner_3AP_NoEnemies_1Obstacle() {
            playerPosition = Vector3Int.zero;
            obstacleTiles.Add(new Vector3Int(0, 2, 0));
            List<Vector3Int> expectedMoveTiles = new List<Vector3Int> { new Vector3Int(),
                new Vector3Int(1,0,0), new Vector3Int(0,1,0),
                new Vector3Int(2,0,0), new Vector3Int(1,1,0),
                new Vector3Int(3,0,0), new Vector3Int(2,1,0), new Vector3Int(1,2,0)};
            List<KeyValuePair<int, Vector3Int>> expectedTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int()),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(1,0,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(0,1,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(2,0,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(1,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,0,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(1,2,0))};

            MovingSystemPlain.GetMovePositions(playerPosition, playerPosition, 3, ref pathfindInfo,
                floorTiles, obstacleTiles, enemiesPositions);
            Assert.AreEqual(expectedMoveTiles, pathfindInfo.moveTiles);
            Assert.AreEqual(expectedTilesWithDistances, pathfindInfo.tilesWithDistances);
        }

        [Test]
        public void _InCorner_3AP_1Enemy_NoObstacle() {
            playerPosition = Vector3Int.zero;
            enemiesPositions.Add(new Vector3Int(0, 2, 0));  //в области хода
            enemiesPositions.Add(new Vector3Int(4, 1, 0));  //не в области хода
            List<Vector3Int> expectedMoveTiles = new List<Vector3Int> { new Vector3Int(),
                new Vector3Int(1,0,0), new Vector3Int(0,1,0),
                new Vector3Int(2,0,0), new Vector3Int(1,1,0),
                new Vector3Int(3,0,0), new Vector3Int(2,1,0), new Vector3Int(1,2,0)};
            List<KeyValuePair<int, Vector3Int>> expectedTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int()),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(1,0,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(0,1,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(2,0,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(1,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,0,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(1,2,0))};

            MovingSystemPlain.GetMovePositions(playerPosition, playerPosition, 3, ref pathfindInfo,
                floorTiles, obstacleTiles, enemiesPositions);
            Assert.AreEqual(expectedMoveTiles, pathfindInfo.moveTiles);
            Assert.AreEqual(expectedTilesWithDistances, pathfindInfo.tilesWithDistances);
        }

        [Test]
        public void _InCorner_3AP_1Enemy_1Obstacle() {
            playerPosition = Vector3Int.zero;
            obstacleTiles.Add(new Vector3Int(1, 0, 0));
            enemiesPositions.Add(new Vector3Int(0, 2, 0));
            List<Vector3Int> expectedMoveTiles = new List<Vector3Int> { new Vector3Int(),
                new Vector3Int(0,1,0),
                new Vector3Int(1,1,0),
                new Vector3Int(2,1,0), new Vector3Int(1,2,0)};
            List<KeyValuePair<int, Vector3Int>> expectedTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int()),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(0,1,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(1,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(1,2,0))};

            MovingSystemPlain.GetMovePositions(playerPosition, playerPosition, 3, ref pathfindInfo,
                floorTiles, obstacleTiles, enemiesPositions);
            Assert.AreEqual(expectedMoveTiles, pathfindInfo.moveTiles);
            Assert.AreEqual(expectedTilesWithDistances, pathfindInfo.tilesWithDistances);
        }

        [Test]
        public void _InCenter_3AP_NoEnemies_NoObstacles() {
            playerPosition = new Vector3Int(4, 3, 0);
            List<Vector3Int> expectedMoveTiles = new List<Vector3Int> { new Vector3Int(4,3,0),
                new Vector3Int(5,3,0), new Vector3Int(3,3,0), new Vector3Int(4,4,0), new Vector3Int(4,2,0),
                new Vector3Int(6,3,0), new Vector3Int(5,4,0), new Vector3Int(5,2,0), new Vector3Int(2,3,0), new Vector3Int(3,4,0), new Vector3Int(3,2,0), new Vector3Int(4,5,0), new Vector3Int(4,1,0),
                new Vector3Int(7,3,0), new Vector3Int(6,4,0), new Vector3Int(6,2,0), new Vector3Int(5,5,0), new Vector3Int(5,1,0), new Vector3Int(1,3,0), new Vector3Int(2,4,0), new Vector3Int(2,2,0),
                                       new Vector3Int(3,5,0), new Vector3Int(3,1,0), new Vector3Int(4,6,0), new Vector3Int(4,0,0)};
            List<KeyValuePair<int, Vector3Int>> expectedTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int(4,3,0)),

                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(5,3,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(3,3,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(4,4,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(4,2,0)),

                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(6,3,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(5,4,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(5,2,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(2,3,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(3,4,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(3,2,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(4,5,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(4,1,0)),

                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(7,3,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(6,4,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(6,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(5,5,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(5,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(1,3,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,4,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,5,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(4,6,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(4,0,0))};

            MovingSystemPlain.GetMovePositions(playerPosition, playerPosition, 3, ref pathfindInfo,
                floorTiles, obstacleTiles, enemiesPositions);
            Assert.AreEqual(expectedMoveTiles, pathfindInfo.moveTiles);
            Assert.AreEqual(expectedTilesWithDistances, pathfindInfo.tilesWithDistances);
        }

        [Test]
        public void _InCenter_3AP_1Enemy_1Obstacle() {
            playerPosition = new Vector3Int(4, 3, 0);
            obstacleTiles.Add(new Vector3Int(5, 3, 0));
            enemiesPositions.Add(new Vector3Int(3, 4, 0));
            List<Vector3Int> expectedMoveTiles = new List<Vector3Int> { new Vector3Int(4,3,0),
                new Vector3Int(3,3,0), new Vector3Int(4,4,0), new Vector3Int(4,2,0),
                new Vector3Int(2,3,0), new Vector3Int(3,2,0), new Vector3Int(5,4,0), new Vector3Int(4,5,0), new Vector3Int(5,2,0), new Vector3Int(4,1,0),
                new Vector3Int(1,3,0), new Vector3Int(2,4,0), new Vector3Int(2,2,0), new Vector3Int(3,1,0), new Vector3Int(6,4,0), new Vector3Int(5,5,0), new Vector3Int(3,5,0),
                                       new Vector3Int(4,6,0), new Vector3Int(6,2,0), new Vector3Int(5,1,0), new Vector3Int(4,0,0)};
            List<KeyValuePair<int, Vector3Int>> expectedTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int(4,3,0)),

                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(3,3,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(4,4,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(4,2,0)),

                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(2,3,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(3,2,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(5,4,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(4,5,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(5,2,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(4,1,0)),

                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(1,3,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,4,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(6,4,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(5,5,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,5,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(4,6,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(6,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(5,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(4,0,0))};

            MovingSystemPlain.GetMovePositions(playerPosition, playerPosition, 3, ref pathfindInfo,
                floorTiles, obstacleTiles, enemiesPositions);
            Assert.AreEqual(expectedMoveTiles, pathfindInfo.moveTiles);
            Assert.AreEqual(expectedTilesWithDistances, pathfindInfo.tilesWithDistances);
        }
    }
   
    public class GetOrthogonalPathMethod : MovingSystemTests {

        [Test]
        public void _3AP_Straight_Path_Horizontal() {
            playerPosition = new Vector3Int(4, 3, 0);
            List<Vector3Int> testMoveTiles = new List<Vector3Int> { new Vector3Int(4,3,0),
                new Vector3Int(5,3,0), new Vector3Int(3,3,0), new Vector3Int(4,4,0), new Vector3Int(4,2,0),
                new Vector3Int(6,3,0), new Vector3Int(5,4,0), new Vector3Int(5,2,0), new Vector3Int(2,3,0), new Vector3Int(3,4,0), new Vector3Int(3,2,0), new Vector3Int(4,5,0), new Vector3Int(4,1,0),
                new Vector3Int(7,3,0), new Vector3Int(6,4,0), new Vector3Int(6,2,0), new Vector3Int(5,5,0), new Vector3Int(5,1,0), new Vector3Int(1,3,0), new Vector3Int(2,4,0), new Vector3Int(2,2,0),
                                       new Vector3Int(3,5,0), new Vector3Int(3,1,0), new Vector3Int(4,6,0), new Vector3Int(4,0,0)};
            List<KeyValuePair<int, Vector3Int>> testTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int(4,3,0)),

                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(5,3,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(3,3,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(4,4,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(4,2,0)),

                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(6,3,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(5,4,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(5,2,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(2,3,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(3,4,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(3,2,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(4,5,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(4,1,0)),

                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(7,3,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(6,4,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(6,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(5,5,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(5,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(1,3,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,4,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,5,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(4,6,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(4,0,0))};

            pathfindInfo.tilesWithDistances = testTilesWithDistances;
            pathfindInfo.moveTiles = testMoveTiles;
            Vector3Int endTile = new Vector3Int(7, 3, 0);

            List<Vector3Int> expectedPath = new List<Vector3Int> { new Vector3Int(7, 3, 0) ,
                        new Vector3Int(6, 3, 0), new Vector3Int(5, 3, 0)};

            Assert.AreEqual(expectedPath, MovingSystemPlain.GetOrthogonalPath(playerPosition, endTile, pathfindInfo));
        }

        [Test]
        public void _3AP_Straight_Path_Vertical() {
            playerPosition = new Vector3Int(4, 3, 0);
            List<Vector3Int> testMoveTiles = new List<Vector3Int> { new Vector3Int(4,3,0),
                new Vector3Int(5,3,0), new Vector3Int(3,3,0), new Vector3Int(4,4,0), new Vector3Int(4,2,0),
                new Vector3Int(6,3,0), new Vector3Int(5,4,0), new Vector3Int(5,2,0), new Vector3Int(2,3,0), new Vector3Int(3,4,0), new Vector3Int(3,2,0), new Vector3Int(4,5,0), new Vector3Int(4,1,0),
                new Vector3Int(7,3,0), new Vector3Int(6,4,0), new Vector3Int(6,2,0), new Vector3Int(5,5,0), new Vector3Int(5,1,0), new Vector3Int(1,3,0), new Vector3Int(2,4,0), new Vector3Int(2,2,0),
                                       new Vector3Int(3,5,0), new Vector3Int(3,1,0), new Vector3Int(4,6,0), new Vector3Int(4,0,0)};
            List<KeyValuePair<int, Vector3Int>> testTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int(4,3,0)),

                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(5,3,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(3,3,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(4,4,0)),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(4,2,0)),

                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(6,3,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(5,4,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(5,2,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(2,3,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(3,4,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(3,2,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(4,5,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(4,1,0)),

                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(7,3,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(6,4,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(6,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(5,5,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(5,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(1,3,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,4,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,5,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(4,6,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(4,0,0))};

            pathfindInfo.tilesWithDistances = testTilesWithDistances;
            pathfindInfo.moveTiles = testMoveTiles;
            Vector3Int endTile = new Vector3Int(4, 0, 0);

            List<Vector3Int> expectedPath = new List<Vector3Int> { new Vector3Int(4, 0, 0) ,
                        new Vector3Int(4, 1, 0), new Vector3Int(4, 2, 0)};

            Assert.AreEqual(expectedPath, MovingSystemPlain.GetOrthogonalPath(playerPosition, endTile, pathfindInfo));
        }

        [Test]
        public void _4AP_Obstacle_Path_Diagonal() {
            playerPosition = new Vector3Int(0, 0, 0);
            //отсутствуют клетки (1,2,0) и (0,1,0) - имитация препятствий на этих клетках
            List<Vector3Int> testMoveTiles = new List<Vector3Int> { new Vector3Int(),
                new Vector3Int(1,0,0),
                new Vector3Int(2,0,0), new Vector3Int(1,1,0), new Vector3Int(0,2,0),
                new Vector3Int(3,0,0), new Vector3Int(2,1,0), new Vector3Int(0,3,0),
                new Vector3Int(4,0,0), new Vector3Int(3,1,0), new Vector3Int(2,2,0), new Vector3Int(1,3,0), new Vector3Int(0,4,0)};

            List<KeyValuePair<int, Vector3Int>> testTilesWithDistances =
                new List<KeyValuePair<int, Vector3Int>> {
                new KeyValuePair<int, Vector3Int>(0, new Vector3Int()),
                new KeyValuePair<int, Vector3Int>(1, new Vector3Int(1,0,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(2,0,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(1,1,0)),
                new KeyValuePair<int, Vector3Int>(2, new Vector3Int(0,2,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(3,0,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(2,1,0)),
                new KeyValuePair<int, Vector3Int>(3, new Vector3Int(0,3,0)),
                new KeyValuePair<int, Vector3Int>(4, new Vector3Int(4,0,0)),
                new KeyValuePair<int, Vector3Int>(4, new Vector3Int(3,1,0)),
                new KeyValuePair<int, Vector3Int>(4, new Vector3Int(2,2,0)),
                new KeyValuePair<int, Vector3Int>(4, new Vector3Int(1,3,0)),
                new KeyValuePair<int, Vector3Int>(4, new Vector3Int(0,4,0))};

            pathfindInfo.tilesWithDistances = testTilesWithDistances;
            pathfindInfo.moveTiles = testMoveTiles;
            Vector3Int endTile = new Vector3Int(2, 2, 0);

            List<Vector3Int> expectedPath = new List<Vector3Int> { new Vector3Int(2, 2, 0) ,
                        new Vector3Int(2, 1, 0), new Vector3Int(1, 1, 0), new Vector3Int(1, 0, 0)};

            Assert.AreEqual(expectedPath, MovingSystemPlain.GetOrthogonalPath(playerPosition, endTile, pathfindInfo));
        }
    }

    public class GetPathSegmentsMethod {

        [Test]
        public void _1Tile_Path() {
            Vector3Int startPosition = new Vector3Int(1, 1, 0);
            List<Vector3Int> testPath = new List<Vector3Int> { new Vector3Int(1, 0, 0) };

            Assert.AreEqual(testPath, MovingSystemPlain.GetPathSegments(testPath, startPosition));
        }

        [Test]
        public void _1Segment_Path() {
            Vector3Int startPosition = new Vector3Int(1, 0, 0);
            List<Vector3Int> testPath = new List<Vector3Int> { new Vector3Int(1, 5, 0),
                new Vector3Int(1, 4, 0), new Vector3Int(1, 3, 0), new Vector3Int(1, 2, 0), new Vector3Int(1, 1, 0)};
            List<Vector3Int> expectedSegments = new List<Vector3Int> { new Vector3Int(1, 5, 0) };
            Assert.AreEqual(expectedSegments, MovingSystemPlain.GetPathSegments(testPath, startPosition));
        }

        [Test]
        public void _2Segment_Path() {
            Vector3Int startPosition = new Vector3Int();
            List<Vector3Int> testPath = new List<Vector3Int> { new Vector3Int(2, 2, 0),
                new Vector3Int(1, 2, 0), new Vector3Int(0, 2, 0), new Vector3Int(0, 1, 0)};
            List<Vector3Int> expectedSegments = new List<Vector3Int> { new Vector3Int(0, 2, 0), new Vector3Int(2, 2, 0) };
            Assert.AreEqual(expectedSegments, MovingSystemPlain.GetPathSegments(testPath, startPosition));
        }

        [Test]
        public void _4Segment_Path() {
            Vector3Int startPosition = new Vector3Int();
            List<Vector3Int> testPath = new List<Vector3Int> { new Vector3Int(4, 1, 0),
                new Vector3Int(3, 1, 0), new Vector3Int(2, 1, 0), new Vector3Int(2, 2, 0),
                new Vector3Int(2, 3, 0), new Vector3Int(1, 3, 0), new Vector3Int(0, 3, 0),
                new Vector3Int(0, 2, 0), new Vector3Int(0, 1, 0)};
            List<Vector3Int> expectedSegments = new List<Vector3Int> { new Vector3Int(0, 3, 0),
                new Vector3Int(2, 3, 0), new Vector3Int(2, 1, 0), new Vector3Int(4, 1, 0)};
            Assert.AreEqual(expectedSegments, MovingSystemPlain.GetPathSegments(testPath, startPosition));
        }

    }
}
