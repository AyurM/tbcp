﻿using UnityEngine;
using NUnit.Framework;

public class BspLeafTests {

    private BspLeaf leaf;

	[Test]
    public void _If_Leaf_Is_Split_Then_False() {
        leaf = new BspLeaf(new Vector3Int(), 30, 20) {
            leftChild = new BspLeaf(new Vector3Int(), 15, 20),
            rightChild = new BspLeaf(new Vector3Int(15, 0, 0), 15, 20)
        };
        Assert.AreEqual(false, leaf.Split());
    }

    [Test]
    public void _If_Leaf_Area_Too_Small_Then_False() {
        leaf = new BspLeaf(new Vector3Int(), 20, 9);
        Assert.AreEqual(false, leaf.Split());
    }

    [TestCase(LevelGenerator.MIN_ROOM_COLUMNS * 2, LevelGenerator.MIN_ROOM_ROWS - 1)]
    [TestCase(LevelGenerator.MIN_ROOM_COLUMNS - 1, LevelGenerator.MIN_ROOM_ROWS * 2)]
    [TestCase(LevelGenerator.MIN_ROOM_COLUMNS + 2, LevelGenerator.MIN_ROOM_ROWS + 2)]
    public void _If_Leaf_Side_Too_Small_Then_False(int width, int height) {
        leaf = new BspLeaf(new Vector3Int(), width, height);
        Assert.AreEqual(false, leaf.Split());
    }

    [Test]
    public void _If_Leaf_Not_Bigger_Than_Max_Room_Then_False() {
        leaf = new BspLeaf(new Vector3Int(), LevelGenerator.MAX_ROOM_COLUMNS,
            LevelGenerator.MAX_ROOM_ROWS);
        Assert.AreEqual(false, leaf.Split());
    }

    [TestCase(LevelGenerator.MAX_ROOM_COLUMNS + 1, LevelGenerator.MAX_ROOM_ROWS)]
    [TestCase(LevelGenerator.MAX_ROOM_COLUMNS, LevelGenerator.MAX_ROOM_ROWS + 1)]
    public void _If_Leaf_Splitable_Then_True(int width, int height) {
        leaf = new BspLeaf(new Vector3Int(), width, height);
        Assert.AreEqual(true, leaf.Split());
    }
}
