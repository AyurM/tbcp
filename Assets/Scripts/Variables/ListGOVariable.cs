﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Variables / GameObject List variable")]
public class ListGOVariable : ScriptableObject {

    public List<GameObject> list;
}
