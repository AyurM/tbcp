﻿using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Variables / Float variable")]
public class FloatVariable : ScriptableObject {

    public float value;
}
