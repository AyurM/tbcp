﻿using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Variables / Int variable")]
public class IntVariable : ScriptableObject {

    public int MaxValue;
    public int CurrentValue;

    public void SetCurrentValue(int value) {
        CurrentValue = value;
    }

    public void SetCurrentValue(IntVariable value) {
        CurrentValue = value.CurrentValue;
    }

    public void SetMaxValue(int value) {
        MaxValue = value;
    }

    public void SetMaxValue(IntVariable value) {
        MaxValue = value.MaxValue;
    }

}
