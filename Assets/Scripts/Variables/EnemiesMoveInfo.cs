﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Move Info / Enemies Info")]
public class EnemiesMoveInfo : ScriptableObject {

    public List<PathfindInfo> moveInfos;

    //здесь сохраняются результаты оценки возможной ОА, 
    //когда AI ищет подходящую позицию для атаки
    public List<Vector3Int> tempAttackTiles;
}
