﻿using UnityEngine;

[CreateAssetMenu(menuName = "Move Info / Player Info")]
public class PlayerMoveInfo : ScriptableObject {

    public PathfindInfo pathfindInfo;
}
