﻿using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Variables / Bool variable")]
public class BoolVariable : ScriptableObject {

    public bool value;
}
