﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Variables / List Vector3Int variable")]
public class ListVector3IntVariable : ScriptableObject {
    [HideInInspector]
    public List<Vector3Int> list;
}
