﻿using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Variables / Vector3 variable")]
public class Vector3Variable : ScriptableObject {

    public Vector3 value;
}
