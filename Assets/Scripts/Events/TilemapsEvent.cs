﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "Game Events / Tilemaps Event")]
public class TilemapsEvent : ScriptableObject {

    private readonly List<ITilemapsEventListener> eventListeners =
           new List<ITilemapsEventListener>();

    public void Raise(Tilemap wallMap, Tilemap floorMap, Tilemap obstacleMap) {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnTilemapsEventRaised(wallMap, floorMap, obstacleMap);
    }

    public void RegisterListener(ITilemapsEventListener listener) {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(ITilemapsEventListener listener) {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
