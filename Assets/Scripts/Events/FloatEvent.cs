﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events / Float Event")]
public class FloatEvent : ScriptableObject {

    private readonly List<IFloatEventListener> eventListeners =
           new List<IFloatEventListener>();

    public void Raise(FloatVariable variable) {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnFloatEventRaised(variable);
    }

    public void RegisterListener(IFloatEventListener listener) {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(IFloatEventListener listener) {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
