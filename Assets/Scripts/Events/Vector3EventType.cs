﻿using UnityEngine;

[CreateAssetMenu(menuName = "Game Events / Vector3 Event Type")]
public class Vector3EventType : ScriptableObject {	
}
