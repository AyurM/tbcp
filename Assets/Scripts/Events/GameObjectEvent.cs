﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events / GameObject Event")]
public class GameObjectEvent : ScriptableObject {

    private readonly List<IGameObjectEventListener> eventListeners =
           new List<IGameObjectEventListener>();

    public void Raise(GameObject gameObject, GameEventType type) {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnGameObjectEventRaised(gameObject, type);
    }

    public void RegisterListener(IGameObjectEventListener listener) {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(IGameObjectEventListener listener) {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
