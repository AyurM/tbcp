﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events / Bool Event")]
public class BoolEvent : ScriptableObject {

    private readonly List<IBoolListener> eventListeners =
           new List<IBoolListener>();

    public void Raise(bool value) {
        for (int i = 0; i < eventListeners.Count; i++)
            eventListeners[i].OnBoolEventRaised(value);
    }

    public void RegisterListener(IBoolListener listener) {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(IBoolListener listener) {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
