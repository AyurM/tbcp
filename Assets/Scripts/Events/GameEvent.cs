﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events / Game Event")]
public class GameEvent : ScriptableObject {

    private readonly List<IGameEventListener> eventListeners =
           new List<IGameEventListener>();

    public void Raise(GameEventType type) {
        for (int i = 0; i < eventListeners.Count; i++)
            eventListeners[i].OnEventRaised(type);
    }

    public void RegisterListener(IGameEventListener listener) {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(IGameEventListener listener) {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }

    public void SortListeners() {
        eventListeners.Sort((x,y) => x.Order.CompareTo(y.Order));
        /*Порядок IGameEventListener'ов: 
          0 - Moving System
          1 - Combat System
          2 - EnemiesController
          3 - WeaponUIListener
          4 - TurnTextListener
          5 - PlayerMoveInputListener
          6 - EnemyInfoUIListener
          7 - EndTurnButtonListener
          8 - CameraScrollListener
          9 - PlayerSounds
          10 - WeaponSound
          11 - LootGenerator
          12 - LootContainer
          13 - LootUIListener
          14 - InventoryUIListener
          15 - EquippedItems
          16 - EnemyMoveInputListener
          17 - AttackButtonListener
          18 - GameStateManager
          19 - QuickSlotUIListener
          20 - CharacterUIListener
          99 - NewFieldUIHandler

          Moving System и Combat System должны обрабатывать события первыми,
          а NewFieldUIHandler последним. InventoryUIListener до EquippedItems.
          Порядок остальных слушателей неважен.
          Сортировка происходит в NewInputSystem в Start().
         */
    }

    //public void ShowListeners() {
    //    for (int i = 0; i < eventListeners.Count; i++) {
    //        Debug.unityLogger.Log("Listener", eventListeners[i]);
    //    }
    //}
}
