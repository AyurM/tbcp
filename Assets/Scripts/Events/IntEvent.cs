﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events / Int Event")]
public class IntEvent : ScriptableObject {

    private readonly List<IIntEventListener> eventListeners =
           new List<IIntEventListener>();

    public void Raise(IntVariable variable) {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnIntEventRaised(variable);
    }

    public void RegisterListener(IIntEventListener listener) {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(IIntEventListener listener) {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
