﻿using UnityEngine;

[CreateAssetMenu(menuName = "Game Events / Game Event Type")]
public class GameEventType : ScriptableObject {

    [HideInInspector]
    public GameObject extraGO;
}
