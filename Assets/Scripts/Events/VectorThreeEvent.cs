﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Events / Vector3 Event")]
public class VectorThreeEvent : ScriptableObject {

    private readonly List<IVectorThreeListener> eventListeners =
           new List<IVectorThreeListener>();

    public void Raise(Vector3 clickPosition, Vector3EventType type) {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnVectorThreeEventRaised(clickPosition, type);
    }

    public void RegisterListener(IVectorThreeListener listener) {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(IVectorThreeListener listener) {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }

}
