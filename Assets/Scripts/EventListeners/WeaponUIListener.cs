﻿using UnityEngine;
using UnityEngine.UI;

public class WeaponUIListener : MonoBehaviour, IGameEventListener, IIntEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public Image weaponImage;
    public Text ammoText;
    public Text attackModeNameText;
    public Text attackModeAPText;
    public Text reloadApText;
    public Button reloadButton;
    public Button attackModeButton;

    public GameEvent gameEvent;
    [Tooltip("Прослушиваемое событие изменения AP")]
    public IntEvent intEvent;

    [Tooltip("Прослушиваемое событие смены текущего оружия игрока.")]
    public GameEventType onWeaponEquip;
    [Tooltip("Прослушиваемое событие перезарядки оружия.")]
    public GameEventType onWeaponReload;
    [Tooltip("Прослушиваемое событие смены режима атаки оружия.")]
    public GameEventType onAttackModeChanged;
    [Tooltip("Прослушиваемое событие промаха игрока.")]
    public GameEventType onPlayerMissed;
    [Tooltip("Прослушиваемое событие при нанесении игроком критического удара.")]
    public GameEventType onPlayerMadeCritHit;
    [Tooltip("Прослушиваемое событие при нанесении игроком обычного удара.")]
    public GameEventType onPlayerMadeHit;
    [Tooltip("Прослушиваемое событие начала хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("Прослушиваемое событие конца хода игрока.")]
    public GameEventType onEndTurn;

    [Tooltip("Вызываемое событие нажатия на кнопку перезарядки.")]
    public GameEventType onReloadButtonClick;
    [Tooltip("Вызываемое событие нажатия на кнопку смены режима атаки оружия.")]
    public GameEventType onAttackModeButtonClick;

    void Awake() {
        Order = 3;
    }

    void OnEnable() {
        Register();
        RegisterIntListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterIntListener();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onWeaponEquip) {
            OnWeaponEquip();
        } else if (type == onAttackModeChanged) {
            OnAttackModeChanged();
        } else if(type == onPlayerMissed || type == onPlayerMadeHit 
                     || type == onPlayerMadeCritHit || type == onWeaponReload) {
            if (db.inventory.equipment.mainWeapon.IsReloadable()) {
                RefreshAmmoUI();
            }
        } else if(type == onStartTurn) {
            OnStartTurn();
        } else if(type == onEndTurn) {
            OnEndTurn();
        }
    }

    private void OnStartTurn() {
        if (db.inventory.equipment.mainWeapon.IsReloadable()) {
            RefreshReloadButton();
        }      
        attackModeButton.interactable = true;
    }

    private void OnEndTurn() {
        reloadButton.interactable = false;
        attackModeButton.interactable = false;
    }

    public void OnIntEventRaised(IntVariable variable) {        
        if (variable == db.playerAP) {
            if (db.inventory.equipment.mainWeapon == null) {
                return;
            }
            if (!db.inventory.equipment.mainWeapon.IsReloadable()) {
                return;
            }
            RefreshReloadButton();
        }
    }

    public void OnReloadButtonClick() {
        gameEvent.Raise(onReloadButtonClick);
    }

    public void OnAttackModeButtonClick() {
        gameEvent.Raise(onAttackModeButtonClick);
    }

    private void OnWeaponEquip() {
        //Событие смены текущего оружия игрока, полностью обновить UI:
        weaponImage.sprite = db.inventory.equipment.mainWeapon.sprite;
        weaponImage.color = db.inventory.equipment.mainWeapon.itemRarity.Color;
        attackModeNameText.text = db.inventory.equipment.mainWeapon.GetCurrentAttackMode().attackName.ToUpper();
        attackModeAPText.text = db.inventory.equipment.mainWeapon.GetCurrentAttackMode().apToAttack + " AP";
        if (!db.inventory.equipment.mainWeapon.IsReloadable()) {
            ammoText.gameObject.SetActive(false);
            reloadButton.gameObject.SetActive(false);
        }
        else {
            ammoText.gameObject.SetActive(true);
            reloadButton.gameObject.SetActive(true);
            RefreshAmmoUI();
        }
    }

    private void OnAttackModeChanged() {
        attackModeNameText.text = db.inventory.equipment.mainWeapon.GetCurrentAttackMode().attackName.ToUpper();
        attackModeAPText.text = db.inventory.equipment.mainWeapon.GetCurrentAttackMode().apToAttack + " AP";
    }

    private void RefreshAmmoUI() {
        ammoText.text = db.inventory.equipment.mainWeapon.magazine.GetCurrentAmmo() + "/" +
                 db.inventory.equipment.mainWeapon.magazine.GetMaxAmmo();
        RefreshReloadButton();
    }

    private void RefreshReloadButton() {
        bool IsAbleToReload = db.playerAP.CurrentValue >= 
            ((GunMagazine)db.inventory.equipment.mainWeapon.magazine).apToReload;
        bool IsNeedForReload = db.inventory.equipment.mainWeapon.magazine.GetCurrentAmmo() < 
            db.inventory.equipment.mainWeapon.magazine.GetMaxAmmo();
        reloadButton.interactable = IsAbleToReload && IsNeedForReload;
        reloadApText.text = ((GunMagazine)db.inventory.equipment.mainWeapon.magazine).apToReload + " AP";
    }

    public void Register() {
        gameEvent.RegisterListener(this);
        //Debug.unityLogger.Log("WeaponUIListener registered!");
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }
}
