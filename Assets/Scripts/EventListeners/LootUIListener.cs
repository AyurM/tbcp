﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootUIListener : MonoBehaviour, IGameEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public Button lootButton;
    public GameObject lootPanel;
    [Tooltip("Префаб UI-элемента окна с лутом.")]
    public GameObject itemUIPrefab;
    [Tooltip("Content ScrollView.")]
    public Transform lootContentPanel;  
    public ScrollRect scrollRect;
    [Tooltip("Заголовок окна с лутом.")]
    public Text lootHeaderText;

    public GameEvent gameEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемое событие перемещения игрока на клетку с лутом.")]
    public GameEventType onLootboxEnter;
    [Tooltip("Прослушиваемое событие ухода игрока от клетки с лутом.")]
    public GameEventType onLootboxExit;
    [Tooltip("Прослушиваемое событие конца хода игрока.")]
    public GameEventType onEndTurn;

    //Вызываемые события
    [Tooltip("Вызываемое событие смены предмета в слоте быстрого доступа")]
    public GameEventType onQuickSlotEquip;

    private GameObject currentLootbox;

    void Awake() {
        Order = 13;
    }

    void OnEnable() {
        Register();
    }

    void OnDisable() {
        Unregister();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onLootboxEnter) {
            lootButton.gameObject.SetActive(true);
            currentLootbox = onLootboxEnter.extraGO;
        } else if(type == onLootboxExit || type == onEndTurn) {
            lootButton.gameObject.SetActive(false);
            currentLootbox = null;
        }
    }

    public void OnShowLootButtonClick() {
        if (lootPanel.activeInHierarchy || currentLootbox == null) {
            return;
        }
        lootPanel.SetActive(true);
        FillLootList();
    }

    public void OnCloseLootButtonClick() {
        ClearLootList();
        lootPanel.SetActive(false);
    }

    private void FillLootList() {
        List<Item> items = currentLootbox.GetComponent<Lootbox>().lootList;
        lootHeaderText.text = currentLootbox.GetComponent<Lootbox>().lootSource;    //обновить заголовок окна лута (название источника лута)
        for(int i = 0; i< items.Count; i++) {
            ShowLootItem(items[i], i);
        }

        /*scrollRect.vertical = items.Count > 3;*/  //если предметы не влезают на один экран, разрешить вертикальный скролл окна с лутом
    }

    private void ShowLootItem(Item item, int itemIndex) {
        GameObject itemUI = Instantiate(itemUIPrefab);
        itemUI.GetComponent<Button>().onClick
            .AddListener(() => AddItemFromLootToInventory(itemIndex));
        itemUI.transform.SetParent(lootContentPanel, false);
        itemUI.GetComponent<LootItemUI>().BindItem(item);
    }

    private void AddItemFromLootToInventory(int itemIndex) {
        List<Item> lootItems = currentLootbox.GetComponent<Lootbox>().lootList;
        db.inventory.AddItem(lootItems[itemIndex]);
        if(lootItems[itemIndex] is StackableItem) {
            gameEvent.Raise(onQuickSlotEquip);
        }
        lootItems.RemoveAt(itemIndex);
        //в данном источнике лута не осталось предметов
        if(lootItems.Count == 0) {
            Object.Destroy(currentLootbox);
            lootButton.gameObject.SetActive(false);
            lootPanel.SetActive(false);
        }
        RefreshLootList();
    }

    private void RefreshLootList() {
        if (currentLootbox == null) {
            OnCloseLootButtonClick();
        } else {
            ClearLootList();
            FillLootList();
        }
    }

    private void ClearLootList() {
        for (int i = 0; i < lootContentPanel.childCount; i++) {
            Object.Destroy(lootContentPanel.GetChild(i).gameObject);
        }
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
