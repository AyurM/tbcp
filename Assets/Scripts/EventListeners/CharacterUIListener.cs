﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterUIListener : MonoBehaviour, IGameEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public GameObject characterPanel;
    [Tooltip("Кнопка открытия окна персонажа.")]
    public Button characterButton;
    public Text charLevelText;
    public Text charXpText;
    public Text charHpText;
    public Text charApText;
    public Text charHitReductionText;
    public Text charShotReductionText;

    public GameEvent gameEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемое/вызываемое событие нажатия на кнопку окна персонажа.")]
    public GameEventType onShowCharacterInfoClick;
    [Tooltip("Прослушиваемое событие начала хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("Прослушиваемое событие конца хода игрока.")]
    public GameEventType onEndTurn;

    void Awake() {
        Order = 20;
    }

    void OnEnable() {
        Register();
    }

    void OnDisable() {
        Unregister();
    }

    public void OnEventRaised(GameEventType type) {
        if (type == onStartTurn) {
            OnStartTurn();
        } else if (type == onEndTurn) {
            OnEndTurn();
        } else if (type == onShowCharacterInfoClick) {
            ShowCharacterInfo();
        }
    }

    public void OnCharacterButtonClick() {
        gameEvent.Raise(onShowCharacterInfoClick);
    }

    public void OnCharacterInfoClose() {
        characterPanel.SetActive(false);
    }

    private void ShowCharacterInfo() {
        if (db.isPlayerMoving.value) {
            return;
        }
        characterPanel.SetActive(true);
        FillCharacterInfoView();
    }

    private void FillCharacterInfoView() {
        charLevelText.text = "Level: " + db.playerLevel.CurrentValue;
        charXpText.text = "Experience: " + db.playerXP.CurrentValue + "/" + db.playerXP.MaxValue;
        charHpText.text = "Health points: " + db.playerHP.CurrentValue + "/" + db.playerHP.MaxValue;
        charApText.text = "Action points: " + db.playerAP.CurrentValue + "/" + db.playerAP.MaxValue;
        charHitReductionText.text = "Hit Reduction: " + db.inventory.equipment.GetTotalHitReduction();
        charShotReductionText.text = "Shot Reduction: " + db.inventory.equipment.GetTotalShotReduction();
    }

    private void OnStartTurn() {
        characterButton.interactable = true;
    }

    private void OnEndTurn() {
        characterButton.interactable = false;
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
