﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AttackButtonListener : MonoBehaviour, IBoolListener, IIntEventListener,
    IGameEventListener{

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    //События
    public GameEvent gameEvent;
    [Tooltip("Прослушиваемое событие активации/деактивации кнопки атаки.")]
    public BoolEvent attackButtonToggleEvent;
    [Tooltip("Прослушиваемое событие изменения AP")]
    public IntEvent intEvent;
    [Tooltip("Прослушиваемое событие завершения уровня.")]
    public GameEventType onLevelComplete;

    [Tooltip("Вызываемое событие нажатия на кнопку атаки.")]
    public GameEventType onAttackButtonClick;
    [Tooltip("Вызываемое событие запроса сохранения игры")]
    public GameEventType onSaveGame;

    public Button attackButton;
    public Text attackText;
    public Color activeTextColor;
    public Color disabledTextColor;

    void Awake() {
        Order = 17;
    }

    void OnEnable() {
        Register();
        RegisterBoolListener();
        RegisterIntListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterBoolListener();
        UnregisterIntListener();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onLevelComplete) {
            OnLevelComplete();
        }
    }

    public void OnBoolEventRaised(bool value) {
        //Debug.unityLogger.Log("attackButtonToggleEvent Raised!", value);
        if (db.isLevelComplete.value) {
            return;
        }
        if(db.inventory.equipment.mainWeapon.IsReloadable() &&
            db.inventory.equipment.mainWeapon.magazine.GetCurrentAmmo() < db.inventory.equipment.mainWeapon.GetCurrentAttackMode().ammoRequired) {
            //в перезаряжаемом оружии не хватает патронов, отключить кнопку атаки
            attackButton.interactable = false;
            attackText.color = disabledTextColor;
            return;
        }
        if (value) {
            CheckAP();
        }
        else {
            attackButton.interactable = false;
            attackText.color = disabledTextColor;
        }       
    }

    public void OnIntEventRaised(IntVariable variable) {
        if (db.isLevelComplete.value) {
            return;
        }
        if (variable == db.playerAP && attackButton.interactable == true) {
            CheckAP();
        }
    }

    //Проверяет, достаточно ли AP для атаки. При нехватке AP отключает кнопку атаки.
    private void CheckAP() {
        bool IsAbleToAttack = db.playerAP.CurrentValue
                >= db.inventory.equipment.mainWeapon.GetCurrentAttackMode().apToAttack;
        attackButton.interactable = IsAbleToAttack;
        attackText.color = IsAbleToAttack ? activeTextColor : disabledTextColor;
    }

    public void OnAttackButtonClick() {
        if (db.isLevelComplete.value) {
            db.levelNumber.CurrentValue++;
            gameEvent.Raise(onSaveGame);
            SceneManager.LoadScene(1, LoadSceneMode.Single);
            return;
        }
        gameEvent.Raise(onAttackButtonClick);
    }

    private void OnLevelComplete() {
        attackButton.interactable = true;
        attackText.color = activeTextColor;
        attackText.text = "NEXT LEVEL";
    }

    //Регистрация/отписка слушателей
    public void RegisterBoolListener() {
        attackButtonToggleEvent.RegisterListener(this);
    }

    public void UnregisterBoolListener() {
        attackButtonToggleEvent.UnregisterListener(this);
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
