﻿using UnityEngine;
using UnityEngine.UI;

public class ApToMoveUIListener : MonoBehaviour, IBoolListener {

    public SODatabase db;

    [Tooltip("Прослушиваемое событие включения/выключения UI с" +
        " требуемым для перемещения количеством очков действия")]
    public BoolEvent apToMoveToggleEvent;
    public GameObject apText;

    void OnEnable() {
        RegisterBoolListener();
    }

    void OnDisable() {
        UnregisterBoolListener();
    }

    void Update() {
        if (apText.activeInHierarchy) {
            gameObject.transform.position = Camera.main.WorldToScreenPoint(db.selectedTile.value);
        }
    }

    public void OnBoolEventRaised(bool value) {
        apText.SetActive(value);
        if (value) {
            apText.GetComponent<Text>().text = "" + db.apToMove.CurrentValue;
            gameObject.transform.position = Camera.main.WorldToScreenPoint(db.clickPosition.value);
        }        
    }

    public void RegisterBoolListener() {
        apToMoveToggleEvent.RegisterListener(this);
    }

    public void UnregisterBoolListener() {
        apToMoveToggleEvent.UnregisterListener(this);
    }
}
