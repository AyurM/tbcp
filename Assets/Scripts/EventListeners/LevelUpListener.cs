﻿using UnityEngine;
using UnityEngine.UI;

public class LevelUpListener : MonoBehaviour, IIntEventListener {

    public SODatabase db;

    public float lifeTime;
    public IntEvent intEvent;

    public Text levelUpText;

    void OnEnable() {
        RegisterIntListener();
    }

    void OnDisable() {
        UnregisterIntListener();
    }

    public void OnIntEventRaised(IntVariable variable) {
        if(variable == db.playerLevel) {
            levelUpText.gameObject.SetActive(true);
            Invoke("HideLeveLUpUI", lifeTime);
        }
    }

    private void HideLeveLUpUI() {
        levelUpText.gameObject.SetActive(false);
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }
}
