﻿using UnityEngine;

public class CameraScrollListener : MovingObject, IVectorThreeListener, 
    IGameEventListener{

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;
    [Tooltip("Регулировка скорости скроллинга камеры")]
    public float scrollCoeff = 0.5f;

    [Tooltip("Прослушиваемое событие скроллинга камеры")]
    public VectorThreeEvent vector3Event;
    [Tooltip("Событие без параметров")]
    public GameEvent gameEvent;

    [Tooltip("Прослушиваемое события начала хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("Прослушиваемое события конца хода игрока.")]
    public GameEventType onEndTurn;
    [Tooltip("Прослушиваемый событие начала хода противника.")]
    public GameEventType onBeforeEnemyTurn;
    [Tooltip("Прослушиваемый тип события GameEvent для обработки окончания перемещения противника.")]
    public GameEventType onEnemyMovementEnd;
    public GameEventType onEnemyTurnEnd;

    public Vector3EventType cameraScrollType;
    public Vector3EventType enemyMoveType;

    private bool isTrackingEnemy;

    void Awake() {
        Order = 8;
        isTrackingEnemy = false;
    }

    void OnEnable() {
        RegisterVectorThreeListener();
        Register();
    }

    void OnDisable() {
        UnregisterVectorThreeListener();
        Unregister();
    }

    void Update() {
        if (db.isPlayerMoving.value) {
            gameObject.transform.position = db.playerPosition.value;
        }
        if (isTrackingEnemy) {
            gameObject.transform.position = db.enemiesList.list[db.currentEnemy.CurrentValue]
                .transform.position;
        }
    }

    public void OnVectorThreeEventRaised(Vector3 vector, Vector3EventType type) {
        if(type == cameraScrollType) {
            //Позиция трекера не должна выходить за границы уровня
            Vector3 oldPosition = gameObject.transform.position;
            Vector3 scrollDelta = vector - oldPosition;
            Vector3 newPosition = new Vector3(Mathf.Clamp(
                oldPosition.x + scrollDelta.x * scrollCoeff, db.cameraMinH.CurrentValue, db.cameraMaxH.CurrentValue),
                Mathf.Clamp(oldPosition.y + scrollDelta.y * scrollCoeff, db.cameraMinV.CurrentValue, db.cameraMaxV.CurrentValue));
            gameObject.transform.position = newPosition;
        } else if(type == enemyMoveType) {
            isTrackingEnemy = true;
        }
        
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onStartTurn) {
            //отцентрировать камеру на игроке в начале его хода
            StartCoroutine(base.Move(db.playerPosition.value));
        } else if (type == onBeforeEnemyTurn) {
            //отцентрировать камеру на противнике в начале его хода
            StartCoroutine(base.Move(
                db.enemiesList.list[db.currentEnemy.CurrentValue].transform.position));
        } else if(type == onEnemyTurnEnd) {
            isTrackingEnemy = false;
        }
    }

    protected override void DoAfterMoveCoroutine() {
    }

    public void RegisterVectorThreeListener() {
        vector3Event.RegisterListener(this);
    }

    public void UnregisterVectorThreeListener() {
        vector3Event.UnregisterListener(this);
    }

    public void Register() {
        gameEvent.RegisterListener(this);
        //Debug.unityLogger.Log("CameraScrollListener registered!");
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
