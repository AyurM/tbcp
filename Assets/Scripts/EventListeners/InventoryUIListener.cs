﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIListener : MonoBehaviour, IGameEventListener,
    IIntEventListener{

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public GameObject inventoryPanel;
    [Tooltip("Префаб UI-элемента окна инвентаря.")]
    public GameObject invItemUIPrefab;
    [Tooltip("Кнопка открытия окна инвентаря.")]
    public Button inventoryButton;
    [Tooltip("Inventory Content ScrollView.")]
    public Transform inventoryContentPanel;
    public ScrollRect scrollRect;
    [Tooltip("Панель со сведениями о предмете инвентаря.")]
    public InventoryItemInfo itemInfo;
    [Tooltip("Спрайт, который будет использоваться для пустых слотов.")]
    public Sprite emptySlotSprite;
    [Tooltip("Объект для хранения генерируемого на уровне лута.")]
    public GameObject lootContainer;
    [Tooltip("Префаб ящика с лутом.")]
    public GameObject lootboxPrefab;

    public GameEvent gameEvent;
    public IntEvent intEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемое событие начала хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("Прослушиваемое событие конца хода игрока.")]
    public GameEventType onEndTurn;
    [Tooltip("Прослушиваемое событие смены текущего оружия игрока.")]
    public GameEventType onWeaponEquip;
    [Tooltip("Прослушиваемое событие смены брони игрока.")]
    public GameEventType onArmorEquip;
    [Tooltip("Прослушиваемое событие смены слота быстрого доступа.")]
    public GameEventType onQuickSlotEquip;
    [Tooltip("Прослушиваемое/вызываемое событие нажатия на кнопку инвентаря.")]
    public GameEventType onShowInventoryClick;

    //Вызываемые события
    [Tooltip("Вызываемое событие перемещения игрока на клетку с лутом.")]
    public GameEventType onLootboxEnter;

    private int prevSelectedItemIndex = -1;
    private const int SLOTS_NUMBER = 12;    //кол-во слотов в инвентаре по умолчанию

    void Awake() {
        Order = 14;
    }

    void OnEnable() {
        Register();
        RegisterIntListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterIntListener();
    }

    void Start() {
        db.selectedInventoryItem.CurrentValue = -1;
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onStartTurn) {
            OnStartTurn();
        } else if(type == onEndTurn) {
            OnEndTurn();
        } else if(type == onWeaponEquip) {
            OnWeaponEquip();
        } else if(type == onArmorEquip) {
            OnArmorEquip();
        } else if(type == onQuickSlotEquip) {
            OnQSlotEquip();
        } else if(type == onShowInventoryClick) {
            OnShowInventory();
        }
    }

    public void OnIntEventRaised(IntVariable variable) {
        if (variable == db.selectedInventoryItem) {
            OnDropItemEvent();
        }      
    }

    private void OnArmorEquip() {
        //при смене брони обновить UI экипированной брони      
        if (inventoryPanel.activeInHierarchy) {
            for (int i = 0; i < db.inventory.items.Count; i++) {
                InventorySlot slot = inventoryContentPanel.GetChild(i).gameObject
                    .GetComponent<InventorySlot>();
                if(slot.slotItem.itemType == db.armorType) {
                    slot.RefreshEquippedUI();
                }               
            }            
        }
    }

    private void OnWeaponEquip() {
        //при смене оружия обновить UI экипированного оружия
        if (inventoryPanel.activeInHierarchy) {
            for (int i = 0; i < db.inventory.items.Count; i++) {
                InventorySlot slot = inventoryContentPanel.GetChild(i).gameObject
                    .GetComponent<InventorySlot>();
                if (slot.slotItem.itemType == db.weaponType) {
                    slot.RefreshEquippedUI();
                }
            }
        }     
    }

    private void OnQSlotEquip() {
        if (inventoryPanel.activeInHierarchy) {
            for (int i = 0; i < db.inventory.items.Count; i++) {
                InventorySlot slot = inventoryContentPanel.GetChild(i).gameObject
                    .GetComponent<InventorySlot>();
                if (slot.slotItem.itemType == db.usableType) {
                    slot.RefreshEquippedUI();
                }
            }
        }
    }

    public void OnInventoryButtonClick() {
        gameEvent.SortListeners();  //порядок InventoryUIListener и EquippedItems может сбиваться из-за переподписки на событие
        gameEvent.Raise(onShowInventoryClick);
    }

    private void OnShowInventory() {
        if (db.isPlayerMoving.value) {
            return;
        }
        inventoryPanel.SetActive(true);
        FillInventoryView();
    }

    private void FillInventoryView() {
        List<Item> items = db.inventory.items;
        for (int i = 0; i < items.Count; i++) {
            ShowInventoryItem(items[i], i);
        }

        //if(items.Count < SLOTS_NUMBER) {
        //    for(int i = 0; i < SLOTS_NUMBER - items.Count; i++) {
        //        ShowEmptySlot();
        //    }
        //}
        /*scrollRect.vertical = items.Count > SLOTS_NUMBER;*/   //если предметы не влезают на один экран, разрешить вертикальный скролл окна инвентаря
    }

    private void ShowInventoryItem(Item item, int index) {
        GameObject itemUI = Instantiate(invItemUIPrefab);
        itemUI.GetComponent<Button>().onClick
            .AddListener(() => {
                itemInfo.ShowItemInfo(item);
                ChangeSelectedItem(index);
            });
        itemUI.transform.SetParent(inventoryContentPanel, false);
        itemUI.GetComponent<InventorySlot>().BindItem(item);
    }    

    public void ChangeSelectedItem(int index) {
        //обновить UI выделенного в инвентаре предмета
        if(db.selectedInventoryItem.CurrentValue != -1) {
            prevSelectedItemIndex = db.selectedInventoryItem.CurrentValue;
            inventoryContentPanel.GetChild(prevSelectedItemIndex)
            .gameObject.GetComponent<InventorySlot>().SetSelected(false);
        }
        db.selectedInventoryItem.CurrentValue = index;
        inventoryContentPanel.GetChild(db.selectedInventoryItem.CurrentValue)
           .gameObject.GetComponent<InventorySlot>().SetSelected(true);
    }

    private void OnDropItemEvent() {
        DropItem();
        for (int i = 0; i < inventoryContentPanel.childCount; i++) {
            Destroy(inventoryContentPanel.GetChild(i).gameObject);
        }
        db.inventory.items.RemoveAt(db.selectedInventoryItem.CurrentValue);
        //TODO: необязательно обновлять предметы до выброшенного предмета
        FillInventoryView();
    }

    private void DropItem() {
        GameObject lootboxGameObject = lootContainer.GetComponent<LootContainer>()
                     .FindLootboxOnPosition(db.playerPosition.value);

        Lootbox lootbox;
        if (lootboxGameObject == null) {
            lootboxGameObject = Instantiate(lootboxPrefab,
                            db.playerPosition.value, Quaternion.identity);
            lootboxGameObject.transform.SetParent(lootContainer.transform);
        }
        lootbox = lootboxGameObject.GetComponent<Lootbox>();

        if(lootbox.lootList == null) {
            lootbox.lootList = new List<Item>();
        }
                   
        lootbox.lootSource = "Player";
        lootbox.lootList.Add(db.inventory.items[db.selectedInventoryItem.CurrentValue]);
        onLootboxEnter.extraGO = lootboxGameObject;
        db.isOnLootbox.value = true;
        gameEvent.Raise(onLootboxEnter);
    }

    //private void ShowEmptySlot() {
    //    GameObject itemUI = Instantiate(invItemUIPrefab);
    //    itemUI.GetComponent<Button>().interactable = false;
    //    itemUI.transform.SetParent(inventoryContentPanel, false);
    //    itemUI.GetComponent<InventorySlot>().EmptySlot(emptySlotSprite);
    //}

    private void OnStartTurn() {
        inventoryButton.interactable = true;
    }

    private void OnEndTurn() {
        inventoryButton.interactable = false;
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }
}
