﻿using UnityEngine;

[RequireComponent(typeof(Cinemachine.CinemachineVirtualCamera))]
public class CameraZoomListener : MonoBehaviour, IFloatEventListener {

    public SODatabase db;
    public FloatEvent floatEvent;

    private Cinemachine.CinemachineVirtualCamera cinemachineCam;
    
    void OnEnable() {
        RegisterFloatListener();
    }

    void OnDisable() {
        UnregisterFloatListener();
    }

    void Start() {
        cinemachineCam = GetComponent<Cinemachine.CinemachineVirtualCamera>();
    }

    public void OnFloatEventRaised(FloatVariable variable) {
        if(variable == db.cameraOrthSize) {
            cinemachineCam.m_Lens.OrthographicSize = db.cameraOrthSize.value;
        }
    }

    public void RegisterFloatListener() {
        floatEvent.RegisterListener(this);
    }

    public void UnregisterFloatListener() {
        floatEvent.UnregisterListener(this);
    }
}
