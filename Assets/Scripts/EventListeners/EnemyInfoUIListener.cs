﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyInfoUIListener : MonoBehaviour, IBoolListener, IGameEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public GameEvent gameEvent;
    [Tooltip("Прослушиваемое событие включения/выключения UI со" +
        " сведениями о противнике")]
    public BoolEvent enemyInfoUIToggleEvent;

    [Tooltip("Вызываемое событие при нанесении игроком критического удара.")]
    public GameEventType onPlayerMadeCritHit;
    [Tooltip("Вызываемое событие при нанесении игроком обычного удара.")]
    public GameEventType onPlayerMadeHit;

    public GameObject enemyInfoPanel;
    public Text enemyNameText;
    public Text enemyWeaponText;
    public Text enemyHPText;
    public Text enemyAPText;

    void Awake() {
        Order = 6;
    }

    void OnEnable() {
        Register();
        RegisterBoolListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterBoolListener();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onPlayerMadeHit || type == onPlayerMadeCritHit) {
            Enemy newEnemy = db.enemiesList.list[db.selectedEnemy.CurrentValue].
                GetComponent<Enemy>();
            ShowEnemyInfo(newEnemy);
        }
    }

    public void OnBoolEventRaised(bool value) {
        if (!value) {
            enemyInfoPanel.SetActive(false);
        } else {
            enemyInfoPanel.SetActive(true);
            Enemy newEnemy = db.enemiesList.list[db.selectedEnemy.CurrentValue].
                GetComponent<Enemy>();
            //извлечь и вывести инфо о противнике
            enemyInfoPanel.GetComponent<Image>().color = newEnemy.rarity.Color;
            enemyNameText.text = newEnemy.enemyName;
            ShowEnemyInfo(newEnemy);
        }
    }

    private void ShowEnemyInfo(Enemy enemy) {
        enemyHPText.text = "HP: " + enemy.currentHP + "/" + enemy.maxHP;
        enemyAPText.text = "AP: " + enemy.currentAP + "/" + enemy.maxAP;
        if(enemy.weapon == null) {
            enemyWeaponText.text = "Unarmed";
            return;
        }
        enemyWeaponText.text = enemy.weapon.itemName + " (" + enemy.weapon.rarityName + ")";
    }

    public void RegisterBoolListener() {
        //Debug.unityLogger.Log("EnemyInfoUIListener - enemyInfoUIToggleEvent", enemyInfoUIToggleEvent);
        enemyInfoUIToggleEvent.RegisterListener(this);
    }

    public void UnregisterBoolListener() {
        enemyInfoUIToggleEvent.UnregisterListener(this);
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
