﻿using UnityEngine;
using UnityEngine.UI;

public class QuickSlotUIListener : MonoBehaviour, IGameEventListener,
    IIntEventListener{

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public GameObject apPanel;
    public GameObject amountPanel;
    public Text apToUseText;
    public Text amountText;
    public Image slotImage;
    public Sprite emptySlotSprite;

    public GameEvent gameEvent;
    public IntEvent intEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемое событие смены предмета в слоте быстрого доступа.")]
    public GameEventType onQuickSlotEquip;

    //Вызываемые события
    [Tooltip("Вызываемое событие использования предмета в слоте быстрого доступа.")]
    public GameEventType onQuickSlotClick;
    [Tooltip("Вызываемое событие начала использования предмета в слоте быстрого доступа.")]
    public GameEventType onBeforePlayerAttack;
    [Tooltip("В конце хода игрока будет вызван GameEvent этого типа.")]
    public GameEventType onEndTurn;
    [Tooltip("Если не осталось врагов, то в конце хода будет вызван GameEvent этого типа.")]
    public GameEventType onStartTurn;

    void Awake() {
        Order = 19;
    }

    void OnEnable() {
        Register();
        RegisterIntListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterIntListener();
    }

    public void OnEventRaised(GameEventType type) {
        if (type == onQuickSlotEquip) {
            OnQuickSlotEquip();
        }
    }

    public void OnIntEventRaised(IntVariable variable) {
        if(variable == db.playerAP) {
            OnPlayerAPChange();
        }
    }

    public void OnQuickSlotClick() {
        Item qSlot = db.inventory.equipment.quickSlot;
        if (qSlot == null) {
            return;
        }
        if (qSlot is IUsable && db.playerAP.CurrentValue < ((IUsable)qSlot).ApToUse) {
            return;
        }
        if(qSlot is Medkit && db.playerHP.CurrentValue == db.playerHP.MaxValue) {
            //предотвратить использование аптечки, если у игрока полное здоровье
            return;
        }
        if (qSlot is IUsable) {
            ((IUsable)qSlot).Use();
            gameEvent.Raise(onBeforePlayerAttack);  //вызывается, чтобы корректно обновить подсветку клеток ОХ, т.к. ОХ будет обновлена из-за уменьшения АР
            db.playerAP.CurrentValue -= ((IUsable)qSlot).ApToUse;
            intEvent.Raise(db.playerAP);
        }
        gameEvent.Raise(onQuickSlotClick);
        CheckQuickSlotAmount();
        gameEvent.Raise(onQuickSlotEquip);
        if(db.playerAP.CurrentValue <= 0) {
            Invoke("EndTurn", 1f);
        }
    }

    private void OnPlayerAPChange() {
        if (db.inventory.equipment.quickSlot is IUsable) {
            CheckQuickSlotAP();
        }
    }

    private void OnQuickSlotEquip() {
        Item qSlot = db.inventory.equipment.quickSlot;
        if (qSlot == null) {
            slotImage.sprite = emptySlotSprite;
            slotImage.color = Color.white;
            apPanel.SetActive(false);
            amountPanel.SetActive(false);
            return;
        }

        slotImage.sprite = qSlot.sprite;
        slotImage.color = qSlot.itemRarity.Color;

        apPanel.SetActive(qSlot is IUsable);
        amountPanel.SetActive(qSlot is StackableItem);
        if (qSlot is IUsable) {
            apToUseText.text = ((IUsable)qSlot).ApToUse + " AP";
            CheckQuickSlotAP();
        }
        if (qSlot is StackableItem) {
            amountText.text = ((StackableItem)qSlot).quantity.ToString();
        }
    }

    private void CheckQuickSlotAP() {
        if (db.playerAP.CurrentValue >= ((IUsable)db.inventory.equipment.quickSlot).ApToUse) {
            apToUseText.color = Color.black;
        }
        else {
            apToUseText.color = Color.red;
        }
    }

    private void CheckQuickSlotAmount() {
        Item qSlot = db.inventory.equipment.quickSlot;
        if (qSlot is StackableItem && ((StackableItem)qSlot).quantity <= 0) {
            db.inventory.items.Remove(qSlot);
            db.inventory.equipment.quickSlot = null;
        }
    }

    private void EndTurn() {
        gameEvent.Raise(db.enemiesList.list.Count > 0 ? onEndTurn : onStartTurn);
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }
}
