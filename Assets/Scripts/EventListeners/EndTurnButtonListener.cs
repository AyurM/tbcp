﻿using UnityEngine;
using UnityEngine.UI;

public class EndTurnButtonListener : MonoBehaviour, IGameEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public GameEvent gameEvent;
    [Tooltip("Прослушиваемое события начала хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("Прослушиваемое события конца хода игрока.")]
    public GameEventType onEndTurn;

    public Button endTurnButton;
    public Text endTurnText;
    public Color activeTextColor;
    public Color disabledTextColor;

    void Awake() {
        Order = 7;
    }

    void OnEnable() {
        Register();
    }

    void OnDisable() {
        Unregister();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onStartTurn) {
            endTurnButton.interactable = true;
            endTurnText.color = activeTextColor;
        } else if(type == onEndTurn) {
            endTurnButton.interactable = false;
            endTurnText.color = disabledTextColor;
        }
    }

    public void OnEndTurnButtonClick() {
        gameEvent.Raise(db.enemiesList.list.Count > 0 ? onEndTurn : onStartTurn);
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
