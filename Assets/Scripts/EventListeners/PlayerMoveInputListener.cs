﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerMoveInputListener : MovingObject, IGameEventListener, 
        ITilemapsEventListener{
    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public GameEvent gameEvent;
    public TilemapsEvent tilemapsEvent;

    [Tooltip("Прослушиваемый тип события GameEvent для начала перемещения игрока.")]
    public GameEventType playerMoveStartEvent;
    [Tooltip("После окончания перемещения игрока будет вызван GameEvent этого типа")]
    public GameEventType playerMoveEndEvent;
    [Tooltip("Прослушиваемое событие нажатия на кнопку атаки.")]
    public GameEventType onAttackButtonClick;

    private Animator animator;

    void Awake() {
        Order = 5;
        objectPosition.value = gameObject.transform.position;
    }

    void OnEnable() {
        Register();
        RegisterTilemapEventListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterTilemapEventListener();
    }

    protected override void Start () {      
        db.isPlayerMoving.value = false;
        animator = GetComponent<Animator>();
        base.Start();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == playerMoveStartEvent) {
            //Debug.unityLogger.Log("Player received command to move!");
            db.isPlayerMoving.value = true;
            StartCoroutine(base.Move(db.playerMoveInfo.pathfindInfo.pathSegments));
        } else if(type == onAttackButtonClick) {
            animator.SetTrigger("playerAttack");
        }
    }

    public void OnTilemapsEventRaised(Tilemap wallMap, Tilemap floorMap, Tilemap obstacleMap) {
        gameObject.transform.position = db.playerPosition.value;
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
    public void RegisterTilemapEventListener() {
        tilemapsEvent.RegisterListener(this);
    }

    public void UnregisterTilemapEventListener() {
        tilemapsEvent.UnregisterListener(this);
    }


    protected override void DoAfterMoveCoroutine() {
        objectPosition.value = gameObject.transform.position;
        db.isPlayerMoving.value = false;
        gameEvent.Raise(playerMoveEndEvent);
    }
}
