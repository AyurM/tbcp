﻿using UnityEngine;

public class EnemyMoveInputListener : MovingObject, IGameObjectEventListener, 
    IGameEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;
    
    //События
    public GameEvent gameEvent;
    public GameObjectEvent gameObjectEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемый тип события GameEvent для начала перемещения противника.")]
    public GameEventType enemyMoveStartEvent;   
    [Tooltip("Прослушиваемое событие попытки противника атаковать игрока.")]
    public GameEventType onEnemyAttemptToAttack;

    //Вызываемые события
    [Tooltip("После окончания перемещения противника будет вызван GameEvent этого типа")]
    public GameEventType enemyMoveEndEvent;

    public GameEventType onStartTurnEvent;

    private Animator animator;

    void Awake() {
        Order = 16;
    }

    protected override void Start() {
        animator = GetComponent<Animator>();
        base.Start();
    }

    void OnEnable() {
        Register();
        RegisterGameObjectListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterGameObjectListener();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onEnemyAttemptToAttack && 
                db.enemiesList.list[db.currentEnemy.CurrentValue] == gameObject) {
            animator.SetTrigger("enemyAttack");
        }
    }

    public void OnGameObjectEventRaised(GameObject gObject, GameEventType type) {
        if (type == enemyMoveStartEvent && gObject == gameObject) {
            Debug.unityLogger.Log("Enemy received command to move!", db.currentEnemy.CurrentValue);
            StartCoroutine(base.Move(
                db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].pathSegments));
        }
    }

    protected override void DoAfterMoveCoroutine() {
        gameEvent.Raise(enemyMoveEndEvent);
    }

    public void RegisterGameObjectListener() {
        gameObjectEvent.RegisterListener(this);
    }

    public void UnregisterGameObjectListener() {
        gameObjectEvent.UnregisterListener(this);
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
