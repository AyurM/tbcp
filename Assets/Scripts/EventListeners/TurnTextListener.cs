﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TurnTextListener : MonoBehaviour, IGameEventListener {
    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public GameEvent gameEvent;
    [Tooltip("Прослушиваемое событие начала хода игрока")]
    public GameEventType onStartTurn;
    [Tooltip("Прослушиваемое событие конца хода игрока")]
    public GameEventType onEndTurn;

    public Color playerTurnColor;
    public Color enemyTurnColor;
    public GameObject turnTextPanel;
    public Text turnText;
    [Tooltip("Расстояние перемещения таблички при анимации появления")]
    public float slideDistance;
    [Tooltip("Время перемещения таблички при анимации появления")]
    public float slideTime;
    [Tooltip("Время жизни таблички")]
    public float lifeTime;
    [Tooltip("Начальная позиция таблички")]
    public Vector2 startPosition;
    
    private const string YOUR_TURN_TEXT = "YOUR TURN";
    private const string ENEMY_TURN_TEXT = "ENEMY TURN";
    private float inverseMoveTime;

    void Awake() {
        Order = 4;
    }

    void Start () {
        inverseMoveTime = 1f / slideTime;
    }

    void OnEnable() {
        Register();
    }

    void OnDisable() {
        Unregister();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onStartTurn || type == onEndTurn) {
            gameObject.transform.position = startPosition * gameObject.GetComponent<Canvas>().scaleFactor;
            Vector3 endPosition = (new Vector3(startPosition.x, 
                            startPosition.y - slideDistance) * gameObject.GetComponent<Canvas>().scaleFactor);
            turnTextPanel.SetActive(true);
            if (type == onStartTurn) {
                turnText.text = YOUR_TURN_TEXT;
                turnText.color = playerTurnColor;
                
            }
            else if (type == onEndTurn) {
                turnText.text = ENEMY_TURN_TEXT;
                turnText.color = enemyTurnColor;
            }
            StartCoroutine(ShowTurnText(endPosition));
        }       
    }

    //Убирает табличку после lifeTime секунд
    private IEnumerator ShowTurnText(Vector3 end) {
        Coroutine textSlide = StartCoroutine(TurnTextSlide(end));
        yield return new WaitForSeconds(lifeTime);
        turnTextPanel.SetActive(false);
        StopCoroutine(textSlide);
    }

    //Отвечает за плавное скольжение таблички
    private IEnumerator TurnTextSlide(Vector3 end) {
        float sqrRemainDistance = (gameObject.transform.position - end).sqrMagnitude;
        while (sqrRemainDistance > float.Epsilon) {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, end,
                inverseMoveTime * Time.deltaTime);
            sqrRemainDistance = (gameObject.transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
