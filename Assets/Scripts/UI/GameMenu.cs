﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour {

    public SODatabase db;

    public GameObject menuPanel;
    public GameObject uiPanel;
    public GameObject soundSettingsPanel;
    public GameObject inventoryPanel;
    public GameObject characterPanel;
    [Tooltip("Inventory Content ScrollView.")]
    public Transform inventoryContentPanel;

    public Slider musicSlider;
    public Slider sfxSlider;

    private void Start() {
        db.isMainMenuActive.value = false;
    }

    void Update () {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            if (soundSettingsPanel.activeInHierarchy) {
                OnBackSoundSettingsClick(); //для выхода из настроек звука нажатием Esc
                return;
            }
            if (inventoryPanel.activeInHierarchy) {
                OnInventoryClose();     //для выхода из окна инвентаря нажатием Esc
                return;
            }
            if (characterPanel.activeInHierarchy) {
                characterPanel.SetActive(false);     //для выхода из окна персонажа нажатием Esc
                return;
            }
            ToggleMainMenu();
        }		
	}

    private void ToggleMainMenu() {
        db.isMainMenuActive.value = !db.isMainMenuActive.value;
        uiPanel.SetActive(!db.isMainMenuActive.value);
        menuPanel.SetActive(db.isMainMenuActive.value);
        Time.timeScale = db.isMainMenuActive.value ? 0 : 1;
    }

    public void OnResumeClick() {
        ToggleMainMenu();
    }

    public void OnRestartClick() {
        ToggleMainMenu();
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public void OnSettingsClick() {
        menuPanel.SetActive(false);
        soundSettingsPanel.SetActive(true);
        musicSlider.value = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
        sfxSlider.value = PlayerPrefs.GetFloat("SfxVolume", 0.5f);
    }

    public void OnMusicVolumeChanged() {
        PlayerPrefs.SetFloat("MusicVolume", musicSlider.value);
    }

    public void OnSfxVolumeChanged() {
        PlayerPrefs.SetFloat("SfxVolume", sfxSlider.value);
    }

    public void OnBackSoundSettingsClick() {
        PlayerPrefs.Save();
        soundSettingsPanel.SetActive(false);
        menuPanel.SetActive(true);
    }

    public void OnInventoryClose() {
        for (int i = 0; i < inventoryContentPanel.childCount; i++) {
            Destroy(inventoryContentPanel.GetChild(i).gameObject);
        }
        db.selectedInventoryItem.CurrentValue = -1;
        inventoryPanel.SetActive(false);
    }    

    public void OnExitClick() {
        Application.Quit();
    }
}
