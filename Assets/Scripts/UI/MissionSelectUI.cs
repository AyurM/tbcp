﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionSelectUI : MonoBehaviour {

    public GameEvent gameEvent;

    [Tooltip("Вызываемое событие нажатия на кнопку окна персонажа.")]
    public GameEventType onShowCharacterInfoClick;

    public void OnCharacterButtonClick() {
        gameEvent.Raise(onShowCharacterInfoClick);
    }

}
