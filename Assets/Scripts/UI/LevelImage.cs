﻿using UnityEngine;
using UnityEngine.UI;

public class LevelImage : MonoBehaviour {

    public SODatabase db;

    public Image levelImage;
    public Text levelText;
    public float delay;

	void Start () {
        PrepareLevelImage();
        Invoke("HideLevelImage", delay);
    }
	
	private void PrepareLevelImage() {
        levelImage.gameObject.SetActive(true);
        levelText.text = "LEVEL " + db.levelNumber.CurrentValue;
    }

    private void HideLevelImage() {
        levelImage.gameObject.SetActive(false);
    }
}
