﻿using UnityEngine;
using UnityEngine.UI;

public class LootItemUI : MonoBehaviour {

    public SODatabase db;

    public Image itemImage;
    public Text itemName;
    public Text itemType;
    public Item lootItem;

    public void BindItem(Item item) {
        lootItem = item;
        itemImage.sprite = item.sprite;
        itemImage.color = item.itemRarity.Color;
        itemName.text = item.itemName;
        if(item.itemType == db.armorType) {
            itemType.text = ((Armor) item).armorType.typeName;
        } else if(item.itemType == db.weaponType) {
            itemType.text = ((Weapon)item).weaponType.typeName;
        } else if(item.itemType == db.ammoType) {
            itemType.text = db.ammoType.typeName;
        }
    }
}
