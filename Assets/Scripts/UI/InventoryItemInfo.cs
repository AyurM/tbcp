﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryItemInfo : MonoBehaviour {

    public SODatabase db;

    public Text itemName;
    public Text itemRarity;
    public Text itemDesc;
    public Text itemInfo;
    public Button equipButton;
    public Button dropButton;

    public GameEvent gameEvent;
    public IntEvent intEvent;

    //Вызываемые события
    [Tooltip("Событие, вызываемое перед сменой основного оружия.")]
    public GameEventType onBeforeWeaponEquip;
    [Tooltip("Вызываемое событие смены текущего оружия игрока.")]
    public GameEventType onWeaponEquip;
    [Tooltip("Вызываемое событие смены брони игрока.")]
    public GameEventType onArmorEquip;
    [Tooltip("Вызываемое событие смены слота быстрого доступа.")]
    public GameEventType onQuickSlotEquip;

    private Item item;

    private void Start() {
        equipButton.onClick.AddListener(OnEquipItemClick);
        dropButton.onClick.AddListener(OnDropItemClick);
        ClearItemInfo();
    }

    private void OnDisable() {
        ClearItemInfo();
    }

    public void ShowItemInfo(Item itemToShow) {
        equipButton.gameObject.SetActive(true);
        dropButton.gameObject.SetActive(true);
        if (item != itemToShow) {
            item = itemToShow;
            itemName.text = item.itemName;
            itemRarity.text = item.rarityName;
            Color tempColor = item.itemRarity.Color;
            tempColor.a = 1f;
            itemRarity.color = tempColor;
            itemDesc.text = item.description;
            itemInfo.text = item.ToString();
            equipButton.interactable = !itemToShow.isItemEquipped;
            dropButton.interactable = !itemToShow.isItemEquipped;
        }
    }

    private void OnEquipItemClick() {
        db.inventory.equipment.Equip(item);
        equipButton.interactable = false;
        dropButton.interactable = false;
        if (item.itemType == db.weaponType) {           
            gameEvent.Raise(onBeforeWeaponEquip);   //вызывается, чтобы FieldUIHandler корректно убрал подсветку ОА при нахождении в состоянии 5
            gameEvent.Raise(onWeaponEquip);
        } else if(item.itemType == db.armorType) {
            gameEvent.Raise(onArmorEquip);
        } else if (item.itemType == db.usableType) {
            gameEvent.Raise(onQuickSlotEquip);
        }
    }

    private void OnDropItemClick() {
        ClearItemInfo();
        intEvent.Raise(db.selectedInventoryItem);
        db.selectedInventoryItem.CurrentValue = -1;
    }

    private void ClearItemInfo() {
        item = null;
        itemName.text = "";
        itemRarity.text = "";
        itemDesc.text = "";
        itemInfo.text = "";
        equipButton.gameObject.SetActive(false);
        dropButton.gameObject.SetActive(false);
    }
}
