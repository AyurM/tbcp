﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MissionMarker : MonoBehaviour, IPointerUpHandler {

    public SODatabase db;

    public IntEvent intEvent;

    public void OnPointerUp(PointerEventData eventData) {
        int index = transform.GetSiblingIndex();
        Debug.unityLogger.Log("Mission number", index);
        db.missionNumber.CurrentValue = index;
        intEvent.Raise(db.missionNumber);
    }
}
