﻿using UnityEngine;
using UnityEngine.UI;

public class EquippedItems : MonoBehaviour, IGameEventListener {

    public const int MAIN_WEAPON_SLOT = 1;
    public const int SECOND_WEAPON_SLOT = 2;
    public const int BODY_ARMOR_SLOT = 3;
    public const int HELMET_SLOT = 4;
    public const int GLOVES_SLOT = 5;
    public const int BOOTS_SLOT = 6;
    public const int QUICK_SLOT = 7;
    public const int ADD_SLOT = 8;

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public GameObject inventoryPanel;
    public Button mainWeaponSlot;
    public Button secondaryWeaponSlot;
    public Button bodyArmorSlot;
    public Button helmetSlot;
    public Button bootsSlot;
    public Button glovesSlot;
    public Button quickSlot;
    public Image mainWeaponImage;
    public Image secondaryWeaponImage;
    public Image bodyArmorImage;
    public Image helmetImage;
    public Image bootsImage;
    public Image glovesImage;
    public Image quickSlotImage;

    public Sprite emptyWeaponSlot;
    public Sprite emptyBodyArmorSlot;
    public Sprite emptyHelmetSlot;
    public Sprite emptyBootsSlot;
    public Sprite emptyGlovesSlot;
    public Sprite emptyQuickSlot;

    [Tooltip("Панель со сведениями о предмете инвентаря.")]
    public InventoryItemInfo itemInfo;
    public InventoryUIListener inventoryUI;

    public GameEvent gameEvent;

    [Tooltip("Прослушиваемое событие нажатия на кнопку инвентаря.")]
    public GameEventType onShowInventoryClick;
    [Tooltip("Прослушиваемое событие смены текущего оружия игрока.")]
    public GameEventType onWeaponEquip;
    [Tooltip("Прослушиваемое событие смены брони игрока.")]
    public GameEventType onArmorEquip;
    [Tooltip("Прослушиваемое событие смены слота быстрого доступа.")]
    public GameEventType onQuickSlotEquip;

    private Color emptySlotColor = new Color(1f, 1f, 1f, 0.39f);
    private Button pressedSlot;

    void Awake() {
        Order = 15;
    }

    void OnEnable() {
        Register();
    }

    void OnDisable() {
        Unregister();
    }

    void Start() {
        mainWeaponSlot.onClick.AddListener(() => {
            pressedSlot = mainWeaponSlot;
            OnEquipmentItemClick();
        });
        secondaryWeaponSlot.onClick.AddListener(() => {
            pressedSlot = secondaryWeaponSlot;
            OnEquipmentItemClick();
        });
        bodyArmorSlot.onClick.AddListener(() => {
            pressedSlot = bodyArmorSlot;
            OnEquipmentItemClick();
        });
        helmetSlot.onClick.AddListener(() => {
            pressedSlot = helmetSlot;
            OnEquipmentItemClick();
        });
        bootsSlot.onClick.AddListener(() => {
            pressedSlot = bootsSlot;
            OnEquipmentItemClick();
        });
        glovesSlot.onClick.AddListener(() => {
            pressedSlot = glovesSlot;
            OnEquipmentItemClick();
        });
        quickSlot.onClick.AddListener(() => {
            pressedSlot = quickSlot;
            OnEquipmentItemClick();
        });
    }

    public void OnEventRaised(GameEventType type) {
        if (type == onShowInventoryClick) {
            BindEquipment();
        } else if(type == onWeaponEquip) {
            OnWeaponEquip();
        } else if(type == onArmorEquip) {
            OnArmorEquip();
        } else if(type == onQuickSlotEquip) {
            OnQSlotEquip();
        }
    }

    private void OnArmorEquip() {
        if (inventoryPanel.activeInHierarchy) {
            BindBodyArmor();
            BindHelmet();
            BindBoots();
            BindGloves();
        }
    }

    private void OnWeaponEquip() {
        if (inventoryPanel.activeInHierarchy) {
            BindMainWeapon();
        }
    }

    private void OnQSlotEquip() {
        if (inventoryPanel.activeInHierarchy) {
            BindQuickSlot();
        }
    }

    public void BindEquipment() {
        BindMainWeapon();
        BindSecondaryWeapon();
        BindBodyArmor();
        BindHelmet();
        BindBoots();
        BindGloves();
        BindQuickSlot();
    }

    private void BindHelmet() {
        if (db.inventory.equipment.helmet == null) {
            helmetImage.sprite = emptyHelmetSlot;
            helmetImage.color = emptySlotColor;
            helmetSlot.interactable = false;
        }
        else {
            helmetImage.sprite = db.inventory.equipment.helmet.sprite;
            helmetImage.color = db.inventory.equipment.helmet.itemRarity.Color;
            helmetSlot.interactable = true;
        }
    }

    private void BindGloves() {
        if (db.inventory.equipment.gloves == null) {
            glovesImage.sprite = emptyGlovesSlot;
            glovesImage.color = emptySlotColor;
            glovesSlot.interactable = false;
        }
        else {
            glovesImage.sprite = db.inventory.equipment.gloves.sprite;
            glovesImage.color = db.inventory.equipment.gloves.itemRarity.Color;
            glovesSlot.interactable = true;
        }
    }

    private void BindBoots() {
        if (db.inventory.equipment.boots == null) {
            bootsImage.sprite = emptyBootsSlot;
            bootsImage.color = emptySlotColor;
            bootsSlot.interactable = false;
        }
        else {
            bootsImage.sprite = db.inventory.equipment.boots.sprite;
            bootsImage.color = db.inventory.equipment.boots.itemRarity.Color;
            bootsSlot.interactable = true;
        }
    }

    private void BindBodyArmor() {
        if (db.inventory.equipment.bodyArmor == null) {
            bodyArmorImage.sprite = emptyBodyArmorSlot;
            bodyArmorImage.color = emptySlotColor;
            bodyArmorSlot.interactable = false;
        }
        else {
            bodyArmorImage.sprite = db.inventory.equipment.bodyArmor.sprite;
            bodyArmorImage.color = db.inventory.equipment.bodyArmor.itemRarity.Color;
            bodyArmorSlot.interactable = true;
        }
    }

    private void BindSecondaryWeapon() {
        if (db.inventory.equipment.secondaryWeapon == null) {
            secondaryWeaponImage.sprite = emptyWeaponSlot;
            secondaryWeaponImage.color = emptySlotColor;
            secondaryWeaponSlot.interactable = false;
        }
        else {
            secondaryWeaponImage.sprite = db.inventory.equipment.secondaryWeapon.sprite;
            secondaryWeaponImage.color = db.inventory.equipment.secondaryWeapon.itemRarity.Color;
            secondaryWeaponSlot.interactable = true;
        }
    }

    private void BindMainWeapon() {
        if (db.inventory.equipment.mainWeapon == null) {
            mainWeaponImage.sprite = emptyWeaponSlot;
            mainWeaponImage.color = emptySlotColor;
            mainWeaponSlot.interactable = false;
        } else {
            mainWeaponImage.sprite = db.inventory.equipment.mainWeapon.sprite;
            mainWeaponImage.color = db.inventory.equipment.mainWeapon.itemRarity.Color;
            mainWeaponSlot.interactable = true;
        }
    }

    private void BindQuickSlot() {
        if (db.inventory.equipment.quickSlot == null) {
            quickSlotImage.sprite = emptyQuickSlot;
            quickSlotImage.color = emptySlotColor;
            quickSlot.interactable = false;
        }
        else {
            quickSlotImage.sprite = db.inventory.equipment.quickSlot.sprite;
            quickSlotImage.color = db.inventory.equipment.quickSlot.itemRarity.Color;
            quickSlot.interactable = true;
        }
    }

    public void OnEquipmentItemClick() {
        Item item = null;
        if(pressedSlot == mainWeaponSlot) {
            item = db.inventory.equipment.mainWeapon;
        } else if(pressedSlot == secondaryWeaponSlot) {
            item = db.inventory.equipment.secondaryWeapon;
        }
        else if (pressedSlot == bodyArmorSlot) {
            item = db.inventory.equipment.bodyArmor;
        }
        else if (pressedSlot == helmetSlot) {
            item = db.inventory.equipment.helmet;
        }
        else if (pressedSlot == glovesSlot) {
            item = db.inventory.equipment.gloves;
        }
        else if (pressedSlot == bootsSlot) {
            item = db.inventory.equipment.boots;
        }
        else if (pressedSlot == quickSlot) {
            item = db.inventory.equipment.quickSlot;
        }

        if (item != null) {
            itemInfo.ShowItemInfo(item);
            inventoryUI.ChangeSelectedItem(db.inventory.items.IndexOf(item));
        }
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
