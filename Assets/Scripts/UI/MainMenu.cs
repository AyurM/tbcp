﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public SODatabase db;

    public GameObject mainPanel;
    public GameObject settingsPanel;

    public Text loadingText;
    public Button loadGameButton;
    public Slider musicSlider;
    public Slider sfxSlider;

    private void Start() {
        loadGameButton.interactable = db.levelNumber.CurrentValue > 1;
    }

    void Update () {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            if (settingsPanel.activeInHierarchy) {
                OnBackSettingsClick();
            } else {
                Application.Quit();
            }            
        }		
	}

    public void OnNewGameClick() {
        GetComponent<GameStateManager>().ResetGameState();
        mainPanel.SetActive(false);
        loadingText.gameObject.SetActive(true);
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void OnLoadGameClick() {
        if(db.levelNumber.CurrentValue == 1) {
            return;
        }
        mainPanel.SetActive(false);
        loadingText.gameObject.SetActive(true);
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void OnSettingClick() {
        mainPanel.SetActive(false);
        settingsPanel.SetActive(true);
        musicSlider.value = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
        sfxSlider.value = PlayerPrefs.GetFloat("SfxVolume", 0.5f);
    }

    public void OnBackSettingsClick() {
        PlayerPrefs.Save();
        settingsPanel.SetActive(false);
        mainPanel.SetActive(true);
    }

    public void OnMusicVolumeChanged() {
        PlayerPrefs.SetFloat("MusicVolume", musicSlider.value);
    }

    public void OnSfxVolumeChanged() {
        PlayerPrefs.SetFloat("SfxVolume", sfxSlider.value);
    }
}
