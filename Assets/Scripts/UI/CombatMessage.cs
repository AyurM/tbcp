﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CombatMessage : MonoBehaviour {

    public SODatabase db;

    [Tooltip("Сдвиг сообщения по оси Y")]
    public float messageOffset = 32;
    [Tooltip("Время для перемещения сообщения на 1 пиксель")]
    public float moveTime = 0.03f;
    [Tooltip("Время жизни сообщения")]
    public float lifeTime = 0.8f;
    [Tooltip("Максимальное расстояние движения сообщения")]
    public float travelDistance = 24;
    [Tooltip("Диапазон случайного сдвига сообщения по оси X")]
    public float randomOffsetRange = 12;

    private float inverseMoveTime;
    private static readonly Color HIT_COLOR = Color.blue;
    private static readonly Color PLAYER_HIT_COLOR = new Color(0.502f, 0f, 0f);
    private static readonly Color PLAYER_CRIT_HIT_COLOR = Color.red;
    private static readonly Color CRITICAL_HIT_COLOR = Color.green;
    private static readonly Color MISS_COLOR = Color.magenta;

    void Start () {
        inverseMoveTime = 1f / moveTime;
        Object.Destroy(gameObject, lifeTime);
    }

    public void PlayerMissed(string messageText) {
        //сообщение о промахе
        gameObject.GetComponent<Text>().text = messageText;
        gameObject.GetComponent<Text>().color = MISS_COLOR;
        gameObject.GetComponent<Text>().fontSize = 20;
        AdjustPosition(Camera.main.WorldToScreenPoint(
                            db.enemiesList.list[db.selectedEnemy.CurrentValue].transform.position));
    }

    public void PlayerMadeHit(string messageText) {
        //сообщение об обычном попадании
        gameObject.GetComponent<Text>().text = messageText;
        gameObject.GetComponent<Text>().color = HIT_COLOR;
        gameObject.GetComponent<Text>().fontSize = 28;
        AdjustPosition(Camera.main.WorldToScreenPoint(
                            db.enemiesList.list[db.selectedEnemy.CurrentValue].transform.position));
    }

    public void PlayerMadeCritHit(string messageText) {
        //сообщение о крите
        gameObject.GetComponent<Text>().text = messageText;
        gameObject.GetComponent<Text>().color = CRITICAL_HIT_COLOR;
        gameObject.GetComponent<Text>().fontSize = 38;
        AdjustPosition(Camera.main.WorldToScreenPoint(
                            db.enemiesList.list[db.selectedEnemy.CurrentValue].transform.position));
    }

    public void PlayerGainedXP(string messageText) {
        //сообщение о получении опыта
        gameObject.GetComponent<Text>().text = messageText;
        gameObject.GetComponent<Text>().color = CRITICAL_HIT_COLOR;
        gameObject.GetComponent<Text>().fontSize = 30;
        AdjustPosition(Camera.main.WorldToScreenPoint(db.playerPosition.value));
    }

    public void EnemyMissed(string messageText) {
        //сообщение о промахе по игроку
        gameObject.GetComponent<Text>().text = messageText;
        gameObject.GetComponent<Text>().color = CRITICAL_HIT_COLOR;
        gameObject.GetComponent<Text>().fontSize = 20;
        AdjustPosition(Camera.main.WorldToScreenPoint(db.playerPosition.value));
    }

    public void EnemyMadeHit(string messageText) {
        //сообщение об обычном попадании по игроку
        gameObject.GetComponent<Text>().text = messageText;
        gameObject.GetComponent<Text>().color = PLAYER_HIT_COLOR;
        gameObject.GetComponent<Text>().fontSize = 28;
        AdjustPosition(Camera.main.WorldToScreenPoint(db.playerPosition.value));
    }

    public void EnemyMadeCritHit(string messageText) {
        //сообщение о критическом попадании по игроку
        gameObject.GetComponent<Text>().text = messageText;
        gameObject.GetComponent<Text>().color = PLAYER_CRIT_HIT_COLOR;
        gameObject.GetComponent<Text>().fontSize = 38;
        AdjustPosition(Camera.main.WorldToScreenPoint(db.playerPosition.value));
    }

    private void AdjustPosition(Vector3 messagePos) {
        Random.InitState(System.DateTime.Now.Millisecond);
        //небольшой случайный сдвиг по координате Х
        float rndOffset = Random.Range(-randomOffsetRange, randomOffsetRange);

        gameObject.transform.SetParent(GameObject.Find("Canvas").transform);
        gameObject.transform.position = messagePos + new Vector3(rndOffset,
                                            messageOffset, 0);
        gameObject.transform.SetSiblingIndex(1);
        StartCoroutine(MessageFloat());
    }

    private IEnumerator MessageFloat() {
        Vector3 endPosition = gameObject.transform.position + new Vector3(0f,
                                            travelDistance, 0f);
        float sqrRemainDistance = (transform.position - endPosition).sqrMagnitude;

        while (sqrRemainDistance > float.Epsilon) {
            transform.position = Vector3.MoveTowards(transform.position, endPosition,
                inverseMoveTime * Time.deltaTime);
            sqrRemainDistance = (transform.position - endPosition).sqrMagnitude;
            yield return null;
        }
    }
}
