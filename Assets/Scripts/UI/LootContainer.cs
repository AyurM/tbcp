﻿using UnityEngine;

public class LootContainer : MonoBehaviour, IGameEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public GameEvent gameEvent;

    //Прослушиваемые события
    public GameEventType onPlayerMovementRefresh;
    public GameEventType onStartTurn;

    //Вызываемые события
    [Tooltip("Вызываемое событие перемещения игрока на клетку с лутом.")]
    public GameEventType onLootboxEnter;
    [Tooltip("Вызываемое событие ухода игрока от клетки с лутом.")]
    public GameEventType onLootboxExit;

    void Awake() {
        Order = 12;
        db.isOnLootbox.value = false;
    }

    void OnEnable() {
        Register();
    }

    void OnDisable() {
        Unregister();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onStartTurn || type == onPlayerMovementRefresh) {
            //на уровне нет лута
            if (gameObject.transform.childCount == 0) {
                return;
            }
            CheckLootboxEnter();
        }
    }

    //проверяет, не находится ли игрок на одной клетке с лутом
    private void CheckLootboxEnter() {
        for(int i = 0; i < gameObject.transform.childCount; i++) {
            if(db.playerPosition.value == gameObject.transform.GetChild(i).transform.position) {
                onLootboxEnter.extraGO = gameObject.transform.GetChild(i).gameObject;
                db.isOnLootbox.value = true;
                gameEvent.Raise(onLootboxEnter);
                return;
            }
        }
        if (db.isOnLootbox) {
            //будет выполняться, если игрок уходит от клетки с лутом
            db.isOnLootbox.value = false;
            gameEvent.Raise(onLootboxExit);
        }
    }

    /*Если на позиции location уже существует Lootbox, возвращает его GameObject.
      Если на позиции location нет Lootbox'ов, возвращает null. */
    public GameObject FindLootboxOnPosition(Vector3 location) {
        GameObject result = null;
        for (int i = 0; i < gameObject.transform.childCount; i++) {
            if (location == gameObject.transform.GetChild(i).transform.position) {
                result = gameObject.transform.GetChild(i).gameObject;
                return result;
            }
        }
        return result;
    }

    public void Register() {
        gameEvent.RegisterListener(this);
        //Debug.unityLogger.Log("Moving system listener registered!");
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
