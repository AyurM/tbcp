﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StatMeter : MonoBehaviour, IIntEventListener {

    [Tooltip("Отслеживаемая переменная")]
    public IntVariable stat;
    [Tooltip("Прослушиваемое событие изменения переменной")]
    public IntEvent intEvent;
    [Tooltip("Время анимации заполнения всей полосы")]
    public float animationTime = 0.3f;
    [Tooltip("Название переменной для отображения в интерфейсе")]
    public string statName;
    [Tooltip("Заполняемая полоса")]
    public Image statImage;
    [Tooltip("Текстовый UI-элемент")]
    public Text statText;
    [Tooltip("Нужно ли выделять низкое значение параметра")]
    public bool highlightLowValue;
    [Tooltip("Порог низкого значения")]
    [Range(0f,1f)]
    public float lowValueThreshold;

    void OnEnable() {
        RegisterIntListener();
    }

    void OnDisable() {
        UnregisterIntListener();
    }

    public void OnIntEventRaised(IntVariable variable) {
        if(variable == stat) {
            float amountToFill = Mathf.Clamp01(
                    Mathf.InverseLerp(0, stat.MaxValue, stat.CurrentValue));
            float deltaFill = amountToFill - statImage.fillAmount;
            float timeToFill = Mathf.Abs(deltaFill) * animationTime;
            if(statText != null) {

                if (highlightLowValue) {

                    if(stat.CurrentValue <= stat.MaxValue * lowValueThreshold) {
                        statText.text = statName + ": " + "<color=red>" +
                            stat.CurrentValue + "</color>" + "/" + stat.MaxValue;
                    } else {
                        statText.text = statName + ": " + stat.CurrentValue + "/" + stat.MaxValue;
                    }

                } else {
                    statText.text = statName + ": " + stat.CurrentValue + "/" + stat.MaxValue;
                }                
            }            
            StartCoroutine(SmoothFill(amountToFill, deltaFill, timeToFill));
        }
    }

    //Плавное заполнение полоски индикатора
    private IEnumerator SmoothFill(float amountToFill, float deltaFill, float fillingTime) {
        float inverseFillTime = 1f / fillingTime;
        while(!IsAmountFilled(amountToFill, deltaFill)) {
            statImage.fillAmount += deltaFill * inverseFillTime * Time.deltaTime;
            yield return null;
        }
    }

    //Проверка полосы индикатора в обоих направлениях
    private bool IsAmountFilled(float amountToFill, float deltaFill) {
        if(deltaFill <= 0) {
            return !(statImage.fillAmount > amountToFill + float.Epsilon);
        }
        return !(statImage.fillAmount < amountToFill - float.Epsilon);
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }
}
