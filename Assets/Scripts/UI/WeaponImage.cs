﻿using UnityEngine;

public class WeaponImage : MonoBehaviour {

    public SODatabase db;

    public GameEvent gameEvent;
    public GameEventType onWeaponIconClick;

    public void OnWeaponIconClick() {
        if (!db.isPlayerTurn.value) {
            return;
        }
        gameEvent.Raise(onWeaponIconClick);
    }
}
