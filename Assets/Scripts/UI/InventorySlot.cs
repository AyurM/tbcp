﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Outline))]
public class InventorySlot : MonoBehaviour {

    public Image itemImage;
    public Image itemEquippedImage;
    public GameObject quantityPanel;
    public Text quantityText;
    public Item slotItem;
    
    public void BindItem(Item item) {
        slotItem = item;
        itemImage.sprite = item.sprite;
        itemImage.color = item.itemRarity.Color;
        itemEquippedImage.gameObject.SetActive(item.isItemEquipped);
        if(item is StackableItem) {
            quantityPanel.SetActive(true);
            quantityText.text = ((StackableItem)item).quantity.ToString();
        } else {
            quantityPanel.SetActive(false);
        }
    }

    public void RefreshEquippedUI() {   
        itemEquippedImage.gameObject.SetActive(slotItem.isItemEquipped);
    }

    public void SetSelected(bool isSelected) {
        GetComponent<Outline>().enabled = isSelected;
    }

    public void EmptySlot(Sprite emptySprite) {
        slotItem = null;
        itemImage.sprite = emptySprite;
        itemImage.color = Color.white;
    }
}
