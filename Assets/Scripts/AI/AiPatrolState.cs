﻿using System.Collections;
using UnityEngine;

public class AiPatrolState : IAiState {
    private AiFSM aifsm;

    public AiFSM Aifsm {
        get {
            return aifsm;
        }
        set {
            aifsm = value;
        }
    }

    public AiPatrolState(AiFSM a) {
        aifsm = a;
    }

    public IEnumerator MakeTurn() {
        //Проверить радиус агрессии
        if (aifsm.IsPlayerInAggroRange()) {
            aifsm.SetState(aifsm.GetAggressiveState());
            aifsm.MakeTurn(aifsm.enemy);
            yield break;
        }

        if (aifsm.IsPositiveAp()) {
            yield return aifsm.StartCoroutine(CheckWeaponAmmoCoroutine());
        } else {
            aifsm.EndTurnAfterMove();
            yield break;
        }
    }

    private IEnumerator CheckWeaponAmmoCoroutine() {
        if (!aifsm.enemy.weapon.IsReloadable()) {
            yield return aifsm.StartCoroutine(MoveToRandomPositionCoroutine());
        }
        else {

            if (aifsm.IsEnoughAmmoToAttack()) {
                yield return aifsm.StartCoroutine(MoveToRandomPositionCoroutine());
            }
            else {

                if (aifsm.IsEnoughApToReload()) {
                    aifsm.ReloadWeapon();
                    yield return aifsm.StartCoroutine(AfterReloadCoroutine());
                }
                else {
                    yield return aifsm.StartCoroutine(AfterReloadCoroutine());
                }

            }
        }
    }

    private IEnumerator AfterReloadCoroutine() {
        if (aifsm.IsPositiveAp()) {
            yield return aifsm.StartCoroutine(MoveToRandomPositionCoroutine());
        }
        aifsm.EndTurnAfterMove();
    }

    private IEnumerator MoveToRandomPositionCoroutine() {
        aifsm.MoveToRandomPosition();
        yield return new WaitForSeconds(aifsm.delayBetweenActions);
        aifsm.EndTurnAfterMove();
    }
}
