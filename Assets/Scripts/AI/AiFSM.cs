﻿using System.Collections.Generic;
using UnityEngine;

public class AiFSM : MonoBehaviour {

    private const float LOW_HP_THRESHOLD = 0.3f;

    [Tooltip("Обрабатываются только ходы противников в радиусе enemyActionRadius" +
        " клеток от игрока.")]
    public int enemyActionRadius;
    [Tooltip("Пауза между действиями AI (атака/перемещение) в секундах.")]
    public float delayBetweenActions;

    public SODatabase db;

    public GameEvent gameEvent;
    public VectorThreeEvent vector3Event;

    //Вызываемые события
    [Tooltip("Событие, вызываемое перед началом хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("В конце хода противника будет вызван GameEvent этого типа.")]
    public GameEventType onEnemyTurnEnd;
    [Tooltip("При попытке противника атаковать игрока будет вызван GameEvent этого типа.")]
    public GameEventType onEnemyAttemptToAttack;
    [Tooltip("Вызываемое событие попытки AI найти позицию для атаки.")]
    public GameEventType onAIFindAttackPosition;
    [Tooltip("Вызываемое событие обновления области хода противника.")]
    public GameEventType onAIRefreshMoveTiles;
    public Vector3EventType enemyMoveType;

    [HideInInspector]
    public Enemy enemy;

    private IAiState patrolState;
    private IAiState aggressiveState;
    private IAiState currentState;

	void Start () {
        patrolState = new AiPatrolState(this);
        aggressiveState = new AiAggressiveState(this);
        currentState = patrolState;
    }

    public void MakeTurn(Enemy newEnemy) {
        enemy = newEnemy;
        if (IsEnemyInActionRadius()) { 
            enemy.GetComponent<SpriteRenderer>().color = Color.red;   //подсветить противника перед началом его хода
            StartCoroutine(currentState.MakeTurn());
            return;
        }
        EndTurn();
    }

    public void SetState(IAiState state) {
        currentState = state;
    }

    public IAiState GetPatrolState() {
        return patrolState;
    }

    public IAiState GetAggressiveState() {
        return aggressiveState;
    }

    public static float ManhattanDistance(Vector3 a, Vector3 b) {
        return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
    }

    public bool IsOnLowHp() {
        return enemy.currentHP <= enemy.maxHP * LOW_HP_THRESHOLD;
    }

    public bool IsPlayerInAggroRange() {
        float distanceFromPlayer = ManhattanDistance(db.playerPosition.value, enemy.transform.position);
        Debug.unityLogger.Log("SimpleEnemyAI - distanceFromPlayer", distanceFromPlayer);
        return distanceFromPlayer <= enemy.aggroRange;
    }

    public bool IsPositiveAp() {
        return enemy.currentAP > 0;
    }

    public bool IsEnoughApToReload() {
        return enemy.currentAP >= ((GunMagazine)enemy.weapon.magazine).apToReload;
    }

    public bool IsEnoughApToAttack() {
        if (enemy.weapon == null) {
            return false;
        }
        return enemy.currentAP >= enemy.weapon.GetCurrentAttackMode().apToAttack;
    }

    public bool IsEnoughAmmoToAttack() {
        return ((GunMagazine)enemy.weapon.magazine).currentAmmo >=
            enemy.weapon.GetCurrentAttackMode().ammoRequired;
    }

    public bool IsPlayerInAttackRange() {
        return db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].attackTiles
            .Contains(Vector3Int.RoundToInt(db.playerPosition.value));
    }

    public bool IsInPlayerAttackRange() {
        return db.playerMoveInfo.pathfindInfo.attackTiles.Contains(
            Vector3Int.RoundToInt(enemy.transform.position));
    }

    public void ReloadWeapon() {
        ((GunMagazine)enemy.weapon.magazine).Reload();
        enemy.currentAP -= ((GunMagazine)enemy.weapon.magazine).apToReload;
    }

    public void AttackPlayer() {
        gameEvent.Raise(onEnemyAttemptToAttack);
    }

    public bool CoverExists() {
        //выдать событие для обновления области хода,
        //на событие должна отреагировать Moving System.
        gameEvent.Raise(onAIRefreshMoveTiles);

        //найти все клетки, которые доступны для перемещения и не находятся в области атаки игрока
        List<KeyValuePair<int, Vector3Int>> possibleCovers = new List<KeyValuePair<int, Vector3Int>>();
        foreach (KeyValuePair<int, Vector3Int> pair in
                db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].tilesWithDistances) {
            if (!db.playerMoveInfo.pathfindInfo.attackTiles.Contains(pair.Value)) {
                possibleCovers.Add(pair);
            }
        }

        if (possibleCovers.Count == 0) {
            return false;
        }

        //отсортировать клетки по убыванию расстояния от игрока, чтобы
        //укрыться на самой дальней от игрока клетке
        possibleCovers.Sort((x, y) => ManhattanDistance(y.Value, db.playerPosition.value)
                .CompareTo(ManhattanDistance(x.Value, db.playerPosition.value)));
        db.enemyCoverPosition.value = possibleCovers[0].Value;
        return true;
    }

    public bool AttackPositionExists() {
        //максимальное расстояние, на которое можно переместиться так, чтобы остались AP на атаку
        int maxTravelDistance = enemy.currentAP - enemy.weapon.GetCurrentAttackMode().apToAttack;
        //клетки с расстоянием не более maxTravelDistance
        List<KeyValuePair<int, Vector3Int>> maxTravelArea =
            db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].GetMoveTilesWithMaxDistance(maxTravelDistance);
        Debug.unityLogger.Log("maxTravelArea size", maxTravelArea.Count);
        maxTravelArea.Sort((x, y) => x.Key.CompareTo(y.Key));   //отсортировать клетки по возрастанию расстояния
        foreach (KeyValuePair<int, Vector3Int> pair in maxTravelArea) {
            db.tempEnemyPosition.value = pair.Value;

            //выдать событие для расчета области атаки, доступной из позиции db.tempEnemyPosition.
            //на событие должна отреагировать Combat System.
            gameEvent.Raise(onAIFindAttackPosition);
            if (db.enemiesMoveInfo.tempAttackTiles
                .Contains(Vector3Int.RoundToInt(db.playerPosition.value))) {
                //найдена позиция, из которой можно атаковать игрока
                return true;
            }
        }
        return false;
    }

    public void MoveToRandomPosition() {
        List<Vector3Int> moveTiles = db.enemiesMoveInfo
             .moveInfos[db.currentEnemy.CurrentValue].GetMoveTiles();
        int seed = System.DateTime.Now.Millisecond;
        Random.InitState(seed);
        int randomIndex = Random.Range(0, moveTiles.Count);
        Debug.unityLogger.Log("SimpleEnemyAI - random tile position", moveTiles[randomIndex]);
        enemy.currentAP -= db.enemiesMoveInfo
            .moveInfos[db.currentEnemy.CurrentValue].GetTravelDistance(moveTiles[randomIndex]);
        vector3Event.Raise(moveTiles[randomIndex], enemyMoveType);        
    }

    public void MoveToCover() {
        Debug.unityLogger.Log("SimpleEnemyAI - MoveToCover", db.enemyCoverPosition.value);
        enemy.currentAP -= db.enemiesMoveInfo
            .moveInfos[db.currentEnemy.CurrentValue].GetTravelDistance(
                Vector3Int.RoundToInt(db.enemyCoverPosition.value));
        vector3Event.Raise(db.enemyCoverPosition.value, enemyMoveType);
        
    }

    public void MoveToAttackPosition() {
        Debug.unityLogger.Log("SimpleEnemyAI - MoveToAttackPosition", db.tempEnemyPosition.value);
        enemy.currentAP -= db.enemiesMoveInfo
            .moveInfos[db.currentEnemy.CurrentValue].GetTravelDistance(
                Vector3Int.RoundToInt(db.tempEnemyPosition.value));
        vector3Event.Raise(db.tempEnemyPosition.value, enemyMoveType);        
    }

    public void MoveAwayFromPlayer() {
        List<Vector3Int> moveTiles = db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].GetMoveTiles();
        //отсортировать клетки по убыванию расстояния от игрока, чтобы
        //отбежать на самую дальнюю от игрока клетку
        moveTiles.Sort((x, y) => ManhattanDistance(y, db.playerPosition.value)
                .CompareTo(ManhattanDistance(x, db.playerPosition.value)));
        Debug.unityLogger.Log("SimpleEnemyAI - MoveAwayFromPlayer", moveTiles[0]);
        enemy.currentAP -= db.enemiesMoveInfo
            .moveInfos[db.currentEnemy.CurrentValue].GetTravelDistance(
                Vector3Int.RoundToInt(moveTiles[0]));
        vector3Event.Raise(moveTiles[0], enemyMoveType);        
    }

    public void MoveToClosestPosition() {
        //TODO: вместо манхэттенского расстояния использовать кол-во AP для ортогонального пути
        List<Vector3Int> moveTiles = db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].GetMoveTiles();
        //отсортировать клетки по возрастанию расстояния до игрока, чтобы
        //подойти на самую ближнюю к игроку клетку
        moveTiles.Sort((x, y) => ManhattanDistance(x, db.playerPosition.value)
                .CompareTo(ManhattanDistance(y, db.playerPosition.value)));
        Debug.unityLogger.Log("SimpleEnemyAI - MoveToClosestPosition", moveTiles[0]);
        enemy.currentAP -= db.enemiesMoveInfo
            .moveInfos[db.currentEnemy.CurrentValue].GetTravelDistance(
                Vector3Int.RoundToInt(moveTiles[0]));
        vector3Event.Raise(moveTiles[0], enemyMoveType);        
    }

    public void EndTurnAfterMove() {
        enemy.GetComponent<SpriteRenderer>().color = Color.white; //снять подсветку сходившего противника
        EndTurn();
    }

    private void EndTurn() {
        if (db.currentEnemy.CurrentValue == db.enemiesList.list.Count - 1) {
            gameEvent.Raise(onEnemyTurnEnd);
            db.currentEnemy.CurrentValue = 0;
            gameEvent.Raise(onStartTurn);
            return;
        }
        gameEvent.Raise(onEnemyTurnEnd);
    }
   
    private bool IsEnemyInActionRadius() {
        return (ManhattanDistance(enemy.transform.position,
            db.playerPosition.value) <= enemyActionRadius);
    }
}
