﻿using System.Collections;

public interface IAiState{

    AiFSM Aifsm {
        get;
        set;
    }

    IEnumerator MakeTurn();
}
