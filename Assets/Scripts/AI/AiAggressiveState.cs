﻿using System.Collections;
using UnityEngine;

public class AiAggressiveState : IAiState {
    private AiFSM aifsm;

    public AiFSM Aifsm {
        get {
            return aifsm;
        }
        set {
            aifsm = value;
        }
    }

    private bool endTurnFlag;

    public AiAggressiveState(AiFSM a) {
        aifsm = a;
    }

    public IEnumerator MakeTurn() {
        endTurnFlag = false;

        //Проверить радиус агрессии
        if (!aifsm.IsPlayerInAggroRange()) {
            aifsm.SetState(aifsm.GetPatrolState());
            aifsm.MakeTurn(aifsm.enemy);
            yield break;
        }

        while (aifsm.IsPositiveAp() && !endTurnFlag) {

            if (aifsm.IsEnoughApToAttack()) {

                if (aifsm.enemy.weapon.IsReloadable()) {

                    if (aifsm.IsEnoughAmmoToAttack()) {

                        yield return aifsm.StartCoroutine(IsPlayerInAttackRangeCoroutine());
                    }
                    else {

                        if (aifsm.IsEnoughApToReload()) {
                            aifsm.ReloadWeapon();

                            if (aifsm.IsEnoughApToAttack()) {

                                yield return aifsm.StartCoroutine(IsPlayerInAttackRangeCoroutine());

                            }
                            else {

                                yield return aifsm.StartCoroutine(CoroutineIsInPlayerAttackRange());

                            }

                        }
                        else {

                            yield return aifsm.StartCoroutine(CoroutineIsInPlayerAttackRange());

                        }
                    }

                }
                else {

                    yield return aifsm.StartCoroutine(IsPlayerInAttackRangeCoroutine());

                }
            }
            else {

                yield return aifsm.StartCoroutine(CoroutineIsInPlayerAttackRange());
                
            }

            yield return null;
        }

        aifsm.EndTurnAfterMove();
        yield break;
    }    

    private IEnumerator AttackPlayerCoroutine() {
        aifsm.AttackPlayer();
        yield return new WaitForSeconds(aifsm.delayBetweenActions);
    }

    private IEnumerator MoveToAttackPositionCoroutine() {
        aifsm.MoveToAttackPosition();
        yield return new WaitForSeconds(aifsm.delayBetweenActions);
    }

    private IEnumerator MoveToClosestPositionCoroutine() {
        aifsm.MoveToClosestPosition();
        yield return new WaitForSeconds(aifsm.delayBetweenActions);
    }

    private IEnumerator MoveToCoverCoroutine() {
        aifsm.MoveToCover();
        yield return new WaitForSeconds(aifsm.delayBetweenActions);
    }

    private IEnumerator MoveAwayFromPlayerCoroutine() {
        aifsm.MoveAwayFromPlayer();
        yield return new WaitForSeconds(aifsm.delayBetweenActions);
    }

    private IEnumerator IsPlayerInAttackRangeCoroutine() {
        if (aifsm.IsPlayerInAttackRange()) {
            yield return aifsm.StartCoroutine(AttackPlayerCoroutine());
        }
        else if (aifsm.AttackPositionExists()) {
            yield return aifsm.StartCoroutine(MoveToAttackPositionCoroutine());
            yield return aifsm.StartCoroutine(AttackPlayerCoroutine());
        }
        else {
            yield return aifsm.StartCoroutine(MoveToClosestPositionCoroutine());
            endTurnFlag = true;
        }
    }

    private IEnumerator CoroutineIsInPlayerAttackRange() {
        if (aifsm.IsInPlayerAttackRange()) {

            if (aifsm.CoverExists()) {
                yield return aifsm.StartCoroutine(MoveToCoverCoroutine());
                endTurnFlag = true;
            }
            else {

                if (aifsm.IsOnLowHp()) {
                    yield return aifsm.StartCoroutine(MoveAwayFromPlayerCoroutine());
                    endTurnFlag = true;
                }
                else {
                    endTurnFlag = true;
                }
            }

        }
        else {
            endTurnFlag = true;
        }
    }
}
