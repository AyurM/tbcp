﻿using System.Collections.Generic;

[System.Serializable]
public class GameState {
 
    public int levelNumber;
    public int playerLevel;
    public int playerXP;
    public int playerHP;
    public List<string> inventory;

    public GameState() {
        levelNumber = 1;
        playerLevel = 1;
        playerXP = 0;
        inventory = new List<string>();
    }

    public bool IsNew() {
        return levelNumber == 1;
    }
}
