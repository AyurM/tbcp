﻿using System;

[Serializable]
public abstract class StackableItem : Item {

    public int quantity;
}
