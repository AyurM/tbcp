﻿using UnityEngine.Tilemaps;

public interface ITilemapsEventListener {

    void RegisterTilemapEventListener();

    void UnregisterTilemapEventListener();

    void OnTilemapsEventRaised(Tilemap wallMap, Tilemap floorMap, Tilemap obstacleMap);
}
