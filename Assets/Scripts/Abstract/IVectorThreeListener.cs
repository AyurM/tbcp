﻿using UnityEngine;

public interface IVectorThreeListener {

    void RegisterVectorThreeListener();

    void UnregisterVectorThreeListener();

    void OnVectorThreeEventRaised(Vector3 vector, Vector3EventType type);
}
