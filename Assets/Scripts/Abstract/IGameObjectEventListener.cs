﻿using UnityEngine;

public interface IGameObjectEventListener {

    void RegisterGameObjectListener();

    void UnregisterGameObjectListener();

    void OnGameObjectEventRaised(GameObject gameObject, GameEventType type);
}
