﻿public interface IBoolListener{

    void RegisterBoolListener();

    void UnregisterBoolListener();

    void OnBoolEventRaised(bool value);
}
