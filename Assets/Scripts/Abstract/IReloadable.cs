﻿public interface IReloadable {

    void Reload();

    bool HasAmmo();

    bool IsReloadable();

    int GetCurrentAmmo();

    void SpendAmmo(int ammoToSpend);

    int GetMaxAmmo();
}
