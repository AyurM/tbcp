﻿using UnityEngine;
using System;

[Serializable]
public abstract class Item {

    public string className;
    public string itemName;
    public string description;
    public string spritePath;
    public string itemTypeName;
    public string rarityName;
    public int itemLevel;
    public int minLevelToDrop;
    public bool isItemEquipped;
    public int equipmentSlot;
    [NonSerialized]
    public Sprite sprite;
    [NonSerialized]
    public ItemType itemType;
    [NonSerialized]
    public ItemRarity itemRarity;

    public abstract string ToJsonString();

    public abstract void Init();
}
