﻿using UnityEngine;

public abstract class StatScaler : ScriptableObject {

    public abstract int GetMaxHP(int currentLevel);

    public abstract int GetMaxAP(int currentLevel);

    public abstract int GetXPForNextLevel(int currentLevel);
}
