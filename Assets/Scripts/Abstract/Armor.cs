﻿using System;

[Serializable]
public abstract class Armor : Item {

    public int defense;
    public int hitReduction;
    public int shotReduction;
    public string armorTypeName;
    [NonSerialized]
    public ArmorType armorType;

    public override string ToString() {
        return "Defense: " + defense + Environment.NewLine +
            "Hit reduction: " + hitReduction + Environment.NewLine +
            "Shot reduction: " + shotReduction;
    }

}
