﻿using System.Collections.Generic;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public abstract class Weapon : Item {

    public const float MAX_CHANCE_TO_HIT = 0.95f;

    public int minDamage;
    public int maxDamage;
    public int attackRange;
    public float critChance;
    public float critMultiplier;
    public string weaponTypeName;
    [NonSerialized]
    public WeaponType weaponType;
    public List<AttackModePO> attackModes;
    public IReloadable magazine;    //отвечает за сведения о патронах в оружии (оружие может и не использовать патронов)

    private int currentAttackMode = 0;

    public bool IsCriticalHit() {
        Random.InitState(DateTime.Now.Millisecond);
        if (Random.Range(0.0f, 1.0f) <= critChance) {
            return true;
        }
        else {
            return false;
        }
    }

    //Рассчитывает средний базовый урон оружия
    public int GetBaseDamage() {
        Random.InitState(DateTime.Now.Millisecond);
        return Random.Range(minDamage, maxDamage + 1);
    }

    //Рассчитывает урон с учетом шанса критического попадания
    public virtual int GetAttackDamage(bool isCriticalHit) {
        int baseDamage = GetBaseDamage();
        if (isCriticalHit) {
            return Mathf.RoundToInt(baseDamage * critMultiplier);
        }
        else {
            return baseDamage;
        }
    }

    public bool IsReloadable() {
        return magazine.IsReloadable(); //false, если оружие не требует патронов
    }

    public bool HasAmmo() {
        if (!magazine.IsReloadable()) {
            return false;
        }
        else {
            return magazine.HasAmmo();
        }
    }

    //public override void Init() {
    //    for (int i = 0; i < attackModes.Count; i++) {
    //        attackModes[i].attackSound = AssetDatabase.LoadAssetAtPath<AudioClip>(
    //            attackModes[i].attackSoundClipPath);
    //    }
    //}

    public virtual void Reload() {
        if (magazine.IsReloadable()) {
            magazine.Reload();
        }
    }

    public virtual void SpendAmmo() {
        if (!magazine.IsReloadable()) {
            return;
        }
    }

    public AttackModePO GetCurrentAttackMode() {
        if(attackModes[currentAttackMode] == null) {
            Debug.unityLogger.Log("currentAttackMode is null!");
        }     
        return attackModes[currentAttackMode];
    }

    public void ChangeAttackMode() {
        currentAttackMode++;
        if (currentAttackMode >= attackModes.Count) {
            currentAttackMode = 0;
        }
    }

    public override string ToString() {
        return "Damage: " + minDamage + " - " + maxDamage + Environment.NewLine +
            "Attack range: " + attackRange + Environment.NewLine +
            "Critical chance: " + critChance * 100f + " %" + Environment.NewLine +
            "Critical damage: " + critMultiplier * 100f + " %" + Environment.NewLine +
            magazine.ToString();
    }
}
