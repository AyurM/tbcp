﻿public interface IIntEventListener {

    void RegisterIntListener();

    void UnregisterIntListener();

    void OnIntEventRaised(IntVariable variable);
}
