﻿public interface IUsable {
    int ApToUse {
        get;
        set;
    }

    void Use();
}
