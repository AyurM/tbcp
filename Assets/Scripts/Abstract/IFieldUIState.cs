﻿public interface IFieldUIState{

    FieldUIHandler FieldUI {
        get;
        set;
    }

    void OnMoveTileClick();

    void OnNeutralTileClick();

    void OnEnemyClick(int enemyIndex);

    void OnWeaponIconClick();

    void OnMovementStart();

    void OnMovementEnd();

    void OnEnemyKill();

    void OnWeaponEquip();

    void OnEndTurn();

    void OnStartTurn();
}
