﻿public interface IGameEventListener {

    int Order {
        get;
        set;
    }

    void Register();

    void Unregister();

    void OnEventRaised(GameEventType type);
}
