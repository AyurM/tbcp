﻿public interface IFloatEventListener{

    void RegisterFloatListener();

    void UnregisterFloatListener();

    void OnFloatEventRaised(FloatVariable variable);
}
