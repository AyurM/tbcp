﻿using UnityEngine;

[System.Serializable]
public class Mission {

    public Vector2 location;
    public int difficulty;
    public int size;
    public MissionType type;
    public MissionReward reward;

    public Mission(Vector2 loc, int diff, int s, MissionType t, MissionReward rew) {
        location = loc;
        difficulty = diff;
        size = s;
        type = t;
        reward = rew;
    }

    public string ToJsonString() {
        return JsonUtility.ToJson(this, true);
    }

    public override string ToString() {
        return "Location: " + location.ToString() + "; Difficulty: " + difficulty +
            "; Size: " + size + "; Type: " + type.ToString();
    }

    [System.Serializable]
    public class MissionReward {
        public int xpReward;
        public int creditsReward;

        public MissionReward(int xp, int credits) {
            xpReward = xp;
            creditsReward = credits;
        }

        public override string ToString() {
            return "+" + xpReward + " XP, +" + creditsReward + " credits";
        }
    }

    [System.Serializable]
    public enum MissionType { ELIMINATION, ASSASSINATION, SEARCH };
}
