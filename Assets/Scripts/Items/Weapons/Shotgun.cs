﻿using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class Shotgun : Weapon {

    public int currentAmmo;
    public int maxAmmo;
    public int reloadAp;
    public string reloadClipPath;
    [NonSerialized]
    public AudioClip reloadClip;

    public Shotgun(BaseShotgun p, ItemRarity itemRarity) {

        //базовые поля Item
        className = typeof(Shotgun).Name;
        itemLevel = 1;
        minLevelToDrop = p.baseMinLevelToDrop;
        itemName = p.baseName;
        description = p.baseDesc;
        sprite = p.baseSprite;
        //spritePath = AssetDatabase.GetAssetPath(image);
        itemTypeName = p.itemType.typeName;
        itemType = p.itemType;
        rarityName = itemRarity.Name;
        this.itemRarity = itemRarity;
        isItemEquipped = false;
        equipmentSlot = -1;

        //поля Weapon
        minDamage = p.baseMinDamage;
        maxDamage = p.baseMaxDamage;
        attackRange = p.baseAttackRange;
        critChance = p.baseCritChance;
        critMultiplier = p.baseCritMultiplier;
        weaponTypeName = p.weaponType.typeName;
        weaponType = p.weaponType;

        //обойма
        this.reloadClip = p.reloadSound;
        //reloadClipPath = AssetDatabase.GetAssetPath(reloadClip);
        currentAmmo = p.baseMaxAmmo;
        maxAmmo = p.baseMaxAmmo;
        this.reloadAp = p.baseApToReload;

        //Reloadable (у дробовиков есть обойма)
        magazine = new GunMagazine(currentAmmo, maxAmmo, reloadAp, reloadClip);

        //AttackModes
        attackModes = new List<AttackModePO>();
        for (int i = 0; i < p.baseAttackModes.Length; i++) {
            attackModes.Add(new AttackModePO(p.baseAttackModes[i]));
        }
    }

    //TODO: добавить в конструктор itemLevel

    public override void Init() {
        //base.Init();
        //reloadClip = AssetDatabase.LoadAssetAtPath<AudioClip>(reloadClipPath);
        magazine = new GunMagazine(currentAmmo, maxAmmo, reloadAp, reloadClip);
    }

    public override void SpendAmmo() {
        magazine.SpendAmmo(GetCurrentAttackMode().ammoRequired);
        currentAmmo -= GetCurrentAttackMode().ammoRequired;
    }

    public override void Reload() {
        base.Reload();
        currentAmmo = maxAmmo;
    }

    public override string ToJsonString() {
        return JsonUtility.ToJson(this, true);
    }
}
