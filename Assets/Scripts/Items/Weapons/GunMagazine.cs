﻿using UnityEngine;

[System.Serializable]
public class GunMagazine : IReloadable {

    public int currentAmmo;
    public int maxAmmo;
    public int apToReload;
    public AudioClip reloadSound;

    public GunMagazine(int cAmmo, int mAmmo, int apr, AudioClip rs) {
        currentAmmo = cAmmo;
        maxAmmo = mAmmo;
        apToReload = apr;
        reloadSound = rs;
    }

    public bool IsReloadable() {
        return true;
    }

    public void Reload() {
        //TODO: добавить проверку наличия подходящих патронов в инвентаре
        currentAmmo = maxAmmo;
    }

    public bool HasAmmo() {
        return currentAmmo > 0;
    }

    public override string ToString() {
        return "Ammo: " + currentAmmo + "/" + maxAmmo + System.Environment.NewLine +
               "AP to reload: " + apToReload;
    }

    public int GetCurrentAmmo() {
        return currentAmmo;
    }

    public void SpendAmmo(int ammoToSpend) {
        currentAmmo -= ammoToSpend;
    }

    public int GetMaxAmmo() {
        return maxAmmo; ;
    }
}
