﻿using UnityEngine;

[CreateAssetMenu(menuName = "Attack Mode")]
public class AttackMode : ScriptableObject {

    public string attackName;
    public string fullAttackName;
    public int apToAttack;
    public int ammoRequired;
    public AudioClip attackSound;
}
