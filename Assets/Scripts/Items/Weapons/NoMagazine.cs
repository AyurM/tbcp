﻿//Реализация IReloadable, не требующая патронов (например, для оружия ближнего боя)
[System.Serializable]
public class NoMagazine : IReloadable {

    public bool IsReloadable() {
        return false;
    }

    public void Reload() {
    }

    public bool HasAmmo() {
        return false;
    }

    public int GetCurrentAmmo() {
        return 0;
    }

    public void SpendAmmo(int ammoToSpend) {
        return;
    }

    public int GetMaxAmmo() {
        return 0; ;
    }

    public override string ToString() {
        return "";
    }
}
