﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MeleeWeapon : Weapon {

    public MeleeWeapon(BaseMeleeWeapon p, ItemRarity itemRarity) {

        //базовые поля Item
        className = typeof(MeleeWeapon).Name;
        itemLevel = 1;
        minLevelToDrop = p.baseMinLevelToDrop;
        itemName = p.baseName;
        description = p.baseDesc;
        sprite = p.baseSprite;
        //spritePath = AssetDatabase.GetAssetPath(image);
        itemTypeName = p.itemType.typeName;
        itemType = p.itemType;
        rarityName = itemRarity.Name;
        this.itemRarity = itemRarity;
        isItemEquipped = false;
        equipmentSlot = -1;

        //поля Weapon
        minDamage = p.baseMinDamage;
        maxDamage = p.baseMaxDamage;
        attackRange = p.baseAttackRange;
        critChance = p.baseCritChance;
        critMultiplier = p.baseCritMultiplier;
        weaponTypeName = p.weaponType.typeName;
        weaponType = p.weaponType;

        //Reloadable (у оружия ближнего боя нет обоймы)
        magazine = new NoMagazine();

        //AttackModes
        attackModes = new List<AttackModePO>();
        for (int i = 0; i < p.baseAttackModes.Length; i++) {
            attackModes.Add(new AttackModePO(p.baseAttackModes[i]));
        }
    }

    //TODO: добавить в конструктор itemLevel

    public override void Init() {
        //base.Init();
        magazine = new NoMagazine();
    }

    public override string ToJsonString() {
        return JsonUtility.ToJson(this, true);
    }
}
