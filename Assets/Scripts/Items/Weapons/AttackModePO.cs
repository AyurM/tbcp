﻿using UnityEngine;
using System;

[Serializable]
public class AttackModePO {

    public string attackName;
    public string fullAttackName;
    public int apToAttack;
    public int ammoRequired;
    public string attackSoundClipPath;
    [NonSerialized]
    public AudioClip attackSound;

    public AttackModePO(AttackMode attackModeSO) {
        attackName = attackModeSO.attackName;
        fullAttackName = attackModeSO.fullAttackName;
        apToAttack = attackModeSO.apToAttack;
        ammoRequired = attackModeSO.ammoRequired;
        attackSound = attackModeSO.attackSound;
        //attackSoundClipPath = AssetDatabase.GetAssetPath(attackModeSO.attackSound);
    }
}
