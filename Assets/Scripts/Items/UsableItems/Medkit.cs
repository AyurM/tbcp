﻿using UnityEngine;
using System;

[Serializable]
public class Medkit : StackableItem, IUsable {

    public int apToUse;
    public int ApToUse {
        get {
            return apToUse;
        }
        set {
            apToUse = value;
        }
    }

    public int hpToHeal;
    public AudioClip useSound;

    public Medkit(BaseMedkit bm, ItemRarity rarity) {
        //базовые поля Item
        className = typeof(Medkit).Name;
        itemLevel = 1;
        minLevelToDrop = bm.baseMinLevelToDrop;
        itemName = bm.baseName;
        description = bm.baseDesc;
        sprite = bm.baseSprite;
        //spritePath = AssetDatabase.GetAssetPath(image);
        itemTypeName = bm.itemType.typeName;
        itemType = bm.itemType;
        rarityName = rarity.Name;
        this.itemRarity = rarity;
        isItemEquipped = false;
        equipmentSlot = -1;

        //Stackable
        quantity = bm.quantity;

        //Medkit
        hpToHeal = bm.hpToHeal;
        apToUse = bm.apToUse;
        useSound = bm.useSound;
    }

    public void Use() {
        quantity--;
    }

    public override void Init() {
    }

    public override string ToString() {
        return "Quantity: " + quantity + Environment.NewLine +
            "Heal amount: " + hpToHeal + " HP" + Environment.NewLine +
            "AP to use: " + apToUse;
    }

    public override string ToJsonString() {
        return JsonUtility.ToJson(this, true);
    }
}
