﻿using UnityEngine;

[System.Serializable]
public class Boots : Armor {

    public Boots(BaseBoots b, ItemRarity rarity) {
        //базовые поля Item
        className = typeof(Boots).Name;
        itemName = b.baseName;
        minLevelToDrop = b.baseMinLevelToDrop;
        description = b.baseDesc;
        sprite = b.baseSprite;
        //spritePath = AssetDatabase.GetAssetPath(baSprite);
        itemTypeName = b.itemType.typeName;
        itemType = b.itemType;
        rarityName = rarity.Name;
        itemRarity = rarity;
        isItemEquipped = false;
        equipmentSlot = -1;

        //поля Armor
        defense = b.baseDefense;
        hitReduction = b.baseHitReduction;
        shotReduction = b.baseShotReduction;
        armorTypeName = b.armorType.typeName;
        armorType = b.armorType;

    }

    public override void Init() {
    }

    public override string ToJsonString() {
        return JsonUtility.ToJson(this, true);
    }
}
