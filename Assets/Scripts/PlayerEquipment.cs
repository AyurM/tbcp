﻿using System.Collections.Generic;

[System.Serializable]
public class PlayerEquipment {

    public Weapon mainWeapon;
    public Weapon secondaryWeapon;
    public BodyArmor bodyArmor;
    public Helmet helmet;
    public Boots boots;
    public Gloves gloves;
    public Item quickSlot;
    public Item addSlot;

    public void Equip(Item item) {
        item.isItemEquipped = true;
        if(item is Weapon) {
            EquipWeapon((Weapon)item);
        } else if(item is Armor) {
            EquipArmor((Armor)item);
        } else if(item is IUsable) {
            EquipUsable(item);
        }
    }

    public void EquipArmor(Armor armor) {
        if (armor is BodyArmor) {
            //снять ранее экипированный предмет
            if(bodyArmor != null) {
                bodyArmor.isItemEquipped = false;
                bodyArmor.equipmentSlot = -1;
            }
            bodyArmor = (BodyArmor)armor;
            armor.equipmentSlot = EquippedItems.BODY_ARMOR_SLOT;
        }
        else if (armor is Helmet) {
            if (helmet != null) {
                helmet.isItemEquipped = false;
                helmet.equipmentSlot = -1;
            }
            helmet = (Helmet)armor;
            armor.equipmentSlot = EquippedItems.HELMET_SLOT;
        }
        else if (armor is Boots) {
            if (boots != null) {
                boots.isItemEquipped = false;
                boots.equipmentSlot = -1;
            }
            boots = (Boots)armor;
            armor.equipmentSlot = EquippedItems.BOOTS_SLOT;
        }
        else if (armor is Gloves) {
            if (gloves != null) {
                gloves.isItemEquipped = false;
                gloves.equipmentSlot = -1;
            }
            gloves = (Gloves)armor;
            armor.equipmentSlot = EquippedItems.GLOVES_SLOT;
        }
    }

    public void EquipWeapon(Weapon weapon) {
        if(mainWeapon != null) {
            mainWeapon.isItemEquipped = false;
            mainWeapon.equipmentSlot = -1;
        }
        mainWeapon = weapon;
        weapon.equipmentSlot = EquippedItems.MAIN_WEAPON_SLOT;
    }

    public void EquipUsable(Item usable) {
        if(quickSlot != null) {
            quickSlot.isItemEquipped = false;
            quickSlot.equipmentSlot = -1;
        }
        quickSlot = usable;
        usable.equipmentSlot = EquippedItems.QUICK_SLOT;
    }

    public List<string> GetEquipmentList() {
        List<string> result = new List<string>();
        if(mainWeapon != null) {
            result.Add(mainWeapon.ToJsonString());
        }
        if (secondaryWeapon != null) {
            result.Add(secondaryWeapon.ToJsonString());
        }
        if (bodyArmor != null) {
            result.Add(bodyArmor.ToJsonString());
        }
        if (boots != null) {
            result.Add(boots.ToJsonString());
        }
        if (helmet != null) {
            result.Add(helmet.ToJsonString());
        }
        if (gloves != null) {
            result.Add(gloves.ToJsonString());
        }
        if(quickSlot != null) {
            result.Add(quickSlot.ToJsonString());
        }
        if(addSlot != null) {
            result.Add(addSlot.ToJsonString());
        }
        return result;
    }

    public int GetTotalHitReduction() {
        int result = 0;
        if (bodyArmor != null) {
            result += bodyArmor.hitReduction;
        }
        if (boots != null) {
            result += boots.hitReduction;
        }
        if (helmet != null) {
            result += helmet.hitReduction;
        }
        if (gloves != null) {
            result += gloves.hitReduction;
        }
        return result;
    }

    public int GetTotalShotReduction() {
        int result = 0;
        if (bodyArmor != null) {
            result += bodyArmor.shotReduction;
        }
        if (boots != null) {
            result += boots.shotReduction;
        }
        if (helmet != null) {
            result += helmet.shotReduction;
        }
        if (gloves != null) {
            result += gloves.shotReduction;
        }
        return result;
    }

}
