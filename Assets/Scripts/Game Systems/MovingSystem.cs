﻿using System.Collections.Generic;
using UnityEngine;

public class MovingSystem : MonoBehaviour, IGameEventListener, IVectorThreeListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    //События
    public GameEvent gameEvent;
    [Tooltip("Событие GameObjectEvent, позволяющее передавать GameObject в качестве параметра")]
    public GameObjectEvent gameObjectEvent;
    [Tooltip("Прослушиваемое событие инициации перемещения противников. Вместе с событием " +
        "передаются координаты конечной точки перемещения.")]
    public VectorThreeEvent vector3Event;
    public IntEvent intEvent;

    public Vector3EventType enemyMoveType;
    //Прослушиваемые события
    [Tooltip("Прослушиваемый тип события GameEvent для обработки одиночного нажатия.")]
    public GameEventType singleClickEvent;     
    [Tooltip("Прослушиваемый тип события GameEvent для обработки окончания перемещения игрока.")]
    public GameEventType onPlayerMovementEnd;
    [Tooltip("Прослушиваемый событие начала хода противника.")]
    public GameEventType onBeforeEnemyTurn;
    [Tooltip("Прослушиваемый тип события GameEvent для обработки окончания перемещения противника.")]
    public GameEventType onEnemyMovementEnd;
    [Tooltip("Прослушиваемый тип события убийства противника.")]
    public GameEventType onEnemyKilled;
    [Tooltip("Прослушиваемое событие промаха игрока.")]
    public GameEventType onPlayerMissed;
    [Tooltip("Прослушиваемое событие нанесения игроком критического удара.")]
    public GameEventType onPlayerMadeCritHit;
    [Tooltip("Прослушиваемое событие нанесения игроком обычного удара.")]
    public GameEventType onPlayerMadeHit;
    [Tooltip("Прослушиваемое событие перезарядки оружия игрока.")]
    public GameEventType onWeaponReload;
    [Tooltip("Прослушиваемое событие обновления области хода противника.")]
    public GameEventType onAIRefreshMoveTiles;
    [Tooltip("Прослушиваемое событие нажатия на слот быстрого доступа.")]
    public GameEventType onQuickSlotClick;

    //Вызываемые события
    [Tooltip("При нажатии на клетку, доступную для перемещения, будет вызван GameEvent этого типа.")]
    public GameEventType onMoveTileClick;
    [Tooltip("В начале перемещения игрока будет вызван GameEvent этого типа.")]
    public GameEventType onPlayerMovementStart;
    [Tooltip("Если после перемещения, у игрока остались AP, будет вызван GameEvent этого типа.")]
    public GameEventType onPlayerMovementRefresh;
    [Tooltip("В начале перемещения противника будет вызван GameObjectEvent данного типа.")]
    public GameEventType onEnemyMovementStart;
    [Tooltip("В начале хода игрока будет вызван GameEvent этого типа.")]
    public GameEventType onStartTurn;
    [Tooltip("В конце хода игрока будет вызван GameEvent этого типа.")]
    public GameEventType onEndTurn;

    void Awake() {        
        Order = 0;
    }

    void OnEnable() {
        Register();
        RegisterVectorThreeListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterVectorThreeListener();
    }

    void Start () {
        db.selectedTile.value = Vector3Int.one;        
        ResetMoveInfo();       
        gameEvent.Raise(onStartTurn);
    }

    public void OnEventRaised(GameEventType type) {
        if (type == singleClickEvent) {

            ProcessSingleClickEvent();

        } else if (type == onPlayerMovementEnd) {

            ProcessMovementEndEvent();

        } else if (type == onStartTurn) {

            ProccessStartTurn();

        } else if (type == onBeforeEnemyTurn) {
            //обновить сведения о доступных клетках перемещения для противника перед его ходом
            RefreshEnemyMoveInfo(db.currentEnemy.CurrentValue);

        } else if(type == onEnemyMovementEnd) {
            RefreshEnemyMoveInfo(db.currentEnemy.CurrentValue);
        } else if(type == onEnemyKilled || type == onPlayerMissed ||
                  type == onPlayerMadeHit || type == onPlayerMadeCritHit ||
                  type == onWeaponReload || type == onQuickSlotClick) {
            //после убийства противника для перемещения могут стать доступны новые клетки,
            //либо были потрачены AP на атаку/перезарядку
            RefreshPlayerMoveInfo();  //обновить ОХ   
        } else if(type == onAIRefreshMoveTiles) {
            RefreshEnemyMoveInfo(db.currentEnemy.CurrentValue);
            Debug.unityLogger.Log("onAIRefreshMoveTiles",
                db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].tilesWithDistances.Count);
        }
    }

    public void OnVectorThreeEventRaised(Vector3 endPosition, Vector3EventType type) {
        if(type == enemyMoveType) {
            //при получении запроса от противника на перемещение в клетку endPosition рассчитать
            //путь перемещения, разбить его на сегменты и вызвать событие перемещения противника
            int i = db.currentEnemy.CurrentValue;
            db.enemiesMoveInfo.moveInfos[i].pathTiles =
                MovingSystemPlain.GetOrthogonalPath(Vector3Int.RoundToInt(db.enemiesList.list[i].transform.position),
                                    Vector3Int.RoundToInt(endPosition), db.enemiesMoveInfo.moveInfos[i]);
            db.enemiesMoveInfo.moveInfos[i].pathSegments = MovingSystemPlain.GetPathSegments(
                    db.enemiesMoveInfo.moveInfos[i].pathTiles, Vector3Int.RoundToInt(db.enemiesList.list[i].transform.position));
            gameObjectEvent.Raise(db.enemiesList.list[i], onEnemyMovementStart);
        }
    }

    private void ProccessStartTurn() {
        db.isPlayerTurn.value = true;
        db.playerAP.CurrentValue = db.playerAP.MaxValue;
        intEvent.Raise(db.playerAP);       //Очки действия игрока (AP) обновились, выдать соответствующее событие
        RefreshPlayerMoveInfo();           //обновить ОХ       
    }

    private void ProcessSingleClickEvent() {
        //клик по игроку
        if (db.clickPosition.value == db.playerPosition.value) {
            //TODO: вставить действие при клике по игроку?
            return;
        }

        //нажатие по клетке из области хода игрока (ОХ), нажатия во время движения игрока игнорируются
        if (db.playerMoveInfo.pathfindInfo.MoveAreaContains(
                    Vector3Int.RoundToInt(db.clickPosition.value)) &&
                    !db.isPlayerMoving.value) {
            //Debug.unityLogger.Log("OnMoveTileClick event raised in moving system", db.clickPosition.value);
            db.isMoveTileClick.value = true;
            //сколько AP требуется для перемещения в эту клетку
            db.apToMove.CurrentValue = db.playerMoveInfo.pathfindInfo
                .GetTravelDistance(Vector3Int.RoundToInt(db.clickPosition.value));             
            gameEvent.Raise(onMoveTileClick); //выдать событие о нажатии на клетку ОХ
            HandleMovementInput(Vector3Int.RoundToInt(db.clickPosition.value));           
        }
        else {
            //нажатие было по клетке, не относящейся к области хода игрока
            db.isMoveTileClick.value = false;
        }
    }

    private void ProcessMovementEndEvent() {
        //Debug.unityLogger.Log("Moving system received OnPlayerMovementEnd event!");
        db.selectedTile.value = Vector3Int.one;    //сбросить выделение клетки ОХ
        if (db.playerAP.CurrentValue <= 0) {            
            gameEvent.Raise(db.enemiesList.list.Count > 0 ? onEndTurn : onStartTurn);  //не передавать ход противнику, если на уровне не осталось врагов
        } else {
            //у игрока остались AP, обновить область хода
            RefreshPlayerMoveInfo();
            gameEvent.Raise(onPlayerMovementRefresh);
        }
    }  

    //Обрабатывает нажатие на клетку, доступную для перемещения
    private void HandleMovementInput(Vector3Int moveTilePosition) {
        //повторный клик по той же клетке перемещает игрока
        if (db.selectedTile.value == moveTilePosition) {
            db.playerMoveInfo.pathfindInfo.pathTiles = MovingSystemPlain.GetOrthogonalPath(
                                Vector3Int.RoundToInt(db.playerPosition.value),
                                moveTilePosition, db.playerMoveInfo.pathfindInfo);     //рассчитать путь в клетку moveTilePosition
            db.playerMoveInfo.pathfindInfo.pathSegments = MovingSystemPlain.GetPathSegments(
                db.playerMoveInfo.pathfindInfo.pathTiles, Vector3Int.RoundToInt(db.playerPosition.value)); //разбить путь на прямолинейные сегменты
            //Debug.unityLogger.Log("Moving System raised player movement event!");
            db.playerAP.CurrentValue -= db.playerMoveInfo.pathfindInfo.GetTravelDistance(moveTilePosition);
            intEvent.Raise(db.playerAP);       //выдать событие изменения AP
            gameEvent.Raise(onPlayerMovementStart);  //выдать событие для начала перемещения игрока
        }
        else {
            //выбрать клетку для перемещения
            db.selectedTile.value = moveTilePosition;
        }
    }

    private List<Vector3Int> GetEnemiesPositions() {
        List<Vector3Int> result = new List<Vector3Int>();
        foreach(GameObject enemy in db.enemiesList.list) {
            result.Add(Vector3Int.RoundToInt(enemy.transform.position));
        }
        return result;
    }

    private void ResetMoveInfo() {
        db.playerMoveInfo.pathfindInfo = new PathfindInfo();
        db.enemiesMoveInfo.moveInfos = new List<PathfindInfo>();
    }

    private void RefreshPlayerMoveInfo() {
        MovingSystemPlain.GetMovePositions(Vector3Int.RoundToInt(db.playerPosition.value),
                        Vector3Int.RoundToInt(db.playerPosition.value),
                        db.playerAP.CurrentValue, ref db.playerMoveInfo.pathfindInfo,
                        db.floorTiles.list, db.obstacleTiles.list, GetEnemiesPositions());
    }

    //Обновляет сведения о доступных для перемещения клетках для противника с номером i
    private void RefreshEnemyMoveInfo(int i) {
        PathfindInfo enemyPathInfo;
        if (db.enemiesMoveInfo.moveInfos.Count <= i) {
            //первый ход противника
            enemyPathInfo = new PathfindInfo();
        }
        else {
            enemyPathInfo = db.enemiesMoveInfo.moveInfos[i];
        }

        MovingSystemPlain.GetMovePositions(Vector3Int.RoundToInt(db.enemiesList.list[i].transform.position),
                Vector3Int.RoundToInt(db.playerPosition.value),
                db.enemiesList.list[i].GetComponent<Enemy>().currentAP, ref enemyPathInfo,
                db.floorTiles.list, db.obstacleTiles.list, GetEnemiesPositions());

        if (db.enemiesMoveInfo.moveInfos.Count <= i) {
            //первый ход противника
            db.enemiesMoveInfo.moveInfos.Add(enemyPathInfo);
        }
    }

    //Регистрация/отписка слушателей
    public void RegisterVectorThreeListener() {
        vector3Event.RegisterListener(this);
    }

    public void UnregisterVectorThreeListener() {
        vector3Event.UnregisterListener(this);
    }

    public void Register() {
        gameEvent.RegisterListener(this);
        //Debug.unityLogger.Log("Moving system listener registered!");
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
