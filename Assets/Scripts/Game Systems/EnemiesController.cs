﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class EnemiesController : MonoBehaviour, IGameObjectEventListener, IGameEventListener {

    private const float LOW_HP_THRESHOLD = 0.3f;
    private int order;

    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;
    public AiFSM ai;

    [Tooltip("Событие без параметров")]
    public GameEvent gameEvent;
    [Tooltip("Событие, позволяющее передавать GameObject в качестве параметра")]
    public GameObjectEvent gameObjectEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемый тип события GameEvent для обработки одиночного нажатия.")]
    public GameEventType singleClickEvent;
    [Tooltip("Прослушиваемое события конца хода игрока.")]
    public GameEventType onEndTurn;
    [Tooltip("Прослушиваемый тип события GameEvent для обработки окончания перемещения противника.")]
    public GameEventType onEnemyMovementEnd;
    [Tooltip("Прослушиваемый тип события GameObjectEvent для обработки спавна противников.")]
    public GameEventType onEnemySpawned;
    [Tooltip("Прослушиваемое событие нанесения игроком критического удара.")]
    public GameEventType onPlayerMadeCritHit;
    [Tooltip("Прослушиваемое событие нанесения игроком обычного удара.")]
    public GameEventType onPlayerMadeHit;

    //Вызываемые события
    [Tooltip("При нажатии на противника будет вызван GameObjectEvent этого типа.")]
    public GameEventType onEnemyClick;
    [Tooltip("При нажатии на нейтральную клетку будет вызван GameEvent этого типа.")]
    public GameEventType onNeutralTileClick;   
    [Tooltip("Событие, вызываемое перед началом хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("Событие, вызываемое перед началом хода каждого из противников.")]
    public GameEventType onBeforeEnemyTurn;
    [Tooltip("В конце хода противника будет вызван GameEvent этого типа.")]
    public GameEventType onEnemyTurnEnd;
    [Tooltip("При попытке противника атаковать игрока будет вызван GameEvent этого типа.")]
    public GameEventType onEnemyAttemptToAttack;
    [Tooltip("Вызываемое событие попытки AI найти позицию для атаки.")]
    public GameEventType onAIFindAttackPosition;
    [Tooltip("Вызываемое событие обновления области хода противника.")]
    public GameEventType onAIRefreshMoveTiles;

    private Enemy enemy;

    private void Awake() {
        db.enemiesList.list = new List<GameObject>();
        db.currentEnemy.CurrentValue = 0;
        db.selectedEnemy.CurrentValue = -1;
        Order = 2;
    }

    void OnEnable() {
        Register();
        RegisterGameObjectListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterGameObjectListener();
    }

    public void OnGameObjectEventRaised(GameObject gameObject, GameEventType type) {
        if(type == onEnemySpawned) {
            //при спавне противника добавить его в список врагов
            db.enemiesList.list.Add(gameObject);
            //Debug.unityLogger.Log("EnemiesController received OnEnemySpawned event", gameObject);
        }
    }

    public void OnEventRaised(GameEventType type) {
        if (type == singleClickEvent && !db.isMoveTileClick.value) {
            ProcessSingleClick();
        } else if (type == onEndTurn) {
            OnEndTurn();
        } else if (type == onEnemyTurnEnd) {
            OnEnemyTurnEnd();
        } else if(type == onPlayerMadeHit || type == onPlayerMadeCritHit) {
            db.enemiesList.list[db.selectedEnemy.CurrentValue].GetComponent<Animator>()
                .SetTrigger("enemyAttack");
        }
    }

    private void OnEndTurn() {
        //Debug.unityLogger.Log("Enemies controller received OnEndTurn event!");
        db.isPlayerTurn.value = false;
        Debug.unityLogger.Log("OnEndTurn event, currentEnemy", db.currentEnemy.CurrentValue);
        enemy = db.enemiesList.list[db.currentEnemy.CurrentValue].GetComponent<Enemy>();
        MakeEnemyTurn();
    }

    private void OnEnemyTurnEnd() {
        if (db.currentEnemy.CurrentValue == db.enemiesList.list.Count - 1) {
            return;
        }
        db.currentEnemy.CurrentValue++;
        Debug.unityLogger.Log("OnEnemyTurnEnd event, currentEnemy", db.currentEnemy.CurrentValue);
        enemy = db.enemiesList.list[db.currentEnemy.CurrentValue].GetComponent<Enemy>();
        MakeEnemyTurn();
    }

    private void MakeEnemyTurn() {
        enemy.currentAP = enemy.maxAP;
        gameEvent.Raise(onBeforeEnemyTurn);     //выдать событие перед ходом каждого из противников
        StartCoroutine(MakeTurnWithDelay());
    }    

    private IEnumerator MakeTurnWithDelay() {
        yield return new WaitForSeconds(enemy.delayBeforeTurn);
        ai.MakeTurn(enemy);
    }

    private void ProcessSingleClick() {
        db.selectedEnemy.CurrentValue = IsEnemyPosition(Vector3Int.RoundToInt(db.clickPosition.value));
        if (db.selectedEnemy.CurrentValue != -1) {
            //Debug.unityLogger.Log("EnemiesController raised OnEnemyClick event", db.selectedEnemy.CurrentValue);
            gameObjectEvent.Raise(db.enemiesList.list[db.selectedEnemy.CurrentValue], onEnemyClick);
        }
        else {
            //Debug.unityLogger.Log("EnemiesController raised OnNeutralTileClick event");
            gameEvent.Raise(onNeutralTileClick);
        }
    }

    //Проверяет, расположен ли на данной клетке противник. При отсутствии
    //противника возвращает -1, при наличии - индекс противника в списке врагов.
    private int IsEnemyPosition(Vector3Int tilePosition) {
        for (int i = 0; i < db.enemiesList.list.Count; i++) {
            if (tilePosition == Vector3Int.RoundToInt(db.enemiesList.list[i].transform.position)) {
                return i;
            }
        }
        return -1;
    }

    public void RegisterGameObjectListener() {
        gameObjectEvent.RegisterListener(this);
    }

    public void UnregisterGameObjectListener() {
        gameObjectEvent.UnregisterListener(this);
    }

    public void Register() {
        gameEvent.RegisterListener(this);
        //Debug.unityLogger.Log("Enemies controller listener registered!");
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
