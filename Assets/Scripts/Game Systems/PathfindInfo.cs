﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//класс для хранения результатов поиска ортогонального пути
public class PathfindInfo {
    public List<Vector3Int> attackTiles;
    public List<Vector3Int> moveTiles;
    public List<Vector3Int> pathTiles;
    public List<Vector3Int> pathSegments;
    public List<KeyValuePair<int, Vector3Int>> tilesWithDistances;

    public PathfindInfo() {    
        attackTiles = new List<Vector3Int>();   //клетки, доступные для атаки (ОА)
        moveTiles = new List<Vector3Int>();     //клетки, доступные для атаки (ОХ)
        pathTiles = new List<Vector3Int>();     //клетки пути, проложенного в конечную клетку
        pathSegments = new List<Vector3Int>();  //путь разбивается на прямолинейные сегменты для анимации с огибанием препятствий
        tilesWithDistances = new List<KeyValuePair<int, Vector3Int>>(); //клетки, доступные для перемещения (ОХ) + расстояние до соответствующей клетки ОХ (кол-во очков действия)
    }

    //Возвращает кол-во AP, необходимых для перемещения в позицию tile.
    public int GetTravelDistance(Vector3Int tile) {
        if (tilesWithDistances == null || tilesWithDistances.Count == 0) {
            return -1;
        }
        foreach(KeyValuePair<int, Vector3Int> pair in tilesWithDistances) {
            if(pair.Value == tile) {
                return pair.Key;
            }
        }      
        return -1;
    }

    //Проверяет, содержится ли в ОХ клетка tile.
    public bool MoveAreaContains(Vector3Int tile) {
        if (tilesWithDistances == null || tilesWithDistances.Count == 0) {
            return false;
        }
        foreach (KeyValuePair<int, Vector3Int> pair in tilesWithDistances) {
            if (pair.Value == tile) {
                return true;
            }
        }
        return false;
    }

    //Возвращает клетки, расстояние до которых не более maxDistance AP.
    //Не включает текущую клетку (с нулевым расстоянием).
    public List<KeyValuePair<int, Vector3Int>> GetMoveTilesWithMaxDistance(int maxDistance) {
        return (from kvp in tilesWithDistances
                where (kvp.Key <= maxDistance) && (kvp.Key != 0)
                select kvp).ToList();
    }

    //Возвращает координаты клеток ОХ без сведений о расстоянии.
    public List<Vector3Int> GetMoveTiles() {
        return moveTiles;
    }
}
