﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputSystem : MonoBehaviour {

    public SODatabase db;

    [Tooltip("Количество мс, требуемых для удержания и перемещения камеры. Нажатия длительностью" +
        " меньше данного порога считаются одиночными кликами.")]
    public int cameraScrollThreshold = 300;
    public float cameraZoomSpeed;
    public float cameraMinOrthSize;
    public float cameraMaxOrthSize;

    public GameEvent gameEvent;
    public FloatEvent floatEvent;

    [Tooltip("При одиночном клике будет вызван GameEvent данного типа.")]
    public GameEventType singleClickEvent;

    [Tooltip("Событие, которое будет вызываться каждый кадр при удержании и перемещении курсора.")]
    public VectorThreeEvent vector3Event;
    public Vector3EventType cameraScrollType;

    private System.DateTime pressTime;
    private System.DateTime currentTime;
    private System.DateTime releaseTime;
    private System.DateTime resetDate;

    void Start () {
        resetDate = new System.DateTime(1, 1, 1);
        pressTime = resetDate;
        gameEvent.SortListeners();
        db.isPlayerDead.value = false;
        //gameEvent.ShowListeners();
    }

    void Update () {
        if (Input.touchCount == 2) {
            GetCameraZoomInput();
            return;
        }
        if (!db.isMainMenuActive.value && db.isPlayerTurn.value) {
            GetMouseEvent();
        }
    }

    private void GetCameraZoomInput() {
        if(db.levelColumns.CurrentValue <= LevelGenerator.MAX_ROOM_COLUMNS &&
            db.levelRows.CurrentValue <= LevelGenerator.MAX_ROOM_ROWS) {
            return;     //уровень слишком маленький, зум камеры не нужен
        }
        Touch touch0 = Input.GetTouch(0);
        Touch touch1 = Input.GetTouch(1);

        Vector2 touch0PrevPos = touch0.position - touch0.deltaPosition;
        Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;

        float prevTouchDeltaMag = (touch0PrevPos - touch1PrevPos).magnitude;
        float touchDeltaMag = (touch0.position - touch1.position).magnitude;
        float deltaMag = prevTouchDeltaMag - touchDeltaMag;

        db.cameraOrthSize.value = Mathf.Clamp(db.cameraOrthSize.value += deltaMag * cameraZoomSpeed,
            cameraMinOrthSize, cameraMaxOrthSize);
 
        floatEvent.Raise(db.cameraOrthSize);
    }

    private void GetMouseEvent() {       
        if (Input.GetMouseButtonDown(0) && !IsPointerOverUIObject()) {
            if (pressTime == resetDate) {
                pressTime = System.DateTime.Now;    //запомнить время первого нажатия
            }
        }

        currentTime = System.DateTime.Now;
        //разрешить скроллинг, если длительность нажатия больше cameraScrollThreshold
        if (pressTime != resetDate && currentTime
                .Subtract(pressTime).TotalMilliseconds >= cameraScrollThreshold) {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 0;
            vector3Event.Raise(mousePosition, cameraScrollType);
        }
        
        if (Input.GetMouseButtonUp(0)) {
            releaseTime = System.DateTime.Now;

            //для одиночного нажатия между нажатием и отжатием должно пройти не более
            //cameraScrollThreshold мс; не пропускать одиночные нажатия сквозь UI-элементы
            if (releaseTime.Subtract(pressTime).TotalMilliseconds <= cameraScrollThreshold &&
                !IsPointerOverUIObject()) {
                Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePosition.z = 0;
                db.clickPosition.value = Vector3Int.FloorToInt(mousePosition); //сохранить округленные координаты нажатия в SO
                //Debug.unityLogger.Log("Input system raised single click event", db.clickPosition.value);
                if (!db.isPlayerDead.value) {
                    //запретить клики по игровому полю, если игрок мертв
                    gameEvent.Raise(singleClickEvent);
                }                              
            }

            pressTime = resetDate;
        }
    }

    //позволяет отличать нажатия на UI-элементы от нажатий на игровое поле
    private bool IsPointerOverUIObject() {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current) {
            position = new Vector2(Input.mousePosition.x, Input.mousePosition.y)
        };
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
