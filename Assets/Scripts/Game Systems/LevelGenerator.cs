﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelGenerator : MonoBehaviour {

    public const int MIN_ROOM_COLUMNS = 12;
    public const int MIN_ROOM_ROWS = 8;
    public const int MAX_ROOM_COLUMNS = 25;
    public const int MAX_ROOM_ROWS = 20;
    public const int MAX_LEVEL_COLUMNS = 40;    //макс. ширина уровня
    public const int MAX_LEVEL_ROWS = 30;       //макс. высота уровня    
    public const int TILES_PER_ENEMY = 60;      //1 враг на TILES_PER_ENEMY клеток
    private const int LVLS_TO_MAX_SIZE = 40;    //за сколько уровней достигается уровень макс размера

    public SODatabase db;

    [Tooltip("Событие, которое будет выдаваться после создания нового уровня")]
    public TilemapsEvent levelCreatedEvent;

    public GameObject levelGrid;
    [Tooltip("Компонент, отвечающий за генерацию комнат на уровне")]
    public RoomGenerator roomGenerator;
    [Tooltip("Компонент, отвечающий за генерацию препятствий в комнатах")]
    public ObstacleGenerator obstacleGenerator;
    public int cameraOffset;

    private GameObject cinemachineCam;
    private PolygonCollider2D levelCollider;    //внешний коллайдер уровня для ограничения камеры Cinemachine
    private List<BspLeaf> levelRooms;

    void Start() {
        float levelArea = GetLevelArea(db.levelNumber.CurrentValue);
        SetLevelDimensions(levelArea);
        GameObject grid = Instantiate(levelGrid);
        cinemachineCam = GameObject.Find("CinemachineCam");
        levelCollider = grid.GetComponent<PolygonCollider2D>();        
        GenerateLevel(grid);
        AdjustLevelCollider(levelRooms);
    }

    //Создает коллайдер вокруг уровня для ограничения 
    //перемещения камеры Cinemachine
    private void AdjustLevelCollider(List<BspLeaf> rooms) {

        List<int> roomsBottom = new List<int>();
        List<int> roomsRight = new List<int>();
        List<int> roomsTop = new List<int>();
        List<int> roomsLeft = new List<int>();

        foreach (BspLeaf room in rooms) {
            roomsBottom.Add(room.leafPosition.y);
            roomsTop.Add(room.leafPosition.y + room.height);
            roomsLeft.Add(room.leafPosition.x);
            roomsRight.Add(room.leafPosition.x + room.width);           
        }

        roomsBottom.Sort();
        roomsTop.Sort();
        roomsLeft.Sort();
        roomsRight.Sort();

        //Рассчитать ограничения на скролл камеры Cinemachine
        db.cameraMinV.CurrentValue = roomsBottom[0];
        db.cameraMaxV.CurrentValue = roomsTop[roomsTop.Count - 1];
        db.cameraMinH.CurrentValue = roomsLeft[0];
        db.cameraMaxH.CurrentValue = roomsRight[roomsRight.Count - 1];
        
        levelCollider.points = new Vector2[] {
            new Vector2(db.cameraMinH.CurrentValue - cameraOffset, db.cameraMinV.CurrentValue - cameraOffset),
            new Vector2(db.cameraMaxH.CurrentValue + cameraOffset, db.cameraMinV.CurrentValue - cameraOffset),
            new Vector2(db.cameraMaxH.CurrentValue + cameraOffset,
                        db.cameraMaxV.CurrentValue + cameraOffset),
            new Vector2(db.cameraMinH.CurrentValue - cameraOffset, db.cameraMaxV.CurrentValue + cameraOffset)};
        cinemachineCam.GetComponent<Cinemachine.CinemachineConfiner>()
            .m_BoundingShape2D = levelCollider;
    }

    private void GenerateLevel(GameObject parentGrid) {
        db.floorTiles.list.Clear();
        db.wallTiles.list.Clear();
        db.obstacleTiles.list.Clear();
        levelRooms = roomGenerator.GenerateLevel();
        obstacleGenerator.GenerateObstacles(levelRooms);
        obstacleGenerator.PlaceEnemies(levelRooms);
        roomGenerator.wallTiles.transform.SetParent(parentGrid.transform);
        roomGenerator.floorTiles.transform.SetParent(parentGrid.transform);
        obstacleGenerator.obstacleTiles.transform.SetParent(parentGrid.transform);
        SetPlayerPosition(levelRooms);
        //Debug.unityLogger.Log("Level generator raised onLevelCreated event");
        levelCreatedEvent.Raise(roomGenerator.wallTiles, roomGenerator.floorTiles,
            obstacleGenerator.obstacleTiles);
    }

    private void SetPlayerPosition(List<BspLeaf> rooms) {
        Random.InitState(System.DateTime.Now.Millisecond);
        int rndIndex = Random.Range(0, rooms.Count);
        db.playerPosition.value = new Vector3Int(rooms[rndIndex].leafPosition.x + 1,
            rooms[rndIndex].leafPosition.y + 1, 0);
    }

    //Рассчитывает площадь уровня с номером level
    private float GetLevelArea(int level) {
        int minArea = MIN_ROOM_COLUMNS * MIN_ROOM_ROWS;
        int maxArea = MAX_ROOM_COLUMNS * MAX_ROOM_ROWS;
        //нелинейная зависимость площади от номера уровня
        float area = minArea + (maxArea - minArea) *
            (1 - Mathf.Pow((1 - ((float)(level - 1)) / ((LVLS_TO_MAX_SIZE - 1))), 2));
        return area;
    }

    /*Выставляет ширину/длину комнаты с площадью roomArea.
    Ширина/высота подбираются случайным образом с учетом ограничений 
    MIN_ROOM_COLUMNS, MAX_ROOM_COLUMNS, MIN_ROOM_ROWS, MAX_ROOM_ROWS*/
    private void SetLevelDimensions(float levelArea) {
        int minRows = Mathf.Max(MIN_ROOM_ROWS, Mathf.RoundToInt(levelArea / MAX_LEVEL_COLUMNS));
        int maxRows = Mathf.Min(MAX_LEVEL_ROWS, Mathf.RoundToInt(levelArea / MIN_ROOM_COLUMNS));
        int[] possibleColumns = new int[maxRows - minRows + 1];
        for (int i = 0; i < possibleColumns.Length; i++) {
            possibleColumns[i] = Mathf.RoundToInt(levelArea / (minRows + i));            
        }

        Random.InitState(System.DateTime.Now.Millisecond);
        int rndIndex = Random.Range(0, possibleColumns.Length);
        db.levelColumns.CurrentValue = possibleColumns[rndIndex];
        db.levelRows.CurrentValue = minRows + rndIndex;
        //Debug.unityLogger.Log("Random columns", db.levelColumns.CurrentValue);
        //Debug.unityLogger.Log("Random rows", db.levelRows.CurrentValue);
    }  
}
