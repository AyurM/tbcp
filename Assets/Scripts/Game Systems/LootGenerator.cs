﻿using System.Collections.Generic;
using UnityEngine;

public class LootGenerator : MonoBehaviour, IGameEventListener, IGameObjectEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;
    public LootGeneratorRules lootRules;

    public GameEvent gameEvent;
    [Tooltip("Событие, позволяющее передавать GameObject в качестве параметра")]
    public GameObjectEvent gameObjectEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемое событие убийства противника.")]
    public GameEventType onEnemyKilled;
    [Tooltip("Прослушиваемый тип события GameObjectEvent для обработки спавна противников.")]
    public GameEventType onEnemySpawned;

    //Вызываемые события
    [Tooltip("Вызываемое событие смены оружия при генерации/загрузке оружия.")]
    public GameEventType onWeaponEquip;
    [Tooltip("Вызываемое событие смены оружия при генерации/загрузке брони.")]
    public GameEventType onArmorEquip;
    [Tooltip("Вызываемое событие смены оружия при генерации/загрузке слота быстрого доступа.")]
    public GameEventType onQuickSlotEquip;

    [Tooltip("Объект для хранения генерируемого на уровне лута.")]
    public GameObject lootContainer;
    [Tooltip("Префаб ящика с лутом.")]
    public GameObject lootboxPrefab;

    void Awake() {
        Order = 11;
    }

    void OnEnable() {
        Register();
        RegisterGameObjectListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterGameObjectListener();
    }

    private void Start() {       
        if (db.inventory.items == null || db.inventory.items.Count == 0) {
            db.inventory.ResetInventory();

            //дать игроку случайное оружие
            Weapon weapon = GetRandomWeapon(1);
            db.inventory.equipment.mainWeapon = GetRandomWeapon(1);
            weapon.isItemEquipped = true;
            db.inventory.AddItem(weapon);

            //дать игроку случайную броню
            Armor armor = GetRandomArmor(1);            
            armor.isItemEquipped = true;
            db.inventory.AddItem(armor);

            //дать игроку случайный вспомогательный предмет
            Item util = GetRandomUtility(1);
            util.isItemEquipped = true;
            db.inventory.AddItem(util);
        }
        gameEvent.Raise(onWeaponEquip);
        gameEvent.Raise(onArmorEquip);
        gameEvent.Raise(onQuickSlotEquip);
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onEnemyKilled) {
            OnEnemyKill();         
        }
    }

    public void OnGameObjectEventRaised(GameObject gObject, GameEventType type) {
        if (type == onEnemySpawned) {
            Enemy spawnedEnemy = gObject.GetComponent<Enemy>();
            spawnedEnemy.weapon = GetRandomWeapon(spawnedEnemy.enemyLevel);
        }
    }

    public Item GetRandomItem(int dropLevel) {
        return lootRules.GetRandomItem(dropLevel);
    }

    public Item GetRandomUtility(int dropLevel) {
        return lootRules.GetRandomUtilItem(dropLevel);
    }

    public Weapon GetRandomWeapon(int dropLevel) {
        return lootRules.GetRandomWeapon(dropLevel);
    }

    public Armor GetRandomArmor(int dropLevel) {
        return lootRules.GetRandomArmor(dropLevel);
    }

    public Item GetItem(int baseItem, int rarityIndex, int dropLevel) {
        return lootRules.GetItem(baseItem, rarityIndex, dropLevel);
    }

    private void OnEnemyKill() {
        Enemy killedEnemy = onEnemyKilled.extraGO.GetComponent<Enemy>();
        GameObject lootboxGameObj = Instantiate(lootboxPrefab, killedEnemy.transform.position,
            Quaternion.identity);
        int itemsToGenerate = killedEnemy.GetNumberOfItemsToDropOnDeath();

        Lootbox lootbox = lootboxGameObj.GetComponent<Lootbox>();
        lootbox.lootList = new List<Item> { killedEnemy.weapon };   //с убитого врага всегда падает его оружие
        lootbox.lootSource = killedEnemy.enemyName;
        //TODO: настроить dropLevel
        for (int i = 0; i < itemsToGenerate; i++) {
            lootbox.lootList.Add(GetRandomItem(1));
        }
        lootboxGameObj.transform.SetParent(lootContainer.transform);
    }

    //Регистрация/отписка слушателей
    public void Register() {
        gameEvent.RegisterListener(this);
        //Debug.unityLogger.Log("Combat system listener registered!");
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }

    public void RegisterGameObjectListener() {
        gameObjectEvent.RegisterListener(this);
    }

    public void UnregisterGameObjectListener() {
        gameObjectEvent.UnregisterListener(this);
    }
}
