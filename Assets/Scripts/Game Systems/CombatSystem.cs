﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class CombatSystem : MonoBehaviour, ITilemapsEventListener, IGameEventListener {
    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;
    public GameObject popUpMessagePrefab;   //префаб всплывающего текста
    
    //События
    public GameEvent gameEvent;
    [Tooltip("Прослушиваемое событие создания уровня. Вместе с событием передается" +
        " информация о тайлах стен, пола, препятствий")]
    public TilemapsEvent tilemapsEvent;
    [Tooltip("Вызываемое событие активации/деактивации кнопки атаки.")]
    public BoolEvent attackButtonToggleEvent;
    public IntEvent intEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемое событие начала хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("Прослушиваемое событие окончания перемещения игрока.")]
    public GameEventType onPlayerMovementEnd;
    public GameEventType onPlayerMovementRefresh;
    [Tooltip("Прослушиваемое событие нажатия на кнопку атаки.")]
    public GameEventType onAttackButtonClick;
    [Tooltip("Прослушиваемое событие нажатия на кнопку перезарядки.")]
    public GameEventType onReloadButtonClick;
    [Tooltip("Прослушиваемое событие нажатия на кнопку смены режима атаки оружия.")]
    public GameEventType onAttackModeButtonClick;
    [Tooltip("Прослушиваемое событие нажатия на слот быстрого доступа.")]
    public GameEventType onQuickSlotClick;
    [Tooltip("Прослушиваемое событие смены текущего оружия игрока.")]
    public GameEventType onWeaponEquip;
    [Tooltip("Прослушиваемый событие начала хода противника.")]
    public GameEventType onBeforeEnemyTurn;
    [Tooltip("Прослушиваемый тип события GameEvent для обработки окончания перемещения противника.")]
    public GameEventType onEnemyMovementEnd;
    [Tooltip("Прослушиваемое событие попытки противника атаковать игрока.")]
    public GameEventType onEnemyAttemptToAttack;
    [Tooltip("Прослушиваемое событие попытки AI найти позицию для атаки.")]
    public GameEventType onAIFindAttackPosition;

    //Вызываемые события
    [Tooltip("Перед расчетом атаки игрока будет вызвано событие этого типа")]
    public GameEventType onBeforePlayerAttack;
    [Tooltip("Перед перезарядкой игрока будет вызвано событие этого типа")]
    public GameEventType onBeforeWeaponReload;
    [Tooltip("Событие, вызываемое после перезарядки оружия.")]
    public GameEventType onWeaponReload;
    [Tooltip("Вызываемое событие смены режима атаки оружия.")]
    public GameEventType onAttackModeChanged;
    [Tooltip("Вызываемое событие промаха игрока.")]
    public GameEventType onPlayerMissed;
    [Tooltip("Вызываемое событие при нанесении игроком критического удара.")]
    public GameEventType onPlayerMadeCritHit;
    [Tooltip("Вызываемое событие при нанесении игроком обычного удара.")]
    public GameEventType onPlayerMadeHit;
    [Tooltip("Вызываемое событие смерти игрока.")]
    public GameEventType onPlayerDeath;
    [Tooltip("Вызываемое событие промаха противника по игроку.")]
    public GameEventType onEnemyMissed;
    [Tooltip("Вызываемое событие при нанесении врагом критического удара по игроку.")]
    public GameEventType onEnemyMadeCritHit;
    [Tooltip("Вызываемое событие при нанесении врагом обычного удара по игроку.")]
    public GameEventType onEnemyMadeHit;
    [Tooltip("Вызываемое событие убийства противника.")]
    public GameEventType onEnemyKilled;
    [Tooltip("В конце хода игрока будет вызван GameEvent этого типа.")]
    public GameEventType onEndTurn;
    [Tooltip("После убийства всех врагов будет вызван GameEvent этого типа.")]
    public GameEventType onLevelComplete;

    //private Tilemap wallTilemap;      //может понадобиться в дальнейшем
    private Tilemap floorTilemap;
    private Tilemap obstacleTilemap;

    private GameObject killedEnemy;

    void Awake() {
        Order = 1;
        db.isLevelComplete.value = false;
    }

    void OnEnable() {
        Register();
        RegisterTilemapEventListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterTilemapEventListener();
    }

    //TODO: на первом ходу расчет ОА происходит до создания коллайдеров в obstacleTilemap, 
    //т.е. до первого шага физики
    public void OnEventRaised(GameEventType type) {
        if (type == onStartTurn || type == onPlayerMovementRefresh ||
            type == onEndTurn) {
            RefreshAttackTiles();
        }
        else if (type == onAttackButtonClick) {
            PlayerAttemptToAttack();
        }
        else if (type == onReloadButtonClick) {
            ReloadWeapon();
        }
        else if (type == onAttackModeButtonClick) {
            ChangePlayerAttackMode();
        } else if(type == onQuickSlotClick) {
            OnQSlotClick();
        }
        else if (type == onPlayerMissed) {
            OnPlayerMissed();
        }
        else if (type == onPlayerMadeHit) {
            OnPlayerMadeHit();
        }
        else if (type == onPlayerMadeCritHit) {
            OnPlayerMadeCritHit();
        }
        else if (type == onEnemyKilled) {
            OnEnemyKilled();
        }
        else if (type == onWeaponEquip) {
            if (floorTilemap != null && obstacleTilemap != null) {
                RefreshAttackTiles();
            }
        }
        else if (type == onEnemyAttemptToAttack) {
            EnemyAttemptToAttack();
        }
        else if (type == onBeforeEnemyTurn || type == onEnemyMovementEnd) {
            RefreshEnemyAttackTiles();
        } 
        else if (type == onAIFindAttackPosition) {
            FindPossibleAttackTiles();
        }
        else if (type == onEnemyMissed) {
            OnEnemyMissed();
        }
        else if (type == onEnemyMadeHit) {
            OnEnemyMadeHit();
        }
        else if (type == onEnemyMadeCritHit) {
            OnEnemyMadeCritHit();
        }
    }

    public void OnTilemapsEventRaised(Tilemap wallMap, Tilemap floorMap, Tilemap obstacleMap) {
        //Debug.unityLogger.Log("onLevelCreated event received in Combat System", tilemapsEvent);
        //wallTilemap = wallMap;
        floorTilemap = floorMap;
        obstacleTilemap = obstacleMap;
    }

    private void PlayerAttemptToAttack() {
        gameEvent.Raise(onBeforePlayerAttack);
        SpendPlayerAmmo();
        SpendPlayerAP();
        if (IsAttackSuccesful()) {
            //игрок попал по противнику
            bool isCrit = db.inventory.equipment.mainWeapon.IsCriticalHit();
            db.damageDealt.CurrentValue = db.inventory.equipment.mainWeapon.GetAttackDamage(isCrit);

            db.enemiesList.list[db.selectedEnemy.CurrentValue].GetComponent<Enemy>()
                .currentHP -= db.damageDealt.CurrentValue; //применить нанесение урона

            if (isCrit) {
                gameEvent.Raise(onPlayerMadeCritHit);
            } else {
                gameEvent.Raise(onPlayerMadeHit);
            }
            
            //проверка убийства противника
            if(db.enemiesList.list[db.selectedEnemy.CurrentValue].GetComponent<Enemy>()
                .currentHP <= 0) {
                killedEnemy = db.enemiesList.list[db.selectedEnemy.CurrentValue];
                db.enemiesList.list.RemoveAt(db.selectedEnemy.CurrentValue);
                onEnemyKilled.extraGO = killedEnemy;
                gameEvent.Raise(onEnemyKilled);
                Object.Destroy(killedEnemy);
                db.selectedEnemy.CurrentValue = -1;
                CheckLevelCompletion();
            }
        } else {
            //игрок промахнулся по противнику
            gameEvent.Raise(onPlayerMissed);
        }

        if(db.playerAP.CurrentValue <= 0) {
            //отложенный вызов конца хода позволяет дождаться окончания анимации всплывающего сообщения
            Invoke("EndTurn", popUpMessagePrefab.GetComponent<CombatMessage>().lifeTime * 1.5f);            
        }
    }

    private void EnemyAttemptToAttack() {
        Enemy enemy = db.enemiesList.list[db.currentEnemy.CurrentValue].GetComponent<Enemy>();
        SpendEnemyAmmo(enemy);
        SpendEnemyAP(enemy);
        if (IsEnemyAttackSuccesful()) {
            //противник попал по игроку            
            bool isCrit = enemy.weapon.IsCriticalHit();
            db.damageDealt.CurrentValue = enemy.weapon.GetAttackDamage(isCrit);
            //Debug.unityLogger.Log("Initial dmg from enemy", db.damageDealt.CurrentValue);

            MitigateDamageFromEnemy(enemy); //учесть броню игрока

            //Debug.unityLogger.Log("Mitigated dmg from enemy", db.damageDealt.CurrentValue);

            if (isCrit) {
                gameEvent.Raise(onEnemyMadeCritHit);
            } else {
                gameEvent.Raise(onEnemyMadeHit);
            }

            db.playerHP.CurrentValue -= db.damageDealt.CurrentValue; //применить нанесение урона
            if(db.damageDealt.CurrentValue > 0) {
                intEvent.Raise(db.playerHP);
            }
            
            //проверка смерти игрока
            if (db.playerHP.CurrentValue <= 0) {
                db.isPlayerDead.value = true;
                //Debug.unityLogger.Log("OnPlayerDeath event is raised in Combat System!");
                gameEvent.Raise(onPlayerDeath);
                db.levelNumber.CurrentValue = 1;
            }
        } else {
            gameEvent.Raise(onEnemyMissed);
            //Debug.unityLogger.Log("Enemy tried to attack the player, but missed!");
        }
        gameEvent.Raise(onEnemyMovementEnd);    //для пересчета ОА и ОХ после попытки атаки, т.к. были потрачены АР
    }

    private void SpendPlayerAmmo() {
        if (!db.inventory.equipment.mainWeapon.IsReloadable()) {
            return;
        } else {
            db.inventory.equipment.mainWeapon.SpendAmmo();
            if(db.inventory.equipment.mainWeapon.magazine.GetCurrentAmmo() 
                < db.inventory.equipment.mainWeapon.GetCurrentAttackMode().ammoRequired) {
                attackButtonToggleEvent.Raise(false);   //отключить кнопку атаки, если не хватает патронов
            }
        }
    }

    private void SpendEnemyAP(Enemy enemy) {        
        enemy.currentAP -= enemy.weapon.GetCurrentAttackMode().apToAttack;
    }

    private void SpendEnemyAmmo(Enemy enemy) {
        if (!enemy.weapon.IsReloadable()) {
            return;
        }
        enemy.weapon.SpendAmmo();
    }

    private void SpendPlayerAP() {
        db.playerAP.CurrentValue -= db.inventory.equipment.mainWeapon.GetCurrentAttackMode().apToAttack;
        intEvent.Raise(db.playerAP);       //выдать событие изменения AP
    }

    private void ReloadWeapon() {
        gameEvent.Raise(onBeforeWeaponReload);
        db.playerAP.CurrentValue -= ((GunMagazine)db.inventory.equipment.mainWeapon.magazine).apToReload;
        intEvent.Raise(db.playerAP);       //выдать событие изменения AP
        db.inventory.equipment.mainWeapon.Reload();
        gameEvent.Raise(onWeaponReload);
        if (db.playerAP.CurrentValue <= 0) {
            gameEvent.Raise(db.enemiesList.list.Count > 0 ? onEndTurn : onStartTurn); //не передавать ход противнику, если на уровне не осталось врагов
        }
    }

    private void MitigateDamageFromEnemy(Enemy enemy) {
        if(enemy.weapon is MeleeWeapon) {
            db.damageDealt.CurrentValue = Mathf.Max(0, db.damageDealt.CurrentValue -
            db.inventory.equipment.GetTotalHitReduction());
        } else if(enemy.weapon is Pistol || enemy.weapon is Shotgun) {
            db.damageDealt.CurrentValue = Mathf.Max(0, db.damageDealt.CurrentValue -
            db.inventory.equipment.GetTotalShotReduction());
        }        
    }

    private void RefreshAttackTiles() {
        db.playerMoveInfo.pathfindInfo.attackTiles = GetAttackTiles(Vector3Int.RoundToInt(db.playerPosition.value),
                db.inventory.equipment.mainWeapon.attackRange);
        //Debug.unityLogger.Log("Combat System - attackTiles.Count",
        //    db.playerMoveInfo.pathfindInfo.attackTiles.Count);
    }

    private void FindPossibleAttackTiles() {
        //рассчитать область атаки, которая будет доступна противнику, если он
        //будет находиться в клетке db.tempEnemyPosition
        db.enemiesMoveInfo.tempAttackTiles = GetAttackTiles(Vector3Int.RoundToInt(db.tempEnemyPosition.value),
                db.enemiesList.list[db.currentEnemy.CurrentValue]
                    .GetComponent<Enemy>().weapon.attackRange);
    }

    private void RefreshEnemyAttackTiles() {
        db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].attackTiles = 
            GetAttackTiles(Vector3Int.RoundToInt(db.enemiesList.list[db.currentEnemy.CurrentValue].transform.position),
                db.enemiesList.list[db.currentEnemy.CurrentValue]
                    .GetComponent<Enemy>().weapon.attackRange);
    }

    private void ChangePlayerAttackMode() {
        db.inventory.equipment.mainWeapon.ChangeAttackMode();

        //настроить кликабельность кнопки атаки - проверить выбран ли враг
        //и находится ли данный враг в области атаки
        if(db.selectedEnemy.CurrentValue != -1 && db.playerMoveInfo.pathfindInfo.attackTiles.Contains(
                    Vector3Int.RoundToInt(
                        db.enemiesList.list[db.selectedEnemy.CurrentValue].transform.position))) {
            bool isAbleToAttack = db.playerAP.CurrentValue >=
                db.inventory.equipment.mainWeapon.GetCurrentAttackMode().apToAttack;
            attackButtonToggleEvent.Raise(isAbleToAttack);
        }
        gameEvent.Raise(onAttackModeChanged);
    }

    //Конец хода вынесен в отдельную функцию для того, чтобы дождаться
    //окончания анимации всплывающего сообщения о промахе/нанесении урона
    private void EndTurn() {
        gameEvent.Raise(db.enemiesList.list.Count > 0 ? onEndTurn : onStartTurn); //не передавать ход противнику, если на уровне не осталось врагов
    }

    private bool IsAttackSuccesful() {
        int seed = System.DateTime.Now.Millisecond;
        Random.InitState(seed);
        return (Random.Range(0.0f, 1.0f) <= GetChanceToHit());
    }

    private bool IsEnemyAttackSuccesful() {
        int seed = System.DateTime.Now.Millisecond;
        Random.InitState(seed);
        return (Random.Range(0.0f, 1.0f) <= 0.75f);
    }

    private float GetChanceToHit() {
        //TODO: добавить механику расчета вероятности попадания
        return 0.95f;
    }

    private void OnPlayerMissed() {
        GameObject dmgText = Instantiate(popUpMessagePrefab);
        dmgText.GetComponent<CombatMessage>().PlayerMissed("MISSED");
    }

    private void OnPlayerMadeHit() {      
        GameObject dmgText = Instantiate(popUpMessagePrefab);
        dmgText.GetComponent<CombatMessage>().PlayerMadeHit(db.damageDealt.CurrentValue.ToString());        
    }

    private void OnPlayerMadeCritHit() {
        GameObject dmgText = Instantiate(popUpMessagePrefab);
        dmgText.GetComponent<CombatMessage>().PlayerMadeCritHit(db.damageDealt.CurrentValue.ToString());        
    }

    private void OnEnemyMissed() {
        GameObject dmgText = Instantiate(popUpMessagePrefab);
        dmgText.GetComponent<CombatMessage>().EnemyMissed("MISSED");
    }

    private void OnEnemyMadeHit() {
        GameObject dmgText = Instantiate(popUpMessagePrefab);
        dmgText.GetComponent<CombatMessage>().EnemyMadeHit(db.damageDealt.CurrentValue.ToString());       
    }

    private void OnEnemyMadeCritHit() {
        GameObject dmgText = Instantiate(popUpMessagePrefab);
        dmgText.GetComponent<CombatMessage>().EnemyMadeCritHit(db.damageDealt.CurrentValue.ToString());        
    }

    private void OnEnemyKilled() {
        GameObject dmgText = Instantiate(popUpMessagePrefab);
        dmgText.GetComponent<CombatMessage>().PlayerGainedXP(
            "+" + killedEnemy.GetComponent<Enemy>().xpForKill + " XP");      
        GainXP(killedEnemy.GetComponent<Enemy>().xpForKill);        
    }

    private void CheckLevelCompletion() {
        if(db.enemiesList.list.Count == 0) {
            db.isLevelComplete.value = true;
            gameEvent.Raise(onLevelComplete);
        }
    }

    private void GainXP(int amount) {
        if (db.playerXP.CurrentValue + amount >= db.playerXP.MaxValue) {
            int xpRemainder = amount - (db.playerXP.MaxValue - db.playerXP.CurrentValue);
            db.playerXP.CurrentValue = db.playerXP.MaxValue;

            //LevelUp
            db.playerLevel.CurrentValue++;
            intEvent.Raise(db.playerLevel);

            //начислить остаток опыта
            db.playerXP.CurrentValue = 0;
            //за одно начисление опыта нельзя повысить более одного уровня
            db.playerXP.CurrentValue += Mathf.Min(xpRemainder, db.playerXP.MaxValue - 1);
            intEvent.Raise(db.playerXP);
            return;
        }

        db.playerXP.CurrentValue += amount;
        intEvent.Raise(db.playerXP);
    }

    private void HealPlayer(Medkit medkit) {
        db.playerHP.CurrentValue = Mathf.Min(db.playerHP.CurrentValue + medkit.hpToHeal,
            db.playerHP.MaxValue);
        intEvent.Raise(db.playerHP);
        GameObject dmgText = Instantiate(popUpMessagePrefab);
        dmgText.GetComponent<CombatMessage>().PlayerGainedXP(
            "+" + medkit.hpToHeal + " HP");
    }

    private void OnQSlotClick() {
        Item qSlot = db.inventory.equipment.quickSlot;
        
        if(qSlot is Medkit) {
            HealPlayer((Medkit)qSlot);
        }
    }

    //Рассчитывает позиции клеток, доступных для атаки из attackerPosition
    //при дальности атаки attackRange.
    private List<Vector3Int> GetAttackTiles(Vector3Int attackerPosition, int attackRange) {
        List<Vector3Int> result = new List<Vector3Int>();
        Vector3Int checkTile = Vector3Int.zero;
        //В отличие от области хода область атаки рассчитывается как окрестность Мура
        for (int i = -attackRange; i <= attackRange; i++) {
            for (int j = -attackRange; j <= attackRange; j++) {
                checkTile.x = attackerPosition.x + i;
                checkTile.y = attackerPosition.y + j;
                if (floorTilemap.HasTile(checkTile) && !obstacleTilemap.HasTile(checkTile)
                        && CheckLoS(attackerPosition, checkTile)) {
                    result.Add(checkTile);
                }
            }
        }
        return result;
    }

    //Проверяет наличие Line of Sight между startPositon и endPosition.
    private bool CheckLoS(Vector3Int startPositon, Vector3Int endPosition) {
        int obstacles;
        RaycastHit2D[] raycastHits = new RaycastHit2D[2];
        Debug.DrawLine(startPositon, endPosition, Color.red, 5f);
        obstacles = Physics2D.LinecastNonAlloc(new Vector2(startPositon.x + 0.5f, startPositon.y + 0.5f),
            new Vector2(endPosition.x + 0.5f, endPosition.y + 0.5f), raycastHits);
        /* Всегда будет обнаружен хотя бы один коллайдер - PolygonCollider2D вокруг стен уровня, 
         * ограничивающий камеру Cinemachine. Данный коллайдер создается в LevelGenerator, 
         * метод AdjustLevelCollider(). Если обнаружены другие коллайдеры, то они
         * относятся к тайлам препятствий или стен, и LoS быть не должно. */
        //for(int i = 0; i < obstacles; i++) {
        //    Debug.unityLogger.Log("CheckLoS", raycastHits[i].collider);
        //}
        return obstacles == 1;
    }

    //Регистрация/отписка слушателей
    public void Register() {
        gameEvent.RegisterListener(this);
        //Debug.unityLogger.Log("Combat system listener registered!");
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }

    public void RegisterTilemapEventListener() {
        //Debug.unityLogger.Log("Combat system - levelCreated", levelCreated);
        tilemapsEvent.RegisterListener(this);
    }

    public void UnregisterTilemapEventListener() {
        tilemapsEvent.UnregisterListener(this);
    }
}
