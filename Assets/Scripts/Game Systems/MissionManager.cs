﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MissionManager : MonoBehaviour, IIntEventListener {

    public SODatabase db;

    [Tooltip("Максимальное количество одновременно доступных миссий.")]
    public int missionsAvailable;
    public GameObject redMarkerPrefab;
    public GameObject blueMarkerPrefab;
    public GameObject greenMarkerPrefab;
    public GameObject missionInfoPanel;
    [Tooltip("Map Content ScrollView.")]
    public Transform mapContentPanel;

    public Text missionTypeText;
    public Text missionDifficultyText;
    public Text missionSizeText;
    public Text missionRewardText;

    public IntEvent intEvent;

    private List<Mission> missions;
    private int mapWidth;
    private int mapHeight;

    void OnEnable() {
        RegisterIntListener();
    }

    void OnDisable() {
        UnregisterIntListener();
    }

    void Start () {
        Random.InitState(System.DateTime.Now.Millisecond);

        missions = new List<Mission>();
        mapWidth = (int)mapContentPanel.GetComponent<RectTransform>().rect.width;
        mapHeight = (int)mapContentPanel.GetComponent<RectTransform>().rect.height;
        Debug.unityLogger.Log("Map width x height", mapWidth + " x " + mapHeight);

        Mission.MissionReward reward = new Mission.MissionReward(500, 1000);

        for(int i = 0; i < missionsAvailable; i++) {
            Vector2 location = new Vector2 {
                x = Random.Range(- mapWidth / 2, mapWidth / 2),
                y = Random.Range(- mapHeight / 2, mapHeight / 2)
            };
            int type = Random.Range(0, 3);
            Mission mission = new Mission(location, 1, 1, (Mission.MissionType)type, reward);
            PlaceMissionMarker(mission);
            missions.Add(mission);
            Debug.unityLogger.Log("Created mission", mission.ToString());
        }
    }

    public void OnIntEventRaised(IntVariable variable) {
        if (variable == db.missionNumber) {
            Debug.unityLogger.Log("Mission Select event received!");
            missionInfoPanel.SetActive(true);
            //первым элементом в ScrollView является MapImage, поэтому индекс больше на 1
            ShowMissionInfo(missions[variable.CurrentValue - 1]);   
        }
    }

    public void OnMissionInfoClose() {
        missionInfoPanel.SetActive(false);
    }

    private void PlaceMissionMarker(Mission mission) {
        GameObject marker;
        switch (mission.type) {
            case Mission.MissionType.ELIMINATION:
                marker = Instantiate(redMarkerPrefab, 
                    new Vector3(mission.location.x, mission.location.y, 0),
                    Quaternion.identity);                
                break;
            case Mission.MissionType.ASSASSINATION:
                marker = Instantiate(blueMarkerPrefab,
                    new Vector3(mission.location.x, mission.location.y, 0),
                    Quaternion.identity);
                break;
            default:
                marker = Instantiate(greenMarkerPrefab,
                    new Vector3(mission.location.x, mission.location.y, 0),
                    Quaternion.identity);
                break;
        }
        marker.transform.SetParent(mapContentPanel, false);
    }

    private void ShowMissionInfo(Mission mission) {
        missionTypeText.text = mission.type.ToString();
        missionDifficultyText.text = "Difficulty: " + mission.difficulty;
        missionSizeText.text = "Size: " + mission.size;
        missionRewardText.text = "Reward: " + mission.reward.ToString();
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }

}
