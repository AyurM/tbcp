﻿using UnityEngine;

//Третье состояние UI игрового поля:
//- область хода подсвечена;
//- область атаки не подсвечена;
//- клетка для перемещения подсвечена;
//- инфо о противнике показывается.

public class FieldUIStateThree : IFieldUIState {
    private FieldUIHandler fieldUI;

    public FieldUIHandler FieldUI {
        get {
            return fieldUI;
        }
        set {
            fieldUI = value;
        }
    }

    public FieldUIStateThree(FieldUIHandler fui) {
        fieldUI = fui;
    }

    public void OnMoveTileClick() {
        //подсветить другую клетку для перемещения и показать требуемое кол-во AP
        //(остаться в состоянии 3)
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.selectedTile.value),
            FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.clickPosition.value),
            FieldUIHandler.SELECTED_TILE_COLOR);
        fieldUI.apToMoveToggleEvent.Raise(true);
    }

    public void OnNeutralTileClick() {
        //убрать подсветку клетки для перемещения, убрать UI с кол-вом требуемых AP,
        //убрать табличку с инфой о противнике (переход в состояние 1)
        fieldUI.apToMoveToggleEvent.Raise(false);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.selectedTile.value),
            FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.enemyInfoUIToggleEvent.Raise(false);
        fieldUI.db.enemiesList.list[fieldUI.db.previousEnemy.CurrentValue]
            .GetComponent<SpriteRenderer>().color = Color.white;
        fieldUI.db.previousEnemy.CurrentValue = -1;
        fieldUI.db.selectedTile.value = Vector3Int.one;
        fieldUI.attackButtonToggleEvent.Raise(false);
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    public void OnEnemyClick(int enemyIndex) {
        if (enemyIndex == fieldUI.db.previousEnemy.CurrentValue) {
            //скрыть табличку со сведениями о противнике при
            //повторном клике на него(переход в состояние 2)
            fieldUI.enemyInfoUIToggleEvent.Raise(false);
            fieldUI.attackButtonToggleEvent.Raise(false);
            fieldUI.db.previousEnemy.CurrentValue = -1;
            fieldUI.SetState(fieldUI.GetStateTwo());
            fieldUI.db.enemiesList.list[enemyIndex].GetComponent<SpriteRenderer>().color = Color.white;
        } else {
            //клик по другому противнику, обновить сведения таблички и выделение противников
            //(остаться в состоянии 3)
            fieldUI.enemyInfoUIToggleEvent.Raise(true);
            fieldUI.db.enemiesList.list[fieldUI.db.previousEnemy.CurrentValue]
                .GetComponent<SpriteRenderer>().color = Color.white;
            fieldUI.db.enemiesList.list[enemyIndex]
                .GetComponent<SpriteRenderer>().color = Color.red;
            fieldUI.db.previousEnemy.CurrentValue = enemyIndex;
            //настроить кликабельность кнопки атаки
            if (fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles.Contains(
                    Vector3Int.RoundToInt(
                        fieldUI.db.enemiesList.list[enemyIndex].transform.position))) {
                fieldUI.attackButtonToggleEvent.Raise(true);
            }
            else {
                fieldUI.attackButtonToggleEvent.Raise(false);
            }
        }       
    }

    public void OnWeaponIconClick() {
        //скрыть подсветку клеток перемещения, подсветить доступные для атаки клетки,
        //подсветить попавших в радиус атаки врагов (переход в состояние 5).
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles,
            FieldUIHandler.ATTACKABLE_TILE_COLOR);
        fieldUI.enemyInfoUIToggleEvent.Raise(false);
        fieldUI.apToMoveToggleEvent.Raise(false);
        fieldUI.db.enemiesList.list[fieldUI.db.previousEnemy.CurrentValue]
                .GetComponent<SpriteRenderer>().color = Color.white;
        fieldUI.db.selectedEnemy.CurrentValue = -1;
        fieldUI.db.selectedTile.value = Vector3Int.one;
        fieldUI.attackButtonToggleEvent.Raise(false);
        foreach (GameObject enemy in fieldUI.db.enemiesList.list) {
            enemy.GetComponent<SpriteRenderer>().color = Color.red;
        }
        fieldUI.SetState(fieldUI.GetStateFive());
    }

    public void OnMovementStart() {
        //если началось движение, оставить только подсветку пути перемещения
        fieldUI.apToMoveToggleEvent.Raise(false);
        fieldUI.attackButtonToggleEvent.Raise(false);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.pathTiles, 
            FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.selectedTile.value),
            FieldUIHandler.SELECTED_TILE_COLOR);
        fieldUI.SetState(fieldUI.GetStateFour());
    }

    public void OnMovementEnd() {
    }

    public void OnEndTurn() {
        //в конце хода убрать подсветку ОХ, убрать требуемое кол-во AP,
        //убрать выделение противника, перейти в состояние 1
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.enemyInfoUIToggleEvent.Raise(false);
        fieldUI.apToMoveToggleEvent.Raise(false);
        fieldUI.db.enemiesList.list[fieldUI.db.previousEnemy.CurrentValue]
                .GetComponent<SpriteRenderer>().color = Color.white;
        fieldUI.db.previousEnemy.CurrentValue = -1;
        fieldUI.db.selectedEnemy.CurrentValue = -1;
        fieldUI.attackButtonToggleEvent.Raise(false);
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    //после убийства противника отключить кнопку атаки, убрать инфо о противнике,
    //подсветить обновленные клетки перемещения, перейти в состояние 1
    public void OnEnemyKill() {
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.selectedTile.value),
            FieldUIHandler.SELECTED_TILE_COLOR);
        fieldUI.enemyInfoUIToggleEvent.Raise(false);
        fieldUI.attackButtonToggleEvent.Raise(false);
        fieldUI.db.previousEnemy.CurrentValue = -1;
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    public void OnWeaponEquip() {
        //настроить кликабельность кнопки атаки
        if (fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles.Contains(
                Vector3Int.RoundToInt(
                    fieldUI.db.enemiesList.list[fieldUI.db.previousEnemy.CurrentValue].transform.position))) {
            fieldUI.attackButtonToggleEvent.Raise(true);
        }
        else {
            fieldUI.attackButtonToggleEvent.Raise(false);
        }
    }

    public void OnStartTurn() {
    }
}
