﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FieldUIHandler : MonoBehaviour, IGameEventListener, IGameObjectEventListener,
    ITilemapsEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public static readonly Color MOVE_TILE_COLOR = new Color(0.345f, 0.957f, 0.631f);
    public static readonly Color ENEMY_MOVE_TILE_COLOR = new Color(0.937f, 0.404f, 0.733f);
    public static readonly Color SELECTED_ENEMY_TILE_COLOR = new Color(0.455f, 0.412f, 0.937f);
    public static readonly Color ATTACKABLE_TILE_COLOR = new Color(0.894f, 0.404f, 0.937f);
    public static readonly Color SELECTED_TILE_COLOR = Color.cyan;
    public static readonly Color NO_TILE_COLOR = Color.white;

    public SODatabase db;

    //Базовые события
    [Tooltip("Прослушиваемое событие создания уровня. Вместе с событием передается" +
        " информация о тайлах стен, пола, препятствий")]
    public TilemapsEvent tilemapsEvent;
    public GameEvent gameEvent;
    [Tooltip("Событие, позволяющее передавать GameObject в качестве параметра")]
    public GameObjectEvent gameObjectEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемое событие нажатия на клетку ОХ.")]
    public GameEventType onMoveTileClick;
    [Tooltip("Прослушиваемое событие нажатия на нейтральную клетку.")]
    public GameEventType onNeutralTileClick;
    [Tooltip("Прослушиваемое событие нажатия на противника.")]
    public GameEventType onEnemyClick;
    [Tooltip("Прослушиваемое событие начала перемещения игрока.")]
    public GameEventType onPlayerMovementStart;
    [Tooltip("Прослушиваемое событие обновления клеток перемещения.")]
    public GameEventType onPlayerMovementRefresh;
    [Tooltip("Прослушиваемое событие начала атаки игрока.")]
    public GameEventType onBeforePlayerAttack;
    [Tooltip("Прослушиваемое событие промаха игрока.")]
    public GameEventType onPlayerMissed;
    [Tooltip("Прослушиваемое событие нанесения игроком критического удара.")]
    public GameEventType onPlayerMadeCritHit;
    [Tooltip("Прослушиваемое событие нанесения игроком обычного удара.")]
    public GameEventType onPlayerMadeHit;
    [Tooltip("Прослушиваемое событие начала перезарядки оружия игрока.")]
    public GameEventType onBeforeWeaponReload;
    [Tooltip("Прослушиваемое событие перезарядки оружия игрока.")]
    public GameEventType onWeaponReload;
    [Tooltip("Прослушиваемое событие начала хода игрока.")]
    public GameEventType onStartTurn;
    [Tooltip("Прослушиваемое событие конца хода игрока.")]
    public GameEventType onEndTurn;
    [Tooltip("Прослушиваемое событие нажатия на картинку оружия.")]
    public GameEventType onWeaponIconClick;
    [Tooltip("Прослушиваемое событие нажатия на слот быстрого доступа.")]
    public GameEventType onQuickSlotClick;
    [Tooltip("Прослушиваемое событие смены текущего оружия игрока.")]
    public GameEventType onBeforeWeaponEquip;
    [Tooltip("Прослушиваемое событие смены текущего оружия игрока.")]
    public GameEventType onWeaponEquip;
    [Tooltip("Прослушиваемый тип события GameEvent для начала перемещения противника.")]
    public GameEventType onEnemyMovementStart;
    [Tooltip("Прослушиваемый тип события GameEvent для обработки окончания перемещения противника.")]
    public GameEventType onEnemyMovementEnd;
    [Tooltip("Прослушиваемый тип события убийства противника.")]
    public GameEventType onEnemyKilled;

    //Вызываемые события
    [Tooltip("Событие, вызываемое для активации/деактивации кнопки атаки.")]
    public BoolEvent attackButtonToggleEvent;
    [Tooltip("Событие, вызываемое для активации/деактивации UI со сведениями о противнике.")]
    public BoolEvent enemyInfoUIToggleEvent;
    [Tooltip("Событие, вызываемое для активации/деактивации UI с количеством очков " +
        "действия, необходимых для перемещения.")]
    public BoolEvent apToMoveToggleEvent;

    private Tilemap floorTilemap;
    //private Tilemap obstacleTilemap;

    private IFieldUIState stateOne;
    private IFieldUIState stateTwo;
    private IFieldUIState stateThree;
    private IFieldUIState stateFour;
    private IFieldUIState stateFive;
    private IFieldUIState currentState;

    void Start() {
        stateOne = new FieldUIStateOne(this);
        stateTwo = new FieldUIStateTwo(this);
        stateThree = new FieldUIStateThree(this);
        stateFour = new FieldUIStateFour(this);
        stateFive = new FieldUIStateFive(this);

        currentState = stateOne;
        db.previousEnemy.CurrentValue = -1;
    }

    void Awake() {
        Order = 99;
    }

    void OnEnable() {
        Register();
        RegisterGameObjectListener();
        RegisterTilemapEventListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterGameObjectListener();
        UnregisterTilemapEventListener();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onMoveTileClick) {
            //Debug.unityLogger.Log("OnMoveTileClick event received in FieldUIHandler");
            OnMoveTileClick();
        } else if(type == onNeutralTileClick) {
            //Debug.unityLogger.Log("OnNeutralTileClick event received in FieldUIHandler");
            OnNeutralTileClick();
        } else if(type == onStartTurn) {
            //Debug.unityLogger.Log("OnStartTurn event received in FieldUIHandler");
            OnStartTurn();
        } else if(type == onEndTurn) {
            //Debug.unityLogger.Log("OnEndTurn event received in FieldUIHandler");
            OnEndTurn();
        } else if(type == onPlayerMovementRefresh || type == onPlayerMissed ||
                    type == onPlayerMadeHit || type == onPlayerMadeCritHit ||
                    type == onWeaponReload || type == onQuickSlotClick) {
            //Debug.unityLogger.Log("OnPlayerMovementRefresh event received in FieldUIHandler");
            OnMovementEnd();
        } else if(type == onPlayerMovementStart || type == onBeforePlayerAttack 
            || type == onBeforeWeaponReload) {
            //Debug.unityLogger.Log("OnPlayerMovementStart event received in FieldUIHandler");
            OnMovementStart();
        } else if(type == onEnemyMovementEnd) {
            Debug.unityLogger.Log("OnEnemyMovementEnd event received in FieldUIHandler");
            Debug.unityLogger.Log("Current enemy", db.currentEnemy.CurrentValue);
            //int index = db.currentEnemy.CurrentValue == 0 ? db.enemiesList.list.Count - 1 : db.currentEnemy.CurrentValue - 1;
            HighlightFloorTiles(db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].pathTiles,
                NO_TILE_COLOR);
            //HighlightFloorTiles(db.enemiesMoveInfo.moveInfos[index].moveTiles,
            //    NO_TILE_COLOR);
        } else if(type == onEnemyKilled) {
            OnEnemyKill();
        } else if(type == onWeaponIconClick) {
            //Debug.unityLogger.Log("OnWeaponIconClick event received in FieldUIHandler");
            OnWeaponIconClick();
        } else if(type == onBeforeWeaponEquip && currentState == stateFive) {
            OnNeutralTileClick();
        } else if(type == onWeaponEquip) {
            OnWeaponEquip();
        }
    }

    public void OnGameObjectEventRaised(GameObject gameObject, GameEventType type) {
        if(type == onEnemyClick) {
            //Debug.unityLogger.Log("OnEnemyClick event received in FieldUIHandler", db.selectedEnemy.CurrentValue);
            OnEnemyClick(db.selectedEnemy.CurrentValue);
        } else if(type == onEnemyMovementStart) {
            //HighlightFloorTiles(db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].moveTiles,
            //    ENEMY_MOVE_TILE_COLOR);
            HighlightFloorTiles(db.enemiesMoveInfo.moveInfos[db.currentEnemy.CurrentValue].pathTiles,
                ENEMY_MOVE_TILE_COLOR);
        }
    }

    public void OnTilemapsEventRaised(Tilemap wallMap, Tilemap floorMap, Tilemap obstacleMap) {
        //Debug.unityLogger.Log("onLevelCreated event received in FieldUIHandler", tilemapsEvent);
        floorTilemap = floorMap;
        //wallTilemap = wallMap;
        //obstacleTilemap = obstacleMap;
    }

    private void OnMoveTileClick() {
        currentState.OnMoveTileClick();
    }

    private void OnNeutralTileClick() {
        currentState.OnNeutralTileClick();
    }

    private void OnEnemyClick(int enemyIndex) {
        currentState.OnEnemyClick(enemyIndex);
    }

    private void OnWeaponIconClick() {
        currentState.OnWeaponIconClick();
    }

    private void OnMovementStart() {
        currentState.OnMovementStart();
    }

    private void OnMovementEnd() {
        currentState.OnMovementEnd();
    }

    private void OnEnemyKill() {
        currentState.OnEnemyKill();
    }

    private void OnWeaponEquip() {
        currentState.OnWeaponEquip();
    }

    private void OnEndTurn() {
        currentState.OnEndTurn();
    }

    private void OnStartTurn() {
        currentState.OnStartTurn();
    }

    public void SetState(IFieldUIState state) {
        currentState = state;
    }

    public IFieldUIState GetStateOne() {
        return stateOne;
    }

    public IFieldUIState GetStateTwo() {
        return stateTwo;
    }

    public IFieldUIState GetStateThree() {
        return stateThree;
    }

    public IFieldUIState GetStateFour() {
        return stateFour;
    }

    public IFieldUIState GetStateFive() {
        return stateFive;
    }

    //Подсвечивает указанные тайлы пола выбранным цветом
    public void HighlightFloorTiles(List<Vector3Int> tiles, Color highlightColor) {
        foreach (Vector3Int tilePosition in tiles) {
            floorTilemap.SetTileFlags(tilePosition, TileFlags.None);
            floorTilemap.SetColor(tilePosition, highlightColor);
        }
    }

    //Подсвечивает указанную клетку пола выбранным цветом
    public void HighlightSingleFloorTile(Vector3Int tile, Color highlightColor) {
        floorTilemap.SetTileFlags(tile, TileFlags.None);
        floorTilemap.SetColor(tile, highlightColor);
    }

    //Регистрация/отписка слушателей
    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }

    public void RegisterGameObjectListener() {
        gameObjectEvent.RegisterListener(this);
    }

    public void UnregisterGameObjectListener() {
        gameObjectEvent.UnregisterListener(this);
    }

    public void RegisterTilemapEventListener() {
        tilemapsEvent.RegisterListener(this);
    }

    public void UnregisterTilemapEventListener() {
        tilemapsEvent.UnregisterListener(this);
    }
}
