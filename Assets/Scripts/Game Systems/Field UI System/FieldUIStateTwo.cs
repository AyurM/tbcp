﻿using UnityEngine;

//Второе состояние UI игрового поля:
//- область хода подсвечена;
//- область атаки не подсвечена;
//- клетка для перемещения подсвечена;
//- инфо о противнике не показывается.

public class FieldUIStateTwo : IFieldUIState {
    private FieldUIHandler fieldUI;

    public FieldUIHandler FieldUI {
        get {
            return fieldUI;
        }
        set {
            fieldUI = value;
        }
    }

    public FieldUIStateTwo(FieldUIHandler fui) {
        fieldUI = fui;
    }

    public void OnMoveTileClick() {
        //подсветить другую клетку для перемещения и показать требуемое кол-во AP
        //(остаться в состоянии 2)
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.selectedTile.value),
            FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.clickPosition.value),
            FieldUIHandler.SELECTED_TILE_COLOR);
        fieldUI.apToMoveToggleEvent.Raise(true);
    }

    public void OnNeutralTileClick() {
        //убрать подсветку клетки для перемещения, убрать UI с кол-вом требуемых AP
        //(переход в состояние 1)
        fieldUI.apToMoveToggleEvent.Raise(false);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.selectedTile.value),
            FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.db.selectedTile.value = Vector3Int.one;
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    public void OnEnemyClick(int enemyIndex) {
        //показать табличку со сведениями о противнике
        //(переход в состояние 3)
        fieldUI.enemyInfoUIToggleEvent.Raise(true);
        fieldUI.db.enemiesList.list[enemyIndex].GetComponent<SpriteRenderer>().color = Color.red;

        //настроить кликабельность кнопки атаки
        if (fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles.Contains(
                Vector3Int.RoundToInt(
                    fieldUI.db.enemiesList.list[enemyIndex].transform.position))) {
            fieldUI.attackButtonToggleEvent.Raise(true);
        }
        else {
            fieldUI.attackButtonToggleEvent.Raise(false);
        }
        fieldUI.db.previousEnemy.CurrentValue = enemyIndex;
        fieldUI.SetState(fieldUI.GetStateThree());
    }

    public void OnWeaponIconClick() {
        //скрыть подсветку клеток перемещения, подсветить доступные для атаки клетки,
        //подсветить попавших в радиус атаки врагов (переход в состояние 5).
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles,
            FieldUIHandler.ATTACKABLE_TILE_COLOR);
        fieldUI.apToMoveToggleEvent.Raise(false);
        fieldUI.db.selectedTile.value = Vector3Int.one;
        foreach (GameObject enemy in fieldUI.db.enemiesList.list) {
            enemy.GetComponent<SpriteRenderer>().color = Color.red;
        }
        fieldUI.SetState(fieldUI.GetStateFive());
    }

    public void OnMovementStart() {
        //если началось движение, оставить только подсветку пути перемещения
        fieldUI.apToMoveToggleEvent.Raise(false);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.pathTiles, 
            FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.selectedTile.value),
            FieldUIHandler.SELECTED_TILE_COLOR);
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    public void OnMovementEnd() {
    }

    public void OnEndTurn() {
        //в конце хода убрать подсветку ОХ, убрать требуемое кол-во AP
        //перейти в состояние 1
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.apToMoveToggleEvent.Raise(false);
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    public void OnEnemyKill() {
    }

    public void OnWeaponEquip() {
    }

    public void OnStartTurn() {
    }
}
