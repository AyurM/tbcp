﻿using UnityEngine;

//Пятое состояние UI игрового поля:
//- область хода не подсвечена;
//- область атаки подсвечена;
//- клетка для перемещения не подсвечена;
//- подсвечены противники, попадающие в область атаки.

public class FieldUIStateFive : IFieldUIState {
    private FieldUIHandler fieldUI;

    public FieldUIHandler FieldUI {
        get {
            return fieldUI;
        }
        set {
            fieldUI = value;
        }
    }

    public FieldUIStateFive(FieldUIHandler fui) {
        fieldUI = fui;
    }

    public void OnMoveTileClick() {
        //убрать подсветку ОА, подсветить ОХ, подсветить клетку для перемещения,
        //убрать подсветку противников (переход в состояние 2)
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles, 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.clickPosition.value),
            FieldUIHandler.SELECTED_TILE_COLOR);
        foreach (GameObject enemy in fieldUI.db.enemiesList.list) {
            enemy.GetComponent<SpriteRenderer>().color = Color.white;
        }
        fieldUI.apToMoveToggleEvent.Raise(true);
        fieldUI.SetState(fieldUI.GetStateTwo());
    }

    public void OnNeutralTileClick() {
        //убрать подсветку ОА, подсветить ОХ,
        //убрать подсветку противников (переход в состояние 1)
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles,
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(),
            FieldUIHandler.MOVE_TILE_COLOR);
        foreach (GameObject enemy in fieldUI.db.enemiesList.list) {
            enemy.GetComponent<SpriteRenderer>().color = Color.white;
        }
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    public void OnEnemyClick(int enemyIndex) {
        //показать табличку со сведениями о противнике
        //(переход в состояние 4)
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles,
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(),
            FieldUIHandler.MOVE_TILE_COLOR);
        foreach (GameObject enemy in fieldUI.db.enemiesList.list) {
            enemy.GetComponent<SpriteRenderer>().color = Color.white;
        }
        fieldUI.enemyInfoUIToggleEvent.Raise(true);
        fieldUI.db.enemiesList.list[enemyIndex].GetComponent<SpriteRenderer>().color = Color.red;
        fieldUI.db.previousEnemy.CurrentValue = enemyIndex;
        fieldUI.SetState(fieldUI.GetStateFour());
        //настроить кликабельность кнопки атаки
        if (fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles.Contains(
                Vector3Int.RoundToInt(
                    fieldUI.db.enemiesList.list[enemyIndex].transform.position))) {
            fieldUI.attackButtonToggleEvent.Raise(true);
        }
        else {
            fieldUI.attackButtonToggleEvent.Raise(false);
        }
    }

    public void OnWeaponIconClick() {
        //убрать подсветку ОА, подсветить ОХ,
        //убрать подсветку противников (переход в состояние 1)
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles,
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(),
            FieldUIHandler.MOVE_TILE_COLOR);
        foreach (GameObject enemy in fieldUI.db.enemiesList.list) {
            enemy.GetComponent<SpriteRenderer>().color = Color.white;
        }
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    public void OnMovementStart() {
    }

    public void OnMovementEnd() {
    }

    public void OnEndTurn() {
        //в конце хода убрать подсветку ОХ, убрать требуемое кол-во AP,
        //убрать выделение противника, перейти в состояние 1
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles,
            FieldUIHandler.NO_TILE_COLOR);
        foreach (GameObject enemy in fieldUI.db.enemiesList.list) {
            enemy.GetComponent<SpriteRenderer>().color = Color.white;
        }
        fieldUI.SetState(fieldUI.GetStateOne());
    }

    public void OnEnemyKill() {
    }

    public void OnWeaponEquip() {
    }

    public void OnStartTurn() {
    }
}