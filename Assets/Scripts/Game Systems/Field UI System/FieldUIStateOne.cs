﻿using UnityEngine;

//Стартовое состояние UI игрового поля:
//- область хода подсвечена;
//- область атаки не подсвечена;
//- клетка для перемещения не подсвечена;
//- инфо о противнике не показывается.

public class FieldUIStateOne : IFieldUIState {
    private FieldUIHandler fieldUI;

    public FieldUIHandler FieldUI {
        get {
            return fieldUI;
        }
        set {
            fieldUI = value;
        }
    }

    public FieldUIStateOne(FieldUIHandler fui) {
        fieldUI = fui;
    }

    public void OnMoveTileClick() {
        //подсветить клетку для перемещения и показать требуемое кол-во AP
        //(переход в состояние 2)
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.clickPosition.value), 
            FieldUIHandler.SELECTED_TILE_COLOR);
        fieldUI.apToMoveToggleEvent.Raise(true);
        fieldUI.SetState(fieldUI.GetStateTwo());
    }

    public void OnNeutralTileClick() {
    }

    public void OnEnemyClick(int enemyIndex) {
        //показать табличку со сведениями о противнике
        //(переход в состояние 4)
        fieldUI.enemyInfoUIToggleEvent.Raise(true);
        fieldUI.db.enemiesList.list[enemyIndex].GetComponent<SpriteRenderer>().color = Color.red;
        //настроить кликабельность кнопки атаки
        if (fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles.Contains(
                Vector3Int.RoundToInt(
                    fieldUI.db.enemiesList.list[enemyIndex].transform.position))) {
            fieldUI.attackButtonToggleEvent.Raise(true);
        } else {
            fieldUI.attackButtonToggleEvent.Raise(false);
        }
        fieldUI.db.previousEnemy.CurrentValue = enemyIndex;
        fieldUI.SetState(fieldUI.GetStateFour());
    }

    public void OnWeaponIconClick() {
        //скрыть подсветку клеток перемещения, подсветить доступные для атаки клетки,
        //подсветить попавших в радиус атаки врагов (переход в состояние 5).
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.attackTiles,
            FieldUIHandler.ATTACKABLE_TILE_COLOR);
        foreach(GameObject enemy in fieldUI.db.enemiesList.list) {
            enemy.GetComponent<SpriteRenderer>().color = Color.red;
        }
        fieldUI.SetState(fieldUI.GetStateFive());
    }

    public void OnMovementStart() {
        //для этого состояния данный метод будет вызываться при нажатии на кнопку перезарядки
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(),
            FieldUIHandler.NO_TILE_COLOR);
    }

    public void OnMovementEnd() {
        //в конце перемещения игрока подсветить клетки новой ОХ, остаться в состоянии 1
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.pathTiles, 
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(),
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.HighlightSingleFloorTile(Vector3Int.RoundToInt(fieldUI.db.selectedTile.value),
            FieldUIHandler.NO_TILE_COLOR);
        fieldUI.db.selectedTile.value = Vector3Int.one;
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(),
            FieldUIHandler.MOVE_TILE_COLOR);
    }

    public void OnEndTurn() {
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(), 
            FieldUIHandler.NO_TILE_COLOR);
    }

    public void OnEnemyKill() {
    }

    public void OnWeaponEquip() {
    }

    public void OnStartTurn() {
        fieldUI.HighlightFloorTiles(fieldUI.db.playerMoveInfo.pathfindInfo.GetMoveTiles(),
             FieldUIHandler.MOVE_TILE_COLOR);
        fieldUI.attackButtonToggleEvent.Raise(false);
    }
}
