﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovingObject : MonoBehaviour {

    [Tooltip("Время, необходимое для перемещения на 1 клетку")]
    public float moveTime = 0.08f;
    public Vector3Variable objectPosition;
    private float inverseMoveTime;

	protected virtual void Start () {
        inverseMoveTime = 1f / moveTime;
    }

    //перемещение объекта в указанную точку end
    protected IEnumerator SmoothMovement(Vector3 end) {
        float sqrRemainDistance = (transform.position - end).sqrMagnitude;

        while(sqrRemainDistance > float.Epsilon) {
            transform.position = Vector3.MoveTowards(transform.position, end,
                inverseMoveTime * Time.deltaTime);
            if(objectPosition != null) {
                objectPosition.value = transform.position;
            }
            sqrRemainDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

    protected virtual IEnumerator Move(Vector3 end) {
        yield return StartCoroutine(SmoothMovement(end));
        DoAfterMoveCoroutine();
    }

    protected void MoveSegment(Vector3 end) {
        StartCoroutine(SmoothMovement(end));
    }

    //Перемещение объекта по пути из нескольких прямолинейных сегментов
    protected virtual IEnumerator Move(List<Vector3Int> segments) {
        int distance;
        for(int i = 0; i < segments.Count; i++) {
            //рассчитать дистанцию перемещения для каждого сегмента,
            //необходимо для корректной временной задержки
            distance = Mathf.CeilToInt((transform.position - segments[i]).magnitude);
            MoveSegment(segments[i]);
            yield return new WaitForSeconds(moveTime * distance);
        }
        DoAfterMoveCoroutine();
    }

    protected abstract void DoAfterMoveCoroutine(); 
}
