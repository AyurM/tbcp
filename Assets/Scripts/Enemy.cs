﻿using UnityEngine;

public class Enemy : MonoBehaviour {

    [Tooltip("Таблица с базовыми характеристиками этого врага")]
    public EnemyStatTable statTable;
    [Tooltip("Редкость врага")]
    public EnemyRarity rarity;
    [Tooltip("Правила масштабирования характеристик в зависимости от " +
        "редкости и уровня")]
    public EnemyStatScaler statScaler;
    public Weapon weapon;
    public float delayBeforeTurn;
    [Tooltip("Если игрок находится на расстоянии менее, чем Aggro Range, " +
        "то данный противник будет нападать на него.")]
    public int aggroRange;

    public int enemyLevel;
    [HideInInspector]
    public string enemyName;
    [HideInInspector]
    public int maxHP;
    [HideInInspector]
    public int currentHP;
    [HideInInspector]
    public int maxAP;
    [HideInInspector]
    public int currentAP;
    [HideInInspector]
    public int xpForKill;
    
    void Start () {
        enemyLevel = statScaler.GetEnemyLevel();
        enemyName = statTable.BaseName;
        maxHP = statScaler.GetMaxHP(statTable, rarity, enemyLevel);
        currentHP = maxHP;
        maxAP = statScaler.GetMaxAP(statTable, rarity, enemyLevel);
        currentAP = maxAP;
        xpForKill = statScaler.GetXPForKill(statTable, rarity, enemyLevel);
    }

    public int GetNumberOfItemsToDropOnDeath() {
        return statScaler.GetNumberOfItemsToDrop(statTable, rarity, enemyLevel);
    }
}
