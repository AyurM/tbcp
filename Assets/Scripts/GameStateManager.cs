﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class GameStateManager : MonoBehaviour, IGameEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    private const string SAVE_FILENAME = "tbcp_saved_game.json";

    //Item Properties
    public ItemType[] itemTypes;
    public ArmorType[] armorTypes;
    public WeaponType[] weaponTypes;
    public ItemRarity[] rarities;
    public BaseItem[] baseItems;
    public BasePistol[] basePistols;
    public BaseShotgun[] baseShotguns;
    public BaseMedkit[] baseMedkits;
    public AttackMode[] attackModes;

    public SODatabase db;
    public PlayerStatScaler statScaler;

    public GameEvent gameEvent;

    //Прослушиваемые события
    [Tooltip("Прослушиваемое события успешного завершения уровня")]
    public GameEventType onLevelComplete;
    [Tooltip("Прослушиваемое событие запроса сохранения игры")]
    public GameEventType onSaveGame;

    private GameState gameState;

    void Awake() {        
        Order = 18;
    }

    void OnEnable() {
        Register();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        Unregister();
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        LoadGameState();
        ApplyGameState();
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onSaveGame) {
            RefreshGameState();
            SaveGameState();
        }
    }

    private void SaveGameState() {
        string saveString = JsonUtility.ToJson(gameState, true);
        Debug.unityLogger.Log("Saving GameState", saveString);
        string filePath = Path.Combine(Application.persistentDataPath, SAVE_FILENAME);
        File.WriteAllText(filePath, saveString);
    }

    public void ResetGameState() {
        gameState = new GameState();
        Debug.unityLogger.Log("GameState reseted!");
        SaveGameState();
    }

    public void LoadGameState() {
        string filePath = Path.Combine(Application.persistentDataPath, SAVE_FILENAME);
        if (File.Exists(filePath)) {
            string dataAsJson = File.ReadAllText(filePath);
            gameState = JsonUtility.FromJson<GameState>(dataAsJson);
            Debug.unityLogger.Log("Successfully loaded GameState");
        }
        else {
            Debug.unityLogger.Log("Error while loading gamestate from file " +
                filePath);
            gameState = new GameState();
        }        
    }

    private void RefreshGameState() {
        gameState.levelNumber = db.levelNumber.CurrentValue;
        gameState.playerLevel = db.playerLevel.CurrentValue;
        gameState.playerXP = db.playerXP.CurrentValue;
        gameState.playerHP = db.playerHP.CurrentValue;
        db.inventory.SaveInventory();
        gameState.inventory = db.inventory.jsonItems;
    }

    private void ApplyGameState() {
        db.levelNumber.CurrentValue = gameState.levelNumber;
        db.playerLevel.CurrentValue = gameState.playerLevel;
        db.playerXP.CurrentValue = gameState.playerXP;

        if (gameState.IsNew()) {
            db.playerHP.MaxValue = statScaler.GetMaxHP(db.playerLevel.CurrentValue);
            db.playerHP.CurrentValue = db.playerHP.MaxValue;
            db.inventory.ResetInventory();
            return;
        }

        db.playerHP.CurrentValue = gameState.playerHP;

        db.inventory.LoadInventory(gameState);
        foreach(Item item in db.inventory.items) {
            InitItem(item);
        }
    }

    private void InitItem(Item item) {
        LoadRarity(item);
        LoadItemType(item);
        LoadItemSubtype(item);
        LoadItemSprite(item);
        LoadItemSounds(item);
    }

    private void LoadRarity(Item item) {
        foreach(ItemRarity ir in rarities) {
            if(item.rarityName == ir.Name) {
                item.itemRarity = ir;
                break;
            }
        }       
    }

    private void LoadItemType(Item item) {
        foreach(ItemType it in itemTypes) {
            if(item.itemTypeName == it.typeName) {
                item.itemType = it;
                break;
            }
        }         
    }

    private void LoadItemSubtype(Item item) {
        if (item.itemType == itemTypes[0]) {
            for (int i = 0; i < armorTypes.Length; i++) {
                if (((Armor)item).armorTypeName == armorTypes[i].typeName) {
                    ((Armor)item).armorType = armorTypes[i];
                    break;
                }
            }
        }
        else if (item.itemType == itemTypes[1]) {
            for (int i = 0; i < weaponTypes.Length; i++) {
                if (((Weapon)item).weaponTypeName == weaponTypes[i].typeName) {
                    ((Weapon)item).weaponType = weaponTypes[i];
                    break;
                }
            }
        }
    }

    private void LoadItemSprite(Item item) {
        foreach(BaseItem bi in baseItems) {
            if(item.itemName == bi.baseName) {
                item.sprite = bi.baseSprite;
                break;
            }
        }
    }

    private void LoadItemSounds(Item item) {
        if(item is Armor) {
            return;
        } else if(item is Weapon) {
            LoadAttackSounds((Weapon)item);
            LoadReloadSound((Weapon)item);
        } else if(item is Medkit) {
            LoadMedkitSound((Medkit) item);
        }
    }

    private void LoadAttackSounds(Weapon weapon) {
        foreach(AttackModePO apo in weapon.attackModes) {
            foreach(AttackMode am in attackModes) {
                if(am.fullAttackName == apo.fullAttackName) {
                    apo.attackSound = am.attackSound;
                    break;
                }
            }
        }
    }

    private void LoadReloadSound(Weapon weapon) {
        if(weapon is MeleeWeapon) {
            return;
        }
        if(weapon is Pistol) {
            foreach(BasePistol bp in basePistols) {
                if(bp.baseName == ((Pistol)weapon).itemName) {
                    ((GunMagazine)weapon.magazine).reloadSound = bp.reloadSound;
                }
            }
        } else if (weapon is Shotgun) {
            foreach (BaseShotgun bs in baseShotguns) {
                if (bs.baseName == ((Shotgun)weapon).itemName) {
                    ((GunMagazine)weapon.magazine).reloadSound = bs.reloadSound;
                }
            }
        }
    }

    private void LoadMedkitSound(Medkit item) {
        foreach(BaseMedkit mkit in baseMedkits) {
            if(item.itemName == mkit.baseName) {
                item.useSound = mkit.useSound;
                break;
            }
        }
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
