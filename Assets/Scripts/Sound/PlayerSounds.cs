﻿using UnityEngine;

public class PlayerSounds : MonoBehaviour, IGameEventListener, IIntEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public AudioClip playerFootsteps;
    public AudioClip levelUpSound;
    public AudioClip itemEquip;
    public AudioClip armorEquip;
    public AudioClip hurtSound;
    public AudioSource audioSource;
    public AudioSource uiAudioSource;

    public GameEvent gameEvent;
    public IntEvent intEvent;

    [Tooltip("Прослушиваемый тип события GameEvent начала перемещения игрока.")]
    public GameEventType onPlayerMovementStart;
    [Tooltip("Прослушиваемый тип события GameEvent окончания перемещения игрока.")]
    public GameEventType onPlayerMovementEnd;
    [Tooltip("Прослушиваемое событие смены текущего оружия игрока.")]
    public GameEventType onWeaponEquip;
    [Tooltip("Прослушиваемое событие смены брони игрока.")]
    public GameEventType onArmorEquip;
    [Tooltip("Прослушиваемое событие нажатия на слот быстрого доступа.")]
    public GameEventType onQuickSlotClick;
    [Tooltip("Прослушиваемое событие нанесения врагом критического удара по игроку.")]
    public GameEventType onEnemyMadeCritHit;
    [Tooltip("Прослушиваемое событие нанесения врагом обычного удара по игроку.")]
    public GameEventType onEnemyMadeHit;

    void Awake() {
        Order = 9;
    }

    void OnEnable() {
        Register();
        RegisterIntListener();
    }

    void OnDisable() {
        Unregister();
        UnregisterIntListener();
    }

    void Start() {
        audioSource.volume = PlayerPrefs.GetFloat("SfxVolume", 0.5f);
        uiAudioSource.volume = PlayerPrefs.GetFloat("SfxVolume", 0.5f);
    }

    public void OnSfxVolumeChanged() {
        audioSource.volume = PlayerPrefs.GetFloat("SfxVolume", 0.5f);
        uiAudioSource.volume = PlayerPrefs.GetFloat("SfxVolume", 0.5f);
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onPlayerMovementStart) {
            audioSource.clip = playerFootsteps;
            audioSource.loop = true;
            audioSource.Play();
        } else if (type == onPlayerMovementEnd) {
            audioSource.loop = false;
            audioSource.Stop();
        } else if(type == onWeaponEquip) {
            audioSource.loop = false;
            audioSource.clip = itemEquip;
            audioSource.Play();
        } else if (type == onArmorEquip) {
            audioSource.loop = false;
            audioSource.clip = armorEquip;
            audioSource.Play();
        } else if(type == onEnemyMadeHit || type == onEnemyMadeCritHit) {
            audioSource.loop = false;
            audioSource.clip = hurtSound;
            audioSource.Play();
        } else if(type == onQuickSlotClick) {
            uiAudioSource.clip = ((Medkit) db.inventory.equipment.quickSlot).useSound;
            uiAudioSource.Play();
        }
    }

    public void OnIntEventRaised(IntVariable variable) {
        if (variable == db.playerLevel) {
            uiAudioSource.clip = levelUpSound;
            uiAudioSource.Play();
        }
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }
}
