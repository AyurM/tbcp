﻿using UnityEngine;

public class WeaponSound : MonoBehaviour, IGameEventListener {

    private int order;
    public int Order {
        get {
            return order;
        }
        set {
            order = value;
        }
    }

    public SODatabase db;

    public AudioSource audioSource;

    public GameEvent gameEvent;
    [Tooltip("Прослушиваемое событие нажатия на кнопку атаки.")]
    public GameEventType onAttackButtonClick;
    [Tooltip("Прослушиваемое событие нажатия на кнопку перезарядки.")]
    public GameEventType onReloadButtonClick;
    [Tooltip("Прослушиваемое событие попытки противника атаковать игрока.")]
    public GameEventType onEnemyAttemptToAttack;

    void Awake() {
        Order = 10;
    }

    void OnEnable() {
        Register();
    }

    void OnDisable() {
        Unregister();
    }

    void Start() {
        audioSource.volume = PlayerPrefs.GetFloat("SfxVolume", 0.5f);
    }

    public void OnSfxVolumeChanged() {
        audioSource.volume = PlayerPrefs.GetFloat("SfxVolume", 0.5f);
    }

    public void OnEventRaised(GameEventType type) {
        if(type == onAttackButtonClick) {
            PlayPlayerAttackSound();
        } else if(type == onReloadButtonClick) {
            PlayReloadSound();
        } else if(type == onEnemyAttemptToAttack) {
            PlayEnemyAttackSound();
        }
    }

    private void PlayPlayerAttackSound() {
        if (db.inventory.equipment.mainWeapon.GetCurrentAttackMode().attackSound != null) {
            audioSource.clip = db.inventory.equipment.mainWeapon.GetCurrentAttackMode().attackSound;
            audioSource.Play();
        }
    }

    private void PlayReloadSound() {
        audioSource.clip = ((GunMagazine)db.inventory.equipment.mainWeapon.magazine).reloadSound;
        audioSource.Play();
    }

    private void PlayEnemyAttackSound() {
        Enemy enemy = db.enemiesList.list[db.currentEnemy.CurrentValue].GetComponent<Enemy>();
        if(enemy.weapon != null) {
            audioSource.clip = enemy.weapon.GetCurrentAttackMode().attackSound;
            audioSource.Play();
        }
    }

    public void Register() {
        gameEvent.RegisterListener(this);
    }

    public void Unregister() {
        gameEvent.UnregisterListener(this);
    }
}
