﻿using UnityEngine;

public class MainMenuSound : MonoBehaviour {

    public AudioSource musicSource;

	void Start () {
        musicSource.volume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
	}
	
	public void OnMusicVolumeUpdate() {
        musicSource.volume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
    }
}
