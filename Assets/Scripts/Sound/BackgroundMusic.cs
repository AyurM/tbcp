﻿using UnityEngine;
using Random = UnityEngine.Random;

public class BackgroundMusic : MonoBehaviour {

    public AudioClip[] musicClips;
    public AudioSource audioSource;

    private void Start() {
        if (musicClips.Length == 0) {
            return;
        }

        audioSource.volume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);

        Random.InitState(System.DateTime.Now.Millisecond);
        audioSource.clip = musicClips[Random.Range(0, musicClips.Length)];
        audioSource.Play();
    }

    public void OnMusicVolumeUpdate() {
        audioSource.volume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
    }
}
