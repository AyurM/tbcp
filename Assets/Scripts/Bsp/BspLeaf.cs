﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class BspLeaf {

    public Vector3Int leafPosition;
    public int width;
    public int height;
    public BspLeaf leftChild;
    public BspLeaf rightChild;

    public BspLeaf(Vector3Int lp, int w, int h) {
        leafPosition = lp;
        width = w;
        height = h;
        //Debug.unityLogger.Log("Created bsp tree leaf", this.ToString());
    }

    public bool Split() {
        if(leftChild != null || rightChild != null) {
            return false;   //разделение данного узла уже было выполнено
        }

        if( (width * height) < (LevelGenerator.MIN_ROOM_COLUMNS * 
            LevelGenerator.MIN_ROOM_ROWS * 2)) {
            return false;   //площадь узла слишком мала, чтобы разделить его на два
        }

        if( ((width >= LevelGenerator.MIN_ROOM_COLUMNS * 2) && 
                            (height < LevelGenerator.MIN_ROOM_ROWS)) ||
            ((height >= LevelGenerator.MIN_ROOM_ROWS * 2) &&
                            (width < LevelGenerator.MIN_ROOM_COLUMNS)) ||
            ((width >= LevelGenerator.MIN_ROOM_COLUMNS && width < LevelGenerator.MIN_ROOM_COLUMNS * 2) &&
                            (height < LevelGenerator.MIN_ROOM_ROWS * 2)) ||
            ((height >= LevelGenerator.MIN_ROOM_ROWS && height < LevelGenerator.MIN_ROOM_ROWS * 2) &&
                            (width < LevelGenerator.MIN_ROOM_COLUMNS * 2))) {
            return false;   //соотношение сторон узла не позволяет разбить его на два
        }

        if(width <= LevelGenerator.MAX_ROOM_COLUMNS &&
            height <= LevelGenerator.MAX_ROOM_ROWS) {
            return false;   //размер узла не превышает макс размеры комнаты, делить не нужно
        }

        Random.InitState(System.DateTime.Now.Millisecond);
        bool splitHoriz;    //разделить узел по горизонтали, если true
        if(height >= LevelGenerator.MIN_ROOM_ROWS * 2 &&
                width < LevelGenerator.MIN_ROOM_COLUMNS * 2) {
            splitHoriz = true;
        } else if(width >= LevelGenerator.MIN_ROOM_COLUMNS * 2 &&
                height < LevelGenerator.MIN_ROOM_ROWS * 2) {
            splitHoriz = false;
        } else {
            splitHoriz = Random.Range(0f, 1f) > 0.5f;
        }

        int maxCoord = splitHoriz ? (height - LevelGenerator.MIN_ROOM_ROWS) :
            (width - LevelGenerator.MIN_ROOM_COLUMNS);
        int splitCoord = Random.Range(splitHoriz ? LevelGenerator.MIN_ROOM_ROWS :
            LevelGenerator.MIN_ROOM_COLUMNS, maxCoord + 1);

        if (splitHoriz) {
            leftChild = new BspLeaf(leafPosition, width, splitCoord);
            rightChild = new BspLeaf(new Vector3Int(leafPosition.x, leafPosition.y + splitCoord, 0), 
                width, height - splitCoord);
        } else {
            leftChild = new BspLeaf(leafPosition, splitCoord, height);
            rightChild = new BspLeaf(new Vector3Int(leafPosition.x + splitCoord, leafPosition.y, 0),
                width - splitCoord, height);
        }
        return true;
    }

    public void CreateRoom(Tilemap wallMap, Tilemap floorMap, RuleTile wallTile,
            RandomTile floorTile, List<BspLeaf> rooms) {
        if (leftChild != null || rightChild != null) {
            //промежуточный узел, спуститься дальше по дереву
            if (leftChild != null) {
                leftChild.CreateRoom(wallMap, floorMap, wallTile, floorTile, rooms);
            }
            if (rightChild != null) {
                rightChild.CreateRoom(wallMap, floorMap, wallTile, floorTile, rooms);
            }
        }
        else {
            //конечный узел, можно создавать комнату
            rooms.Add(this);
        }
    }

    public void GenerateRoom(Tilemap wallMap, Tilemap floorMap, RuleTile wallTile,
            RandomTile floorTile, SODatabase dbase) {

        Vector3Int currentPosition;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                currentPosition = new Vector3Int(leafPosition.x + i, leafPosition.y + j, 0);

                if (i == 0 || i == width - 1 || j == 0 || j == height - 1) {                    
                    wallMap.SetTile(currentPosition, wallTile);
                    dbase.wallTiles.list.Add(currentPosition);
                }
                else {
                    floorMap.SetTile(currentPosition, floorTile);
                    dbase.floorTiles.list.Add(currentPosition);
                }

            }
        }
    }

    public override string ToString() {
        return "Leaf: Position = " + leafPosition.ToString() + "; width = " + width +
            "; height = " + height;
    }
}
