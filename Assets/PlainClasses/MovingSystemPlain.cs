﻿using System.Collections.Generic;
using UnityEngine;

public class MovingSystemPlain {

    //Рассчитывает область хода (ОХ), которая доступна из клетки startPosition
    //при наличии actionPoints очков действия. Результат сохраняется в moveInfo.
    public static void GetMovePositions(Vector3Int startPosition, Vector3Int playerPosition,
                            int actionPoints,
                            ref PathfindInfo moveInfo,
                            List<Vector3Int> floorTiles, List<Vector3Int> obstacleTiles,                                
                            List<Vector3Int> enemiesPositions) {
        moveInfo.tilesWithDistances = new List<KeyValuePair<int, Vector3Int>> {
            new KeyValuePair<int, Vector3Int>(0, startPosition)
        };
        moveInfo.moveTiles = new List<Vector3Int> { startPosition };
        int startCheckIndex = 0;
        int endCheckIndex = 1;
        for (int i = 0; i < actionPoints; i++) {
            for (int j = startCheckIndex; j < endCheckIndex; j++) {
                FindAdjacentMoveTiles(moveInfo.tilesWithDistances[j].Value,
                    playerPosition, moveInfo.tilesWithDistances[j].Key, ref moveInfo,
                    floorTiles, obstacleTiles, enemiesPositions);
            }
            startCheckIndex = endCheckIndex;
            endCheckIndex = moveInfo.tilesWithDistances.Count;
        }
    }

    //Вычисляет клетки ортогонального пути из startTile в endTile
    public static List<Vector3Int> GetOrthogonalPath(Vector3Int startTile, Vector3Int endTile,
                PathfindInfo moveTiles) {
        List<Vector3Int> result = new List<Vector3Int>();
        int distance = moveTiles.GetTravelDistance(endTile);
        if (distance == -1) {
            return result;
        }
        result.Add(endTile);
        int currentDistance = distance; //дистанция от обрабатываемой клетки до стартовой на каждом шаге будет сокращаться

        Vector3Int currentTile = endTile;     //на каждом шаге обрабатываемая клетка будет изменяться

        for (int i = 0; i < distance - 1; i++) {

            for (int j = 0; j < 4; j++) {
                Vector3Int checkTile = new Vector3Int();
                Vector3Int direction = Vector3Int.zero;
                switch (j) {
                    case 0:
                        direction = Vector3Int.right;
                        break;
                    case 1:
                        direction = Vector3Int.left;
                        break;
                    case 2:
                        direction = Vector3Int.up;
                        break;
                    case 3:
                        direction = Vector3Int.down;
                        break;
                }

                checkTile = currentTile + direction;
                //клетка должна быть доступна для перемещения и быть ближе на единицу к стартовой точке
                if (moveTiles.moveTiles.Contains(checkTile) &&
                    moveTiles.GetTravelDistance(checkTile) == currentDistance - 1) {
                    currentTile = checkTile;
                    result.Add(checkTile);
                    break;
                }
            }
            currentDistance--;
        }

        return result;
    }

    //Разделяет ортогональный путь path на прямолинейные сегменты для
    //корректной анимации перемещения
    public static List<Vector3Int> GetPathSegments(List<Vector3Int> path, Vector3Int startTile) {
        List<Vector3Int> result = new List<Vector3Int>();
        Vector3Int direction = Vector3Int.zero;
        Vector3Int currentDirection = Vector3Int.zero;
        if (path.Count == 1) {
            result.Add(path[0]);
            return result;
        }
        for (int i = path.Count - 1; i >= 0; i--) {
            if (i == path.Count - 1) {
                currentDirection = startTile - path[i];
                direction = currentDirection;
            }
            else {
                currentDirection = path[i + 1] - path[i];
                if (currentDirection != direction) {
                    result.Add(path[i + 1]);
                    direction = currentDirection;
                }
                if (i == 0) {
                    result.Add(path[i]);
                }
            }
        }
        return result;
    }

    //Проверяет, доступны ли соседние клетки для перемещения
    private static void FindAdjacentMoveTiles(Vector3Int tile, Vector3Int playerPosition,
                    int currentDistance,
                    ref PathfindInfo moveInfo,
                    List<Vector3Int> floorTiles, List<Vector3Int> obstacleTiles,
                    List<Vector3Int> enemiesPositions) {
        for (int i = 0; i < 4; i++) {
            Vector3Int checkTile = new Vector3Int();
            Vector3Int direction = Vector3Int.zero;
            switch (i) {
                case 0:
                    direction = Vector3Int.right;
                    break;
                case 1:
                    direction = Vector3Int.left;
                    break;
                case 2:
                    direction = Vector3Int.up;
                    break;
                case 3:
                    direction = Vector3Int.down;
                    break;
            }

            checkTile = tile + direction;
            //клетка доступна для перемещения, если: 
            //- является клеткой пола;
            //- не была добавлена в список доступных клеток ранее
            //- на данной клетке нет препятствий
            //- не занята противником или игроком
            if (floorTiles.Contains(checkTile) && !moveInfo.MoveAreaContains(checkTile)
                    && !obstacleTiles.Contains(checkTile) 
                    && !IsOccupiedByEnemy(checkTile, enemiesPositions)
                    && checkTile != playerPosition) {
                moveInfo.moveTiles.Add(checkTile);
                moveInfo.tilesWithDistances.Add(new KeyValuePair<int, Vector3Int>(
                    currentDistance + 1, checkTile));
            }
        }
    }

    //определяет не занята ли данная клетка противником
    private static bool IsOccupiedByEnemy(Vector3Int tile, List<Vector3Int> enemiesPositions) {
        for (int i = 0; i < enemiesPositions.Count; i++) {
            if (tile == enemiesPositions[i]) {
                return true;
            }
        }
        return false;
    }
}
