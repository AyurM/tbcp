﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public abstract class ObstacleGenerator : ScriptableObject {

    public SODatabase db;

    [Tooltip("Событие GameObjectEvent, позволяющее передавать GameObject в качестве параметра")]
    public GameObjectEvent gameObjectEvent;

    [Tooltip("При спавне нового противника будет вызван GameObjectEvent данного типа.")]
    public GameEventType onEnemySpawned;

    public Tilemap obstacleTilesPrefab;
    public RuleTile obstacleTile;
    public GameObject[] enemiesPrefabs;
    [HideInInspector]
    public Tilemap obstacleTiles;

    public float minObstacleRate = 0.1f;
    public float maxObstacleRate = 0.15f;

    public abstract void GenerateObstacles(List<BspLeaf> rooms);

    public abstract void PlaceEnemies(List<BspLeaf> rooms);
}
