﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public abstract class RoomGenerator : ScriptableObject {

    public SODatabase db;

    public Tilemap wallTilesPrefab;
    public Tilemap floorTilesPrefab;
    [HideInInspector]
    public Tilemap wallTiles;
    [HideInInspector]
    public Tilemap floorTiles;
    public RandomTile[] floorTile;
    public RuleTile wallTile;

    public abstract List<BspLeaf> GenerateLevel();
}
