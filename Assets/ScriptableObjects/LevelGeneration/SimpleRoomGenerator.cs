﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

//Генерирует простую прямоугольную комнату размером levelColumns х levelRows (включая стены)
[CreateAssetMenu(menuName = "Room Generator / Simple Room Generator")]
public class SimpleRoomGenerator : RoomGenerator {

    public override List<BspLeaf> GenerateLevel() {
        List<BspLeaf> result = new List<BspLeaf>();
        wallTiles = Instantiate(wallTilesPrefab);
        floorTiles = Instantiate(floorTilesPrefab);
        Random.InitState(System.DateTime.Now.Millisecond);
        int randomIndex = Random.Range(0, floorTile.Length);
        for (int i = 0; i < db.levelColumns.CurrentValue; i++) {
            for (int j = 0; j < db.levelRows.CurrentValue; j++) {
                if (i == 0 || i == db.levelColumns.CurrentValue - 1 || j == 0 || j == db.levelRows.CurrentValue - 1) {
                    //по внешнему периметру уровня разместить стены
                    wallTiles.SetTile(new Vector3Int(i, j, 0), wallTile);
                }
                else {
                    floorTiles.SetTile(new Vector3Int(i, j, 0), floorTile[randomIndex]);   //внутри уровня - пол
                }
            }
        }
        result.Add(new BspLeaf(new Vector3Int(), 
            db.levelColumns.CurrentValue, db.levelRows.CurrentValue));
        return result;
    }
}
