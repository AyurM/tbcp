﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

//Генерирует набор рандомизированных прямоугольных комнат без коридоров с помощью BSP-дерева
[CreateAssetMenu(menuName = "Room Generator / BSP Room Generator")]
public class BspRoomGenerator : RoomGenerator {

    public int minDoorSize;
    public int maxDoorSize;

    private List<BspLeaf> leaves;   //все узлы BSP-дерева
    private List<BspLeaf> rooms;    //конечные узлы BSP-дерева, в которых будут сгенерированы комнаты
    private int[,] adjacency;       //матрица смежности для графа комнат

    private enum AdjSide { None, Top, Right, Bottom, Left };

    public override List<BspLeaf> GenerateLevel() {
        leaves = new List<BspLeaf>();
        rooms = new List<BspLeaf>();
        BspLeaf rootLeaf = new BspLeaf(new Vector3Int(0, 0, 0), db.levelColumns.CurrentValue, 
                                       db.levelRows.CurrentValue);
        leaves.Add(rootLeaf);
        SplitLevelArea(leaves);     //выполнить BSP-разбиение области уровня

        Random.InitState(System.DateTime.Now.Millisecond);
        int randomIndex = Random.Range(0, floorTile.Length);    //для случайного выбора тайлсета пола
        wallTiles = Instantiate(wallTilesPrefab);
        floorTiles = Instantiate(floorTilesPrefab);

        //Сохраняет конечные листья с комнатами в rooms
        rootLeaf.CreateRoom(wallTiles, floorTiles, wallTile, floorTile[randomIndex], rooms);

        Debug.unityLogger.Log("Total rooms", rooms.Count);
        //Если уровень состоит из 1 комнаты, не рандомизировать
        if(rooms.Count > 1) {
            List<BspLeaf> tempRooms = new List<BspLeaf>();
            foreach (BspLeaf r in rooms) {
                tempRooms.Add(new BspLeaf(r.leafPosition, r.width, r.height));
            }

            adjacency = FindAdjacencyMatrix(rooms);

            /*Промежуточная матрица смежности, позволяет пробовать
             удалять связи между комнатами случайным образом с 
             возможностью отмены удаления в случае нарушения связности*/
            int[,] tempAdjacency = adjacency.Clone() as int[,];
            //Debug.unityLogger.Log(Array2DToString(adjacency));

            RandomizeAdjacency(tempRooms, tempAdjacency);   //рандомизировать связи между комнатами
            ReadjustWalls(tempRooms, tempAdjacency);        //сделать толщину всех стен равной 1 клетке

            Debug.unityLogger.Log(Array2DToString(tempAdjacency));
            foreach (BspLeaf r in tempRooms) {
                Debug.unityLogger.Log("Room before randomization", r.ToString());
            }

            adjacency = tempAdjacency;
            rooms = tempRooms;
            GenerateRooms(rooms, adjacency);    //рандомизировать размеры комнат
        }
       
        //создать стены и пол в комнатах
        foreach (BspLeaf r in rooms) {            
            r.GenerateRoom(wallTiles, floorTiles, wallTile, floorTile[randomIndex], db);
            //Debug.unityLogger.Log("Room after randomization", r.ToString());
        }
        if(rooms.Count > 1) {
            //соединить комнаты рандомизированными проемами
            MakeDoors(rooms, adjacency, wallTiles, floorTiles, floorTile[randomIndex]);
        }       
        return rooms;
    }

    //Разбиение уровня на области c помощью BSP-дерева.
    //Комнаты должны удовлетворять размерам, заданным в LevelGenerator.
    private void SplitLevelArea(List<BspLeaf> bspLeaves) {
        bool didSplit = true;
        while (didSplit) {
            didSplit = false;
            for (int i = 0; i < bspLeaves.Count; i++) {
                if (bspLeaves[i].leftChild == null && bspLeaves[i].rightChild == null) {
                    if (bspLeaves[i].Split()) {
                        bspLeaves.Add(bspLeaves[i].leftChild);
                        bspLeaves.Add(bspLeaves[i].rightChild);
                        didSplit = true;
                    }
                }
            }
        }
    }

    //Случайным образом задает связи между комнатами, при этом
    //гарантируется связность получаемого графа комнат.
    private void RandomizeAdjacency(List<BspLeaf> tempRooms, int[,] tempAdjacency) {
        for (int i = 0; i < tempAdjacency.GetLength(0); i++) {
            for (int j = i + 1; j < tempAdjacency.GetLength(1); j++) {

                if (tempAdjacency[i, j] > 0) {
                    //С вероятностью 50% попытаться удалить связь между комнатами
                    //Random.InitState(System.DateTime.Now.Millisecond);
                    bool removeConnection = Random.Range(0f, 1f) > 0.5f;

                    if (removeConnection &&
                        ((tempAdjacency[i, j] == (int)AdjSide.Top && tempRooms[i].height == LevelGenerator.MIN_ROOM_ROWS && tempRooms[j].height == LevelGenerator.MIN_ROOM_ROWS) ||
                        (tempAdjacency[i, j] == (int)AdjSide.Right && tempRooms[i].width == LevelGenerator.MIN_ROOM_COLUMNS && tempRooms[j].width == LevelGenerator.MIN_ROOM_COLUMNS))) {
                            removeConnection = false;   //комнаты минимального размера, отменить уменьшение размера
                    }

                    if (removeConnection) {

                        Debug.unityLogger.Log("Removed connection between " + i + " and " + j + " rooms");
                        RemoveConnection(tempRooms[i], tempRooms[j], tempAdjacency[i, j]);
                        tempAdjacency[i, j] = tempAdjacency[j, i] = 0;

                        //Отменить удаление при нарушении связности, т.е.
                        //при появлении комнат, в которые невозможно попасть
                        if (!IsRoomGraphConnected(tempAdjacency)) {
                            Debug.unityLogger.Log("Restoring connection between " + i + " and " + j + " rooms");
                            tempAdjacency[i, j] = adjacency[i, j];
                            tempAdjacency[j, i] = adjacency[j, i];
                            tempRooms[i] = new BspLeaf(rooms[i].leafPosition, rooms[i].width, rooms[i].height);
                            tempRooms[j] = new BspLeaf(rooms[j].leafPosition, rooms[j].width, rooms[j].height);
                        } else {
                            //сохранить изменения
                            adjacency[i, j] = adjacency[j, i] = 0;
                            rooms[i] = new BspLeaf(tempRooms[i].leafPosition, tempRooms[i].width, tempRooms[i].height);
                            rooms[j] = new BspLeaf(tempRooms[j].leafPosition, tempRooms[j].width, tempRooms[j].height);
                        }

                    }
                }
            }
        }
    }

    //Предотвращает появление стен толщиной более 1 клетки.
    private void ReadjustWalls(List<BspLeaf> tempRooms, int[,] tempAdjacency) {
        //Расширить комнату на 1 клетку в сторону соседней комнаты,
        //чтобы избавиться от стен в 2 клетки толщиной.
        int shift;
        for (int i = 0; i < tempAdjacency.GetLength(0); i++) {
            for (int j = i + 1; j < tempAdjacency.GetLength(1); j++) {
                if (tempAdjacency[i, j] > 0) {
                    //Достаточно обработать только правую половину 
                    //матрицы смежности(случаи Top и Right)
                    switch (tempAdjacency[i, j]) {
                        case (int)AdjSide.Top:
                            shift = (tempRooms[j].leafPosition.y + 1) - (tempRooms[i].leafPosition.y + tempRooms[i].height);
                            if(shift > 0) {
                                //расширить меньшую комнату
                                if (tempRooms[i].height < tempRooms[j].height) {
                                    tempRooms[i].height += shift;
                                }
                                else {
                                    tempRooms[j].height += shift;
                                    tempRooms[j].leafPosition.y -= shift;
                                }
                            }                           
                            break;
                        case (int)AdjSide.Right:
                            shift = (tempRooms[j].leafPosition.x + 1) - (tempRooms[i].leafPosition.x + tempRooms[i].width);
                            if (shift > 0) {
                                if (tempRooms[i].width < tempRooms[j].width) {
                                    tempRooms[i].width += shift;
                                }
                                else {
                                    tempRooms[j].width += shift;
                                    tempRooms[j].leafPosition.x -= shift;
                                }                                
                            }
                            break;
                    }
					rooms[i] = new BspLeaf(tempRooms[i].leafPosition, tempRooms[i].width, tempRooms[i].height);
                }
            }
        }
    }

    //Создает случайные комнаты на основе конечных узлов BSP-дерева rooms, 
    //и матрицы смежности, определяющей связи между комнатами.
    private void GenerateRooms(List<BspLeaf> rooms, int[,] adj) {
        for(int i = 0; i < rooms.Count; i++) {
            for(int s = 1; s < 5; s++) {
                RandomizeSide(rooms, adj, i, (AdjSide)s);
            }
        }
    }
   
    private void RandomizeSide(List<BspLeaf> rooms, int[,] adj,
                                int roomIndex, AdjSide side) {
        if (IsSideFixed(adj, roomIndex, side)) {
            return;     //данную сторону комнаты нельзя изменять
        }

        Random.InitState(System.DateTime.Now.Millisecond);
        /* выбрать случайным образом величину уменьшения стороны (в допустимых
         * пределах) и выполнить уменьшение. */
        int randomShift = Random.Range(0, GetSideConstraints(rooms, adj, roomIndex, side) + 1);
        switch (side) {
            case AdjSide.Left:
                rooms[roomIndex].leafPosition.x += randomShift;
                rooms[roomIndex].width -= randomShift; 
                break;
            case AdjSide.Right:                
                rooms[roomIndex].width -= randomShift;
                break;
            case AdjSide.Top:                
                rooms[roomIndex].height -= randomShift;
                break;
            default:                
                rooms[roomIndex].leafPosition.y += randomShift;
                rooms[roomIndex].height -= randomShift;
                break;
        }
    }

    //По виду матрицы смежности adj определяет, можно ли 
    //изменять сторону side комнаты с номером roomIndex.
    private bool IsSideFixed(int[,] adj, int roomIndex, AdjSide side) {
        for (int i = 0; i < adj.GetLength(0); i++) {
            if (adj[roomIndex, i] == (int)side) {
                return true;
            }
        }
        return false;
    }

    /* Определяет размер максимально возможного сдвига стороны side комнаты 
     * с номером roomIndex из списка rooms с матрицей смежности adj, при
     * котором не нарушатся связи между соседними комнатами. */
    private int GetSideConstraints(List<BspLeaf> rooms, int[,] adj, 
                                int roomIndex, AdjSide side) {
        //комнаты, которые могут ограничивать сдвиг стороны side для комнаты с номером roomIndex
        List<BspLeaf> constrainingRooms = FindConstrainingRooms(rooms, adj, 
                                                                roomIndex, side);
        
        //если ограничивающих комнат нет, то нужно учесть только минимально допустимые размеры комнаты
        if(constrainingRooms.Count == 0) {
            if(side == AdjSide.Right || side == AdjSide.Left) {
                return rooms[roomIndex].width - LevelGenerator.MIN_ROOM_COLUMNS;
            }
            return rooms[roomIndex].height - LevelGenerator.MIN_ROOM_ROWS;
        }

        //список координат, ограничивающих уменьшение стороны комнаты
        List<int> jointCoords = FindConstraintCoordinates(constrainingRooms, 
                                    rooms[roomIndex], side);
        return FindFinalConstraint(jointCoords, rooms[roomIndex], side);
    }

    //Возвращает список комнат, связь с которыми может пропасть после
    //уменьшения стороны side комнаты с номером roomIndex из списка rooms.
    private List<BspLeaf> FindConstrainingRooms(List<BspLeaf> rooms, int[,] adj,
                                int roomIndex, AdjSide side) {
        List<BspLeaf> constrainingRooms = new List<BspLeaf>();
        int cSide1, cSide2;
        if (side == AdjSide.Right || side == AdjSide.Left) {
            //при сдвиге правой/левой стены комнаты нужно учитывать положение 
            //комнат, находящихся сверху/снизу, т.к. может исчезнуть связь между комнатами.
            cSide1 = (int)AdjSide.Top;
            cSide2 = (int)AdjSide.Bottom;
        }
        else {
            //при сдвиге верхней/нижней стены комнаты нужно учитывать положение 
            //комнат, находящихся справа/слева.
            cSide1 = (int)AdjSide.Left;
            cSide2 = (int)AdjSide.Right;
        }

        for (int i = 0; i < adj.GetLength(0); i++) {
            if (adj[roomIndex, i] == cSide1 || adj[roomIndex, i] == cSide2) {
                constrainingRooms.Add(rooms[i]);
            }
        }
        return constrainingRooms;
    }

    //Возвращает отсортированный список координат, ограничивающих  
    //случайное уменьшение стороны side комнаты currentRoom.
    private List<int> FindConstraintCoordinates(List<BspLeaf> constrainingRooms,
                                                BspLeaf currentRoom, AdjSide side) {
        List<int> constraintCoords = new List<int>();
        switch (side) {
            case AdjSide.Left:
                foreach (BspLeaf room in constrainingRooms) {
                    constraintCoords.Add(Mathf.Min(room.leafPosition.x + room.width,
                        currentRoom.leafPosition.x + currentRoom.width));
                }                   
                break;
            case AdjSide.Right:
                foreach (BspLeaf room in constrainingRooms) {
                    constraintCoords.Add(Mathf.Max(room.leafPosition.x,
                        currentRoom.leafPosition.x));
                }
                break;
            case AdjSide.Top:
                foreach (BspLeaf room in constrainingRooms) {
                    constraintCoords.Add(Mathf.Max(room.leafPosition.y,
                        currentRoom.leafPosition.y));
                }
                break;
            case AdjSide.Bottom:
                foreach (BspLeaf room in constrainingRooms) {
                    constraintCoords.Add(Mathf.Min(room.leafPosition.y + room.height,
                        currentRoom.leafPosition.y + currentRoom.height));
                }
                break;
        }        
        constraintCoords.Sort();
        return constraintCoords;
    }

    //Возвращает максимальную величину, на которую можно уменьшить сторону 
    //side комнаты currentRoom без нарушения связи с соседними комнатами.
    private int FindFinalConstraint(List<int> constraintCoords, BspLeaf currentRoom, 
                                    AdjSide side) {
        switch (side) {
            case AdjSide.Left:
                return Mathf.Min(constraintCoords[0] - 2 - minDoorSize - currentRoom.leafPosition.x,
                    currentRoom.width - LevelGenerator.MIN_ROOM_COLUMNS);
            case AdjSide.Right:
                return Mathf.Min(currentRoom.leafPosition.x + currentRoom.width -
                    (constraintCoords[constraintCoords.Count - 1] + 2 + minDoorSize),
                    currentRoom.width - LevelGenerator.MIN_ROOM_COLUMNS);
            case AdjSide.Top:
                return Mathf.Min(currentRoom.leafPosition.y + currentRoom.height -
                    (constraintCoords[constraintCoords.Count - 1] + 2 + minDoorSize),
                    currentRoom.height - LevelGenerator.MIN_ROOM_ROWS);
            default:
                return Mathf.Min(constraintCoords[0] - 2 - minDoorSize - currentRoom.leafPosition.y,
                    currentRoom.height - LevelGenerator.MIN_ROOM_ROWS);
        }
    }

    /* Соединяет смежные комнаты дверями. В данной реализации дверь =
     * отсутствие стены. Ширина и положение двери выбираются случайно.
     * Достаточно обработки только правой половины матрицы смежности adj,
     * поэтому возможны только случаи Top и Right. */
    private void MakeDoors(List<BspLeaf> rooms, int[,] adj, Tilemap wallMap, 
                           Tilemap floorMap, RandomTile floorTile) {
        int possibleDoorStart, possibleDoorEnd, doorSize, doorOffset;
        Random.InitState(System.DateTime.Now.Millisecond);
        for (int i = 0; i < adj.GetLength(0); i++) {
            for (int j = i + 1; j < adj.GetLength(1); j++) {

                if(adj[i, j] > 0) {  //находим соприкасающиеся комнаты                    
                    //определяем координаты соприкосновения комнат
                    possibleDoorStart = adj[i, j] == (int)AdjSide.Top ?
                        Mathf.Max(rooms[i].leafPosition.x + 1, rooms[j].leafPosition.x + 1) :
                        Mathf.Max(rooms[i].leafPosition.y + 1, rooms[j].leafPosition.y + 1);
                    possibleDoorEnd = adj[i, j] == (int)AdjSide.Top ?
                        Mathf.Min(rooms[i].leafPosition.x + rooms[i].width - 1,
                                                    rooms[j].leafPosition.x + rooms[j].width - 1) :
                        Mathf.Min(rooms[i].leafPosition.y + rooms[i].height - 1,
                                                    rooms[j].leafPosition.y + rooms[j].height - 1);
                    
                    //рандомизируем положение и размер двери                    
                    doorSize = Random.Range(minDoorSize, Mathf.Min(maxDoorSize, possibleDoorEnd - possibleDoorStart) + 1);
                    doorOffset = Random.Range(0, (possibleDoorEnd - possibleDoorStart) - doorSize + 1);

                    for (int m = 0; m < doorSize; m++) {
                        Vector3Int pos = adj[i, j] == (int)AdjSide.Top ?
                            new Vector3Int(possibleDoorStart + doorOffset + m,
                                            rooms[j].leafPosition.y, 0) :
                            new Vector3Int(rooms[j].leafPosition.x,
                                            possibleDoorStart + doorOffset + m, 0);
                        wallMap.SetTile(pos, null);         //делаем дверной проем, убирая стены
                        db.wallTiles.list.Remove(pos);

                        floorMap.SetTile(pos, floorTile);   //застилаем проем полом
                        db.floorTiles.list.Add(pos);
                    }
                }
            }
        }
    }

    //Находит матрицу смежности для комнат из списка rooms.
    private int[,] FindAdjacencyMatrix(List<BspLeaf> rooms) {
        if (rooms == null || rooms.Count == 0) {
            return null;
        }
        int[,] result = new int[rooms.Count, rooms.Count];

        for(int i = 0; i < rooms.Count; i++) {
            for(int j = 0; j < rooms.Count; j++) {
                if(i == j) {
                    result[i, j] = 0;    //нулевая главная диагональ
                } else if (i < j) {  //граф неориентированный, каждую связь можно проверить 1 раз
                    int connection = FindConnection(rooms[i], rooms[j]);
                    result[i, j] = connection;
                    result[j, i] = FindReverseConnection(connection);
                }
            }
        }
        return result;
    }

    //Определяет расположение сгенерированных комнат room1 и room2 относительно 
    //друг друга. Возможны 3 варианта - справа, слева, не соприкасаются.    
    private int FindConnection(BspLeaf room1, BspLeaf room2) {
        if ((room1.leafPosition.y + room1.height == room2.leafPosition.y) &&
            (((room1.leafPosition.x <= room2.leafPosition.x) && (room1.leafPosition.x + room1.width >= room2.leafPosition.x + 2 + minDoorSize)) ||
            ((room1.leafPosition.x + 2 + minDoorSize <= room2.leafPosition.x + room2.width) && (room1.leafPosition.x + room1.width >= room2.leafPosition.x + room2.width)) ||
            ((room1.leafPosition.x <= room2.leafPosition.x) && (room1.leafPosition.x + room1.width >= room2.leafPosition.x + room2.width)))) {
            return (int)AdjSide.Top;   //room2 находится сверху от room1
        }
        else if ((room1.leafPosition.x + room1.width == room2.leafPosition.x) &&
                (((room1.leafPosition.y <= room2.leafPosition.y) && (room1.leafPosition.y + room1.height >= room2.leafPosition.y + 2 + minDoorSize)) ||
                ((room1.leafPosition.y + 2 + minDoorSize <= room2.leafPosition.y + room2.height) && (room1.leafPosition.y + room1.height >= room2.leafPosition.y + room2.height)) ||
                ((room1.leafPosition.y <= room2.leafPosition.y) && (room1.leafPosition.y + room1.height >= room2.leafPosition.y + room2.height)))) {
            return (int)AdjSide.Right;   //room2 находится справа от room1
        }
        else {
            //room1 и room2 не соприкасаются, либо не хватает места 
            //для создания двери шириной не менее minDoorSize клеток
            return (int)AdjSide.None;
        }
    }

    //Заполняет левую половины матрицы смежности.
    private int FindReverseConnection(int connection) {
        switch (connection) {
            case (int)AdjSide.Top:
                return (int)AdjSide.Bottom;
            case (int)AdjSide.Right:
                return (int)AdjSide.Left;
            default:
                return (int)AdjSide.None;
        }
    }

    private string Array2DToString(int[,] a) {
        string result = "";
        for (int i = 0; i < a.GetLength(0); i++) {
            for (int j = 0; j < a.GetLength(1); j++) {
                result += a[i, j] + " ";
            }
            result += System.Environment.NewLine;
        }
        return result;
    }

    /* Удаляет связь между комнатами, после чего стены комнат не будут соприкасаться.
     * Для этого комната с большей размерностью уменьшается на 1. Обрабатывается
     * только правая половина матрицы смежности, поэтому учитываются только случаи
     * Top и Right. */
    private void RemoveConnection(BspLeaf room1, BspLeaf room2, int connection) {
        switch (connection) {
            case (int)AdjSide.Top:
                if (room1.height > room2.height) {
                    room1.height--;
                } else {
                    room2.leafPosition.y++;
                    room2.height--;
                }
                break;
            case (int)AdjSide.Right:
                if (room1.width > room2.width) {
                    room1.width--;
                }
                else {
                    room2.leafPosition.x++;
                    room2.width--;
                }
                break;
            default:
                break;
        }
    }

    //Возвращает true, если граф с комнатами является связным -
    //из каждой комнаты можно попасть в любую другую.
    private bool IsRoomGraphConnected(int[,] adj) {
        List<int> visitedVertices = new List<int> {0};    //для хранения посещенных вершин графа

        bool newVerticesAdded = true;
        int checkIteration = 0;
        while (newVerticesAdded) {
            newVerticesAdded = false;
            for (int i = checkIteration; i < visitedVertices.Count; i++) {
                if (visitedVertices.Count == adj.GetLength(0)) {
                    return true;  //все вершины посещены, граф является связным
                }
                for(int j = 0; j < adj.GetLength(0); j++) {
                    if(adj[visitedVertices[i], j] > 0 &&
                            !visitedVertices.Contains(j)) {
                        visitedVertices.Add(j);
                        newVerticesAdded = true;
                    }
                }
            }
            checkIteration++;   //чтобы не проверять уже проверенные вершины
        }
        return false;       
    }
}
