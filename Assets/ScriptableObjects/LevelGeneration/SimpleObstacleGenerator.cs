﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

//Размещает на уровне клетки препятствий и противников случайным образом
[CreateAssetMenu(menuName = "Obstacle Generator / Simple Obstacle Generator")]
public class SimpleObstacleGenerator : ObstacleGenerator {

    public EnemyRarity[] enemyRarities;
    private List<List<Vector3Int>> roomGrids = new List<List<Vector3Int>>();

    //minRate - минимальный процент клеток, являющихся укрытиями; maxRate - максимальный.
    public override void GenerateObstacles(List<BspLeaf> rooms) {
        obstacleTiles = Instantiate(obstacleTilesPrefab);
        InitializeList(rooms);
        int seed = System.DateTime.Now.Millisecond;
        Random.InitState(seed);

        for(int i = 0; i < rooms.Count; i++) {
            //вычисляем кол-во клеток препятствий для каждой комнаты уровня
            int objectCount = Random.Range(Mathf.RoundToInt(
                                rooms[i].width * rooms[i].height * minObstacleRate),
                            Mathf.RoundToInt(
                                rooms[i].width * rooms[i].height * maxObstacleRate) + 1);

            for (int j = 0; j < objectCount; j++) {
                Vector3Int randomPosition = RandomPosition(i);
                obstacleTiles.SetTile(randomPosition, obstacleTile);
                db.obstacleTiles.list.Add(randomPosition);
            }
        }
    }

    public override void PlaceEnemies(List<BspLeaf> rooms) {
        
        for (int i = 0; i < rooms.Count; i++) {
            //кол-во врагов в каждой комнате рассчитывается, исходя из ее площади
            int minEnemies = Mathf.RoundToInt((rooms[i].width * rooms[i].height) / LevelGenerator.TILES_PER_ENEMY);
            int enemiesCount = Random.Range(minEnemies, minEnemies + 2);

            for (int j = 0; j < enemiesCount; j++) {
                Vector3Int randomPosition = RandomEnemyPosition(i, rooms);
                GameObject enemyClone = Instantiate(enemiesPrefabs[Random.Range(0, enemiesPrefabs.Length)],
                    randomPosition, Quaternion.identity) as GameObject;
                enemyClone.GetComponent<Enemy>().rarity = GetEnemyRarity();                
                gameObjectEvent.Raise(enemyClone, onEnemySpawned);  //выдать событие спавна нового противника
            }
        }
    }

    private void InitializeList(List<BspLeaf> rooms) {
        roomGrids.Clear();

        foreach(BspLeaf room in rooms) {
            List<Vector3Int> roomGrid = new List<Vector3Int>();
            //по внутреннему периметру препятствия не размещаются
            for (int x = 2; x < room.width - 2; x++) {
                for (int y = 2; y < room.height - 2; y++) {
                    roomGrid.Add(new Vector3Int(room.leafPosition.x + x,
                        room.leafPosition.y + y, 0));
                }
            }
            roomGrids.Add(roomGrid);
        }        
    }

    private Vector3Int RandomPosition(int roomIndex) {
        int randomIndex = Random.Range(0, roomGrids[roomIndex].Count);
        Vector3Int randomPosition = roomGrids[roomIndex][randomIndex];
        roomGrids[roomIndex].RemoveAt(randomIndex);
        return randomPosition;
    }

    private Vector3Int RandomEnemyPosition(int roomIndex, List<BspLeaf> rooms) {
        int randomIndex;
        Vector3Int randomPosition;
        //генерирует случайную позицию для противника. при этом противники
        //должны размещаться в дальней от игрока половине комнаты
        do {
            randomIndex = Random.Range(0, roomGrids[roomIndex].Count);
            randomPosition = roomGrids[roomIndex][randomIndex];
        } while (randomPosition.x < rooms[roomIndex].leafPosition.x + 
                                    ((rooms[roomIndex].width - 2) / 2));
        roomGrids[roomIndex].RemoveAt(randomIndex);
        return randomPosition;
    }

    private EnemyRarity GetEnemyRarity() {
        float rarityRoll = Random.Range(0.0f, 1.0f);
        float rarityMinRoll = 0;
        for (int i = 0; i < enemyRarities.Length; i++) {
            if (rarityRoll >= rarityMinRoll && rarityRoll < rarityMinRoll + enemyRarities[i].ChanceToSpawn) {
                return enemyRarities[i];
            }
            else {
                rarityMinRoll += enemyRarities[i].ChanceToSpawn;
            }
        }
        return enemyRarities[0];
    }
}
