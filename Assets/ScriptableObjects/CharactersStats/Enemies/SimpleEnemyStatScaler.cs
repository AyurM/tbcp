﻿using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Stat Scaler/Simple Enemy Stat Scaler")]
public class SimpleEnemyStatScaler : EnemyStatScaler {

    private const int MIN_GAME_LEVEL = 1;
    private const int MAX_GAME_LEVEL = 100;
    private const int MIN_ENEMY_LEVEL = 1;
    private const int MAX_ENEMY_LEVEL = 80;
    private const float ENEMY_LVL_SCALE = 1.2f;

    public override int GetEnemyLevel() {

        float t = (((float)db.levelNumber.CurrentValue) - MIN_GAME_LEVEL) / ((float)MAX_GAME_LEVEL - MIN_GAME_LEVEL);
        int result = Mathf.RoundToInt((1 - Mathf.Pow(1 - t, ENEMY_LVL_SCALE)) * (MAX_ENEMY_LEVEL - MIN_ENEMY_LEVEL) +
            MIN_ENEMY_LEVEL);
        //Debug.unityLogger.Log("Enemy lvl scale - result", result);
        return result;
    }

    //TODO: добавить зависить от enemyLevel
    public override int GetMaxAP(EnemyStatTable table, EnemyRarity rarity,
        int enemyLevel) {
        return (int)(table.BaseAP * rarity.BaseAPMultiplier);
    }

    public override int GetMaxHP(EnemyStatTable table, EnemyRarity rarity,
        int enemyLevel) {
        return (int)(table.BaseHP * rarity.BaseHPMultiplier);
    }

    public override int GetXPForKill(EnemyStatTable table, EnemyRarity rarity,
        int enemyLevel) {
        return(int)(table.BaseXPForKill * rarity.BaseXPMultiplier);
    }

    public override int GetNumberOfItemsToDrop(EnemyStatTable table, EnemyRarity rarity,
        int enemyLevel) {
        return Random.Range(table.BaseMinItemsToDrop, table.BaseMaxItemsToDrop + 1);
    }
}
