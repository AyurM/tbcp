﻿using UnityEngine;

public abstract class EnemyStatScaler : ScriptableObject {

    public SODatabase db;

    public abstract int GetEnemyLevel();

    public abstract int GetMaxHP(EnemyStatTable table, EnemyRarity rarity,
        int enemyLevel);

    public abstract int GetMaxAP(EnemyStatTable table, EnemyRarity rarity,
        int enemyLevel);

    public abstract int GetXPForKill(EnemyStatTable table, EnemyRarity rarity,
        int enemyLevel);

    public abstract int GetNumberOfItemsToDrop(EnemyStatTable table, EnemyRarity rarity,
        int enemyLevel);
}
