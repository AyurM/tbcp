﻿using UnityEngine;

[CreateAssetMenu]
public class EnemyRarity : ScriptableObject {

    public string Name;
    public Color Color;
    public float BaseHPMultiplier;
    public float BaseAPMultiplier;
    public float BaseXPMultiplier;
    public float ChanceToSpawn;
}
