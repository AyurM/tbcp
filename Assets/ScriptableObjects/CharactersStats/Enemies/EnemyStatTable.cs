﻿using UnityEngine;

[CreateAssetMenu(menuName = "Stat Scaler/Enemy Stats Table")]
public class EnemyStatTable : ScriptableObject {

    public string BaseName;
    public int BaseHP;
    public int BaseAP;
    public int BaseXPForKill;
    public int BaseMinItemsToDrop;
    public int BaseMaxItemsToDrop;
}
