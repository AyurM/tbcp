﻿using UnityEngine;

[CreateAssetMenu(menuName = "Stat Scaler/Player Stat Scaler")]
public class PlayerStatScaler : StatScaler {
   
    public int StartHP = 20;
    public int MaxXP = 2100000000;
    public int MaxLevel = 100;
    public int StartAP = 8;
    public int HPPerLevel = 3;
    public int LevelsForOneAp = 10;

    public override int GetMaxHP(int currentLevel) {
        return StartHP + (currentLevel - 1) * HPPerLevel;
    }

    public override int GetMaxAP(int currentLevel) {
        return StartAP + Mathf.FloorToInt(currentLevel / LevelsForOneAp);
    }

    public override int GetXPForNextLevel(int currentLevel) {
        return Mathf.RoundToInt(
            (Mathf.Pow((currentLevel + 1f) / MaxLevel, 4.5f) -
                Mathf.Pow((float)currentLevel / MaxLevel, 4.5f)) * MaxXP);
    }
}
