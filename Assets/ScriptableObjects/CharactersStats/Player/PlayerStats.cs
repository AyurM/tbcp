﻿using UnityEngine;

public class PlayerStats : MonoBehaviour, IIntEventListener {

    public SODatabase db;
    public PlayerStatScaler StatScaler;

    public IntEvent intEvent;

    void OnEnable() {
        RegisterIntListener();
    }

    void OnDisable() {
        UnregisterIntListener();
    }

    private void Start() {
        db.playerHP.MaxValue = StatScaler.GetMaxHP(db.playerLevel.CurrentValue);
        intEvent.Raise(db.playerHP);
        db.playerAP.MaxValue = StatScaler.GetMaxAP(db.playerLevel.CurrentValue);
        db.playerAP.CurrentValue = db.playerAP.MaxValue;
        intEvent.Raise(db.playerAP);
        db.playerXP.MaxValue = StatScaler.GetXPForNextLevel(db.playerLevel.CurrentValue);
        intEvent.Raise(db.playerXP);
    }

    public void OnIntEventRaised(IntVariable variable) {
        if (variable == db.playerLevel) {
            OnLevelUp();
        }
    }

    private void OnLevelUp() {
        db.playerXP.MaxValue = StatScaler.GetXPForNextLevel(db.playerLevel.CurrentValue);
        db.playerHP.MaxValue = StatScaler.GetMaxHP(db.playerLevel.CurrentValue);
        db.playerHP.CurrentValue = db.playerHP.MaxValue;
        intEvent.Raise(db.playerHP);
        db.playerAP.MaxValue = StatScaler.GetMaxAP(db.playerLevel.CurrentValue);
        intEvent.Raise(db.playerAP);
    }

    public void RegisterIntListener() {
        intEvent.RegisterListener(this);
    }

    public void UnregisterIntListener() {
        intEvent.UnregisterListener(this);
    }
}
