﻿using UnityEngine;

[CreateAssetMenu(menuName = "Game Database")]
public class SODatabase : ScriptableObject {

    //Boolean variables
    public BoolVariable isPlayerMoving;
    public BoolVariable isPlayerTurn;
    public BoolVariable isPlayerDead;
    public BoolVariable isOnLootbox;
    public BoolVariable isMainMenuActive;
    public BoolVariable isMoveTileClick;
    public BoolVariable isLevelComplete;

    //Integer variables
    public IntVariable playerXP;
    public IntVariable playerHP;
    public IntVariable playerAP;
    public IntVariable playerLevel;
    public IntVariable apToMove;
    [Tooltip("Номер противника, выбранного игроком.")]
    public IntVariable selectedEnemy;
    [Tooltip("Номер противника, совершающего ход в данный момент.")]
    public IntVariable currentEnemy;
    public IntVariable previousEnemy;
    public IntVariable damageDealt;
    public IntVariable levelNumber;
    public IntVariable levelColumns;
    public IntVariable levelRows;
    public IntVariable selectedInventoryItem;
    public IntVariable cameraMinH;
    public IntVariable cameraMaxH;
    public IntVariable cameraMinV;
    public IntVariable cameraMaxV;
    public IntVariable missionNumber;

    //Float variables
    public FloatVariable cameraOrthSize;

    //Vector3 variables
    public Vector3Variable playerPosition;
    public Vector3Variable clickPosition;
    public Vector3Variable selectedTile;
    [Tooltip("Здесь сохраняются координаты возможной позиции противника, когда AI ищет" +
        " позицию для атаки игрока.")]
    public Vector3Variable tempEnemyPosition;
    public Vector3Variable enemyCoverPosition;

    //ListVector3Int variables
    public ListVector3IntVariable floorTiles;
    public ListVector3IntVariable wallTiles;
    public ListVector3IntVariable obstacleTiles;

    //Item Types
    public ItemType ammoType;
    public ItemType armorType;
    public ItemType usableType;
    public ItemType weaponType;

    //Others
    [Tooltip("Список присутствующих на уровне противников.")]
    public ListGOVariable enemiesList;
    public EnemiesMoveInfo enemiesMoveInfo;
    public PlayerMoveInfo playerMoveInfo;
    public Inventory inventory;

}
