﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Loot Generator Rules / Simple Rules")]
public class SimpleLootGeneratorRules : LootGeneratorRules {
    public Inventory inventory;

    //Шаблоны для оружия
    public BaseMeleeWeapon[] baseMeleeWeapons;
    public BasePistol[] basePistols;
    public BaseShotgun[] baseShotguns;

    //Шаблоны для брони
    public BaseBodyArmor[] baseBodyArmors;
    public BaseHelmet[] baseHelmets;
    public BaseBoots[] baseBoots;
    public BaseGloves[] baseGloves;

    //Шаблоны для вспомогательных предметов
    public BaseMedkit[] baseMedkits;

    private int GetItemRarity() {
        //Random.InitState(DateTime.Now.Millisecond);
        float rarityRoll = Random.Range(0.0f, 1.0f);
        //Debug.unityLogger.Log("ItemRarity roll", rarityRoll);
        float rarityMinRoll = 0;
        for(int i = 0; i < rarities.Length; i++) {
            if(rarityRoll >= rarityMinRoll && rarityRoll < rarityMinRoll + rarities[i].ChanceToDrop) {
                return i;
            } else {
                rarityMinRoll += rarities[i].ChanceToDrop;
            }
        }
        return 0;
    }

    private int GetRandomBaseItem() {
        //равновероятное выпадение каждого базового типа
        //int seed = System.DateTime.Now.Millisecond;
        //Random.InitState(seed);
        return Random.Range(0, itemBasesCount);
    }

    private int GetRandomBaseWeapon() {
        //равновероятное выпадение каждого базового типа оружия
        //int seed = DateTime.Now.Millisecond;
        //Random.InitState(seed);
        return Random.Range(0, weaponBasesCount);
    }

    private int GetRandomBaseArmor() {
        //равновероятное выпадение каждого базового типа брони
        //int seed = DateTime.Now.Millisecond;
        //Random.InitState(seed);
        return Random.Range(weaponBasesCount, weaponBasesCount + armorBasesCount);
    }

    private int GetRandomUtilityBase() {
        //Random.InitState(DateTime.Now.Millisecond);
        return Random.Range(weaponBasesCount + armorBasesCount, weaponBasesCount + armorBasesCount + utilBasesCount);
    }

    public override Item GetItem(int baseItem, int rarityIndex, int dropLevel) {
        switch (baseItem) {
            case 0:
                return GetMeleeWeapon(rarityIndex, dropLevel);
            case 1:
                return GetPistol(rarityIndex, dropLevel);
            case 2:
                return GetShotgun(rarityIndex, dropLevel);
            case 3:
                return GetBodyArmor(rarityIndex, dropLevel);
            case 4:
                return GetHelmet(rarityIndex, dropLevel);
            case 5:
                return GetBoots(rarityIndex, dropLevel);
            case 6:
                return GetGloves(rarityIndex, dropLevel);
            case 7:
                return GetMedkit(dropLevel);
            default:
                return GetShotgun(rarityIndex, dropLevel);
        }
    }

    public override Item GetRandomItem(int dropLevel) {
        return GetItem(GetRandomBaseItem(), GetItemRarity(), dropLevel);
    }

    public override Weapon GetRandomWeapon(int dropLevel) {
        Weapon result = (Weapon)GetItem(GetRandomBaseWeapon(), GetItemRarity(), dropLevel);
        return result;
    }

    public override Armor GetRandomArmor(int dropLevel) {
        Armor result = (Armor)GetItem(GetRandomBaseArmor(), GetItemRarity(), dropLevel);
        return result;
    }

    public override Item GetRandomUtilItem(int dropLevel) {
        return GetItem(GetRandomUtilityBase(), GetItemRarity(), dropLevel);
    }

    private BodyArmor GetBodyArmor(int rarityIndex, int dropLevel) {
        List<BaseBodyArmor> baseArmorsToDrop = GetBasesToDrop(baseBodyArmors, dropLevel);
        int seed = DateTime.Now.Millisecond;
        Random.InitState(seed);

        BaseBodyArmor baseArmor = baseArmorsToDrop[Random.Range(0, baseArmorsToDrop.Count)];

        //Создать броню по выбранному случайному шаблону
        BodyArmor result = new BodyArmor(baseArmor, rarities[rarityIndex]);
        ApplyRarityToArmor(ref result, rarityIndex);

        return result;
    }

    private Helmet GetHelmet(int rarityIndex, int dropLevel) {
        List<BaseHelmet> baseHelmetsToDrop = GetBasesToDrop(baseHelmets, dropLevel);
        int seed = DateTime.Now.Millisecond;
        Random.InitState(seed);

        BaseHelmet baseHelmet = baseHelmetsToDrop[Random.Range(0, baseHelmetsToDrop.Count)];

        //Создать шлем по выбранному случайному шаблону
        Helmet result = new Helmet(baseHelmet, rarities[rarityIndex]);
        ApplyRarityToArmor(ref result, rarityIndex);
        
        return result;
    }

    private Boots GetBoots(int rarityIndex, int dropLevel) {
        List<BaseBoots> baseBootsToDrop = GetBasesToDrop(baseBoots, dropLevel);
        int seed = DateTime.Now.Millisecond;
        Random.InitState(seed);

        BaseBoots baseBoot = baseBootsToDrop[Random.Range(0, baseBootsToDrop.Count)];

        //Создать обувь по выбранному случайному шаблону
        Boots result = new Boots(baseBoot, rarities[rarityIndex]);
        ApplyRarityToArmor(ref result, rarityIndex);

        return result;
    }

    private Gloves GetGloves(int rarityIndex, int dropLevel) {
        List<BaseGloves> baseGlovesToDrop = GetBasesToDrop(baseGloves, dropLevel);
        int seed = DateTime.Now.Millisecond;
        Random.InitState(seed);

        BaseGloves baseGlove = baseGlovesToDrop[Random.Range(0, baseGlovesToDrop.Count)];

        //Создать перчатки по выбранному случайному шаблону
        Gloves result = new Gloves(baseGlove, rarities[rarityIndex]);
        ApplyRarityToArmor(ref result, rarityIndex);     
        return result;
    }

    private Pistol GetPistol(int rarityIndex, int dropLevel) {
        List<BasePistol> basePistolsToDrop = GetBasesToDrop(basePistols, dropLevel);
        int seed = DateTime.Now.Millisecond;
        Random.InitState(seed);
        BasePistol basePistol = basePistolsToDrop[Random.Range(0, basePistolsToDrop.Count)];

        //Создать пистолет по выбранному случайному шаблону
        Pistol result = new Pistol(basePistol, rarities[rarityIndex]);

        ApplyRarityToWeapon(ref result, rarityIndex);
        return result;
    }

    private Shotgun GetShotgun(int rarityIndex, int dropLevel) {
        List<BaseShotgun> baseSgsToDrop = GetBasesToDrop(baseShotguns, dropLevel);
        int seed = DateTime.Now.Millisecond;
        Random.InitState(seed);
        BaseShotgun baseSg = baseSgsToDrop[Random.Range(0, baseSgsToDrop.Count)];

        //Создать дробовик по выбранному случайному шаблону
        Shotgun result = new Shotgun(baseSg, rarities[rarityIndex]);

        ApplyRarityToWeapon(ref result, rarityIndex);
        return result;

    }

    private MeleeWeapon GetMeleeWeapon(int rarityIndex, int dropLevel) {
        List<BaseMeleeWeapon> baseMeleeToDrop = GetBasesToDrop(baseMeleeWeapons, dropLevel);
        int seed = DateTime.Now.Millisecond;
        Random.InitState(seed);
        BaseMeleeWeapon baseMelee = baseMeleeToDrop[Random.Range(0, baseMeleeToDrop.Count)];

        //Создать оружие ближнего боя по выбранному случайному шаблону
        MeleeWeapon result = new MeleeWeapon(baseMelee, rarities[rarityIndex]);

        ApplyRarityToWeapon(ref result, rarityIndex);
        return result;
    }

    private Medkit GetMedkit(int dropLevel) {
        List<BaseMedkit> baseMedkitsToDrop = GetBasesToDrop(baseMedkits, dropLevel);
        Random.InitState(DateTime.Now.Millisecond);
        BaseMedkit baseMedkit = baseMedkitsToDrop[Random.Range(0, baseMedkitsToDrop.Count)];

        Medkit result = new Medkit(baseMedkit, rarities[0]);
        return result;
    }

    private List<T> GetBasesToDrop<T>(T[] bases, int dropLevel) where T : BaseItem{
        //отсеять неподходящие по дроплевелу базы
        List<T> result = new List<T>();
        for (int i = 0; i < bases.Length; i++) {
            if (bases[i].baseMinLevelToDrop <= dropLevel) {
                result.Add(bases[i]);
            }
        }
        return result;
    }

    //Усиливает базовые значения параметров оружия в зависимости от редкости
    private void ApplyRarityToWeapon<T>(ref T weapon, int rarityIndex) where T : Weapon{
        int propertiesToUpgrade;
        //определить, сколько параметров оружия необходимо усилить
        if (rarityIndex == 0) {
            propertiesToUpgrade = rarities[rarityIndex].WeaponAttributesToUpgrade;
        }
        else {
            propertiesToUpgrade = Random.Range(
                rarities[rarityIndex - 1].WeaponAttributesToUpgrade + 1,
                rarities[rarityIndex].WeaponAttributesToUpgrade + 1);
        }

        List<int> attributes = new List<int> { 0, 1, 2, 3, 4 };
        
        //TODO: вставить алгоритм усиления базовых параметров оружия в зависимости от редкости
        for (int i = 0; i < propertiesToUpgrade; i++) {
            int attribute = attributes[Random.Range(0, attributes.Count)];
            attributes.Remove(attribute);
            switch (attribute) {
                case 0:
                    weapon.minDamage += 1;
                    break;
                case 1:
                    weapon.maxDamage += 1;
                    break;
                case 2:
                    weapon.attackRange += 1;
                    break;
                case 3:
                    weapon.critChance += 0.1f;
                    break;
                case 4:
                    weapon.critMultiplier += 0.2f;
                    break;
            }
        }
    }

    //Усиливает базовые значения параметров брони в зависимости от редкости
    private void ApplyRarityToArmor<T>(ref T armor, int rarityIndex) where T : Armor {
        int propertiesToUpgrade;
        //определить, сколько параметров оружия необходимо усилить
        if (rarityIndex == 0) {
            propertiesToUpgrade = rarities[rarityIndex].ArmorAttributesToUpgrade;
        }
        else {
            propertiesToUpgrade = Random.Range(
                rarities[rarityIndex - 1].ArmorAttributesToUpgrade + 1,
                rarities[rarityIndex].ArmorAttributesToUpgrade + 1);
        }

        List<int> attributes = new List<int> { 0, 1, 2};

        //TODO: вставить алгоритм усиления базовых параметров брони в зависимости от редкости
        for (int i = 0; i < propertiesToUpgrade; i++) {
            int attribute = attributes[Random.Range(0, attributes.Count)];
            attributes.Remove(attribute);
            switch (attribute) {
                case 0:
                    armor.defense += 1;
                    break;
                case 1:
                    armor.hitReduction += 1;
                    break;
                case 2:
                    armor.shotReduction += 1;
                    break;                
            }
        }
    }
}
