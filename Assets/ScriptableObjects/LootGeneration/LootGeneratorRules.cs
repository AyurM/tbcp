﻿using UnityEngine;

public abstract class LootGeneratorRules : ScriptableObject {

    public ItemRarity[] rarities;
    public int itemBasesCount;
    public int weaponBasesCount;
    public int armorBasesCount;
    public int utilBasesCount;

    public abstract Item GetItem(int baseItem, int rarityIndex, int dropLevel);

    public abstract Item GetRandomItem(int dropLevel);

    public abstract Weapon GetRandomWeapon(int dropLevel);

    public abstract Armor GetRandomArmor(int dropLevel);

    public abstract Item GetRandomUtilItem(int dropLevel);
}
