﻿using UnityEngine;

[CreateAssetMenu]
public class ItemRarity : ScriptableObject {

    public string Name;
    public Color Color;

    [Range(0.0f, 1.0f)]
    public float ChanceToDrop;
    public int WeaponAttributesToUpgrade;
    public int ArmorAttributesToUpgrade;
}
