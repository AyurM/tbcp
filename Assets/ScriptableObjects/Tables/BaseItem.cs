﻿using UnityEngine;

public abstract class BaseItem : ScriptableObject {

    public int baseMinLevelToDrop;
    public string baseName;
    public string baseDesc;
    public ItemType itemType;
    public Sprite baseSprite;
}
