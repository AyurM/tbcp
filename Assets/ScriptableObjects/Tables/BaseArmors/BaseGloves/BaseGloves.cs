﻿using UnityEngine;

[CreateAssetMenu(menuName = "Base Armor / Base Gloves")]
public class BaseGloves : BaseItem {

    public ArmorType armorType;
    public int baseDefense;
    public int baseHitReduction;
    public int baseShotReduction;
}
