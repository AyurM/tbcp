﻿using UnityEngine;

[CreateAssetMenu(menuName = "Base Armor / Base Body Armor")]
public class BaseBodyArmor : BaseItem {

    public ArmorType armorType;
    public int baseDefense;
    public int baseHitReduction;
    public int baseShotReduction;
}
