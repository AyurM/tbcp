﻿using UnityEngine;

[CreateAssetMenu(menuName = "Base Armor / Base Boots")]
public class BaseBoots : BaseItem {

    public ArmorType armorType;
    public int baseDefense;
    public int baseHitReduction;
    public int baseShotReduction;
}
