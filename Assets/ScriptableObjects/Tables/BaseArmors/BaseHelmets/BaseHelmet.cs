﻿using UnityEngine;

[CreateAssetMenu(menuName = "Base Armor / Base Helmet")]
public class BaseHelmet : BaseItem {

    public ArmorType armorType;
    public int baseDefense;
    public int baseHitReduction;
    public int baseShotReduction;
}
