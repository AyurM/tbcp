﻿using UnityEngine;

[CreateAssetMenu(menuName = "Base Usable / Base Medkit")]
public class BaseMedkit : BaseItem {

    public int quantity;
    public int hpToHeal;
    public int apToUse;
    public AudioClip useSound;
}
