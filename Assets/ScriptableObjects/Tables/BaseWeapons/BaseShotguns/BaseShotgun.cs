﻿using UnityEngine;

[CreateAssetMenu(menuName = "Base Weapon / Base Shotgun")]
public class BaseShotgun : BaseItem {

    public WeaponType weaponType;
    public AudioClip reloadSound;
    public int baseMinDamage;
    public int baseMaxDamage;
    public int baseAttackRange;

    [Range(0.0f, 1.0f)]
    public float baseCritChance;
    public float baseCritMultiplier;
    public int baseMaxAmmo;
    public int baseApToReload;

    public AttackMode[] baseAttackModes;
}

