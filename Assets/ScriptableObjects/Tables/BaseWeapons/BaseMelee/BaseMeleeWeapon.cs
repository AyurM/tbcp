﻿using UnityEngine;

[CreateAssetMenu(menuName = "Base Weapon / Base Melee Weapon")]
public class BaseMeleeWeapon : BaseItem {

    public WeaponType weaponType;
    public int baseMinDamage;
    public int baseMaxDamage;
    public int baseAttackRange;

    [Range(0.0f, 1.0f)]
    public float baseCritChance;
    public float baseCritMultiplier;

    public AttackMode[] baseAttackModes;
}
