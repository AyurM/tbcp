﻿using UnityEngine;

[CreateAssetMenu(menuName = "Item Types / Item Type")]
public class ItemType : ScriptableObject {
    public string typeName;
}
