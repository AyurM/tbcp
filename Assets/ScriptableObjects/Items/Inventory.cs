﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory")]
[System.Serializable]
public class Inventory : ScriptableObject {

    [HideInInspector]   
    public List<Item> items;   
    public PlayerEquipment equipment;

    public List<string> jsonItems;

    public void ResetInventory() {
        items = new List<Item>();      
        equipment = new PlayerEquipment();
        jsonItems = new List<string>();
    }

    public void AddItem(Item item) {
        if(item is StackableItem) {
            bool itemFound = false;
            foreach(Item it in items) {
                if(it.itemName == item.itemName) {
                    ((StackableItem)it).quantity += ((StackableItem)item).quantity;
                    itemFound = true;
                    break;
                }
            }
            if (!itemFound) {
                items.Add(item);
            }
        } else {
            items.Add(item);
        }
        
        if (item.isItemEquipped) {
            equipment.Equip(item);
        }
    }

    public void LoadInventory(GameState gState) {
        ResetInventory();
        foreach(string itemString in gState.inventory) {
            AddItem(LoadItem(itemString));
        }    
    }

    private Item LoadItem(string jsonString) {
        JsonItem temp = JsonUtility.FromJson<JsonItem>(jsonString);
        Item itemPO = (Item) JsonUtility.FromJson(jsonString,
                System.Type.GetType(temp.className));
        itemPO.Init();
        return itemPO;
    }

    public void SaveInventory() {
        jsonItems.Clear();
        for(int i = 0; i < items.Count; i++) {
            jsonItems.Add(items[i].ToJsonString());
        }
    }
}
