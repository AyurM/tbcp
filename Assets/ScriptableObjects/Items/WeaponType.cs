﻿using UnityEngine;

[CreateAssetMenu(menuName = "Item Types / Weapon Type")]
public class WeaponType : ScriptableObject {
    public string typeName;
}
