﻿using UnityEngine;

[CreateAssetMenu(menuName = "Item Types / Armor Type")]
public class ArmorType : ScriptableObject {
    public string typeName;
}

