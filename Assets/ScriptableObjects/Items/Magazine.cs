﻿using UnityEngine;

public abstract class Magazine : ScriptableObject {

    public abstract void Reload();

    public abstract bool HasAmmo();

    public abstract bool IsReloadable();

    public abstract int GetCurrentAmmo();

    public abstract void SpendAmmo(int ammoToSpend);

    public abstract int GetMaxAmmo();
}
